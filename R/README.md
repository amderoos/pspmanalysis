# PSPManalysis

This directory contains the R package of PSPManalysis. This software package implements numerical procedures for the analysis of physiologically structured population models (PSPMs). PSPMs represent a class of models that consistently translate continuous-time models of individual life history to the population level. The formulation of such models is discussed extensively in Metz & Diekmann (1986) and De Roos (1997) and is presented here only as far as needed for the use of the software.

The software allows for five different types of analyses of PSPMs:

* **Demographic analysis**: For linear PSPMs that do not account for density dependence or population feedback on the life history of individual organisms, the long-term population growth rate can be calculated. If the dynamics of such a linear PSPM would be simulated over time in the long run the population would grow exponentially or decline to zero with this population growth rate. The software also automatically calculates the sensitivity of this population growth rate with respect to all model parameters. Furthermore, the software calculates the stable population distribution, which characterizes the composition of the population during its exponential growth phase, and the reproductive value of the individuals in this stable population state as a function of their individual state.

* **Equilibrium analysis**: Equilibrium states can be computed for non-linear PSPMs that do account for density dependence or feedback of the population on the life history of individual organisms. These equilibrium states are computed as a function of a single model parameter, resulting in a parameterized curve of equilibrium states. Two types of special points can be detected on these equilibrium curves: limit points, also called saddle-node bifurcation points, and branching points or transcritical bifurcation points. Furthermore, the software allows for the computation of these two types of bifurcation points as a function of two model parameters. A detailed mathematical description of the methods involved in computing equilibrium curves can be found in Sanz & Getto (2016).

* **Analysis of evolutionary fixed points**: During the computation of equilibrium curves of a non-linear PSPM the software also can check whether an evolutionary singular point as defined by Adaptive Dynamics or ESS-theory (Dieckmann, 1997; Metz et al., 1996) is encountered. These singular points are subsequently classified as either a convergent stable strategy (CSS), an evolutionary branching point (EBP) or an evolutionary repellor (ERP) (Geritz et al., 1998). The software can also compute the value of a detected evolutionary singular point as a function of a second model parameter and can, starting from a detected evolutionary singular point, compute the pairwise invasibility plot (Dieckmann, 1997; Metz et al., 1996).

* **Ecological dynamics simulation**: The ecological dynamics of PSPM can be computed using the *Escalator Boxcar Train* (De Roos, 1988; De Roos et al., 1992), a numerical method especially designed for numerical integration of the partial differential equations that are the mathematical representations of PSPMs. A separate software package, [EBTtool](https://staff.fnwi.uva.nl/a.m.deroos/EBT/index.html), for computing the ecological dynamics of PSPMs has been available already for many years. The [EBTtool](https://staff.fnwi.uva.nl/a.m.deroos/EBT/index.html) consists of a graphical user interface including extensive plotting capabilities and a computational engine. A trimmed down version of this computational engine is included in the PSPManalysis package.

* **Evolutionary dynamics simulation**: The dynamics of life history trait values, which in the model occur as parameters, can be simulated over evolutionary time scales, using the canonical equation for adaptive dynamics as explained in (Dieckmann & Law, 1996). These evolutionary dynamic simulations are based on the assumption that the system approaches an ecological equilibrium in between mutation events, which change the value of the life history trait. The evolutionary rate of change is proportional to the selection gradient in the ecological equilibrium and the population birth rate.

The software package consists of a collection of routines implemented in ``C`` with front-ends that allows the software to be used from ``R``. The implementation of the elements of the PSPM under study can be programmed in either ``R`` or ``C`` using the template files provided with the package. Implementation of the user-defined ingredients of the PSPM to be analyzed in ``R`` is easier and to most users probably more familiar, but the user should be aware that implementing the user-defined ingredients of the PSPM in ``C`` will decrease computation times by roughly 2 orders of magnitude. Specifying the user-defined model ingredients in ``R`` hence comes at the price of computations being excruciatingly slow. In many cases the added difficulty of using ``C`` will therefore pay off.

This ``R`` package is distributed via CRAN and can be hence be installed using the ``R`` command:

```
install.packages(“PSPManalysis”)
```
The ``R`` package contains a very detailed manual, which discusses the full functionality of the package and illustrates its use with step-by-step instructions. After installing the package an illustration of its functionality can be obtained by running the command:

```
library(PSPManalysis)
demo("Salmon", package = "PSPManalysis", echo = FALSE)
```

The basic methodology to numerically compute the equilibrium of a PSPM has been presented in Kirkilionis et al. (2001) and Diekmann et al. (2003), while De Roos (2008) presented the modification of the latter approach to compute the demographic characteristics of a linear PSPM. Sanz & Getto (2016) provide a mathematical description hwo to compute entire branches of equilibria of a PSPM  as a function of a model parameter.

For more information you can also check out the [presentation](https://staff.fnwi.uva.nl/a.m.deroos/downloads/PSPManalysis/AMdeRoos-PSPManalysis.mp4) I gave about this software package during an ESMTB summerschool in Palermo in 2019.

**Also refer to the [Wiki](https://bitbucket.org/amderoos/pspmanalysis/wiki/Home) document for more information!!**

----

## Acknowledgments

![ERC logo](https://staff.fnwi.uva.nl/a.m.deroos/PSPManalysis/files/logo-erc-2.png)

The development of this software has been made possible by financial support from the European Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement No. 322814

## References
A.M. De Roos, 1988. Numerical methods for structured population models: the Escalator boxcar train. Numer. Meth. Partial Diff. Eqs. 4(3): 173-195.

A.M. De Roos, O. Diekmann & J.A.J. Metz, 1992. Studying the dynamics of structured population models: A versatile technique and its application to Daphnia population dynamics. Amer. Natur. 139: 123-147.

A.M. De Roos, 2008. Demographic analysis of continuous-time life-history models. Ecol. Lett. 11(1): 1-15.

U. Dieckmann, 1997. Can adaptive dynamics invade? Trends in Ecology & Evolution 12 (4): 128–31.

U. Dieckmann & R. Law. 1996. The dynamical theory of coevolution: A derivation from stochastic ecological processes. Journal of Mathematical Biology 34 (5-6): 579–612.

O. Diekmann, M. Gyllenberg & J.A.J. Metz, 2003. Steady-State Analysis of Structured Population Models. Theoretical Population Biology 63 (4): 309-338.

S.A.H. Geritz, E. Kisdi, G. Meszéna, & J.A.J. Metz. 1998. Evolutionarily singular strategies and the adaptive growth and branching of the evolutionary tree. Evolutionary Ecology 12 (1): 35–57.

M.A. Kirkilionis, O. Diekmann, B. Lisser, M. Nool, A.M. De Roos & B.P. Sommeijer, 2001. Numerical continuation of equilibria of physiologically structured population models. I. Theory. Math. Models Meth. Appl. Sci. 11(6): 1101-1127.

J.A.J. Metz & O. Diekmann, 1986. The dynamics of physiologically structured populations. Vol. 68. Lecture Notes in Biomathematics. Springer-Verlag, Heidelberg.

J.A.J. Metz, S.A.H Geritz, G. Meszéna, F.J.A. Jacobs, & J.S. van Heerwaarden. 1996. Adaptive dynamics, a geometrical study of the consequences of nearly faithful reproduction. In Stochastic and Spatial Structures of Dynamical Systems, edited by S.J. van Strien & S.M. Verduyn-Lunel, 183–231. Amsterdam: KNAW Verhandelingen.


J. S. Sanz & P. Getto, 2016. Numerical Bifurcation Analysis of Physiologically Structured Populations: Consumer–Resource, Cannibalistic and Trophic Models. Bull Math Biol 78, 1546–1584.

*****


WHAT'S NEW?
-----------
                                
### PSPManalysis 0.3.4

* Corrected all "Format overflow" and "may be uninitialized" error messages in the code that showed up using gcc (from Rtools) on Windows

### PSPManalysis 0.3.3

* Added a C as well as an R implementation of another example model (Salmon.h and Salmon.h), used in the publication on PSPManalysis

* Added more informative output messages of computations in case no output was generated

* removed the compiler directive "-Wno-format-overflow" from buildSO.R and PSPMecodyn.R as this one is unknown to the clang compiler

* csbread() now returns a list of states when the state argument is of the form "State-4.0" and the value 4.0 occurs more than once in the CSB file

### PSPManalysis 0.3.2

* Corrected return statements without () in `PSPMecodyn.R` and `PSPMevodyn.R` as reported on the CRAN results pages for fedora-clang and fedora-gcc.

* Adapted the building process of the compiled module in `buildSO.R` and `PSPMecodyn.R` to deal with situations in which the current working directory is not the directory in which the model files are stored	

### PSPManalysis 0.3.1

* Updated to version 0.3.1 - Changed compilation command to work with R 4.0.0 under Windows

* Added the manual in bookdown format as a ZIP file 

### PSPManalysis 0.3.0

* Changed the manual to the bookdown version that can be opened in the Rstudio viewer	
