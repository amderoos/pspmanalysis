function [] = PSPMclean()

%
% PSPMclean: Deletes on request all files produced by the PSPManalysis package 
%
% Matlab syntax:
%
%   PSPMclean
%
% Copyright (C) 2015, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%

disp(' ');
disp('Delete all PSPManalysis result files (default) and/or all executables (hit "F") in the current directory?');
disp(' ');

prompt = 'Press "Q" to abort, "F" for a full clean up, any other key to only clean the result files: ';
str = input(prompt, 's');
if isempty(str)
    str = 'Y';
end

if strcmp(str, 'q') || strcmp(str, 'Q')
    disp(' ');
    disp('Delete operation aborted');
    disp(' ');
else
    disp(' ');
    allsrcs = dir('*.h');
    if (strcmp(str, 'f') || strcmp(str, 'F'))
        disp('Deleting all executables in current directory');
        for i = 1:length(allsrcs)
            [cdir, modelname] = fileparts(allsrcs(i).name);
        
            exename = [modelname, 'ecodyn.', mexext];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'ecodyn.mex'];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'evodyn.', mexext];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'evodyn.mex'];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'equi.', mexext];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'equi.mex'];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'demo.', mexext];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'demo.mex'];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'ind.', mexext];
            if exist(exename, 'file')
                delete(exename);
            end
            exename = [modelname, 'ind.mex'];
            if exist(exename, 'file')
                delete(exename);
            end
        end
    end
    
    disp('Deleting all result files in current directory');
    disp(' ');
    for i = 1:length(allsrcs)
        [cdir, modelname] = fileparts(allsrcs(i).name);
        
        resfnames = strcat(modelname, '-', '*', '-', '*');
        delete(strcat(resfnames, '.bif'));
        delete(strcat(resfnames, '.csb'));
        delete(strcat(resfnames, '.err'));
        delete(strcat(resfnames, '.out'));
        delete(strcat(resfnames, '.mat'));
    end
end
end
