function [curvepoints, curvedesc] = PSPMdemo(modelname, varargin)

%
%
% PSPMdemo: Performs demographic analysis of a structured population model
%
% Syntax:
%
%   [curvepoints, curvedesc] = ...
%       PSPMdemo(modelname, curvepars, parameters, options, 'clean', 'force', 'debug')
%
% Arguments:
%
%   modelname:   (string, required)
%                Basename of the file with model specification. The file
%                should have an extension '.m' or '.h'. For example, the model 'Medfly'
%                is specified in the file 'Medfly.m' or 'Medfly.h'
%
%   curvepars:   (row vector, optional, can be the empty vector [])
%                Vector of length 5, specifying:
%
%                curvepars(1): the index of the parameter to vary
%                curvepars(2): the initial value of the parameter
%                curvepars(3): the step size in the parameter value
%                curvepars(4): lower threshold, below which value of the
%                              parameter the computation stops
%                curvepars(5): upper threshold, above which value of the
%                              parameter the computation stops
%
%   parameters:  (row vector, required, but can be the empty vector [])
%                Vector of a length equal to the length of the variable
%                'DefaultParameters' in case the model is specified in Matlab
%                (in a '.m' file) or equal to the constant PARAMETER_NR in case
%                the model is specified in C (in a '.h' file). The vector
%                specifies the values for the model parameters to
%                use in the computation. Vectors of other lengths, including
%                an empty vector will be ignored.
%
%   options:     (cell array, optional, can be the empty cell array {})
%                Cell array with pairs of an option name and a value (for
%                example {'isort', '1'}) or single options (i.e. 'test').
%                Possible option names and their values are:
%
%                'isort',  '<index>': Index of i-state variable to use as
%                                     ruling variable for sorting the
%                                     structured populations
%                "report", "<value>": Interval between consecutive output of 
%                                     computed points to the console ( >= 1).
%                                     Minimum value of 1 implies output of 
%                                     every point
%                'test'             : Perform only a single integration over
%                                     the life history, reporting dynamics
%                                     of survival, R0 and i-state variables
%
%   'clean':     (string. optional argument)
%                Remove all the result files of the model before the
%                computation
%
%   'force':     (string, optional argument)
%                Force a rebuilding of the model before the computation
%
%   'debug':     (string, optional argument)
%                Compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   curvepoints: Matrix with output for all computed points along the curve
%
%   curvedesc:   Column vector with strings, summarizing the numerical details
%                of the computed curve (i.e., initial point, parameter values,
%                numerical settings used).
%
% Copyright (C) 2017, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%



forcebuild = 0;
debugging = 0;
nVarargs = length(varargin);
myArgc = 0;
parameters = [];
options = {};

curvepoints = [];
curvedesc = [];
curvepars = [];

% Deduce the basename of the executable
if ((modelname(length(modelname)-1) == '.') && ((modelname(length(modelname)) == 'h') || (modelname(length(modelname)) == 'm')))
    basename = modelname(1:length(modelname)-2);
else
    basename = modelname;
end

% Process the variable arguments
for k = 1:nVarargs
    if ischar(varargin{k})
        if strcmp(varargin{k}, 'clean')
            resfnames = strcat(basename, '-', '*', '-', '*');
            delete(strcat(resfnames, '.err'));
            delete(strcat(resfnames, '.out'));
            delete(strcat(resfnames, '.mat'));
        elseif strcmp(varargin{k}, 'force') || strcmp(varargin{k}, 'forcebuild')
            forcebuild = 1;
        elseif strcmp(varargin{k}, 'debug')
            debugging = 1;
        else
            msg = ['Unknown function argument: ', varargin{k}];
            disp(' ');
            disp(msg);
            disp(' ');
        end
    else
        switch myArgc
            case 0
                curvepars = varargin{k};
            case 1
                parameters = varargin{k};
            case 2
                options = varargin{k};
        end
        myArgc = myArgc + 1;
    end
end

% Consistency checks of other arguments
if (length(curvepars) && ((length(curvepars) ~= 5) || (~isa(curvepars,'double'))))
    error('If specified the curve parameter argument should be a vector of length 5 with double values');
end
if (length(parameters) && (~isa(parameters,'double'))) 
    error('If specified parameter values should be a vector with double values');
end
if (~isa(options,'cell'))
    error('If specified options should be a cell array');
end

% Build the executable
[Mmodel exebasename] = buildSO('PSPMdemo', modelname, forcebuild, debugging);
if (~exist(exebasename, 'file'))
    msg = ['Executable ', exebasename, ' not found'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end

drawnow;

fname = feval(exebasename, curvepars, parameters, options);

if (Mmodel == 1)
    clear global PopulationNr IStateDimension LifeHistoryStages ImpactDimension
    clear global NumericalOptions EnvironmentState DefaultParameters
    clear global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc DiscreteChangesFunc EnvEquiFunc
end

if (~isempty(fname))
    if exist(strcat(fname, '.out'), 'file')
        fid = fopen(strcat(fname, '.out'));
        line = fgetl(fid);
        headerlines = 0;
        HasData = 0;
        while ischar(line)
            if line(1) ~= '#'
                HasData = 1;
                break;
            end
            headerlines = headerlines + 1;
            curvedesc = [curvedesc '\n' line];
            line = fgetl(fid);
        end
        fclose(fid);
        curvedesc = [curvedesc '\n'];
        fprintf(curvedesc);

        if (HasData)
            curvepoints = dlmread(strcat(fname, '.out'), '', headerlines, 0);
        end
    end
end
end
