/***
   NAME
     csb2mat
   DESCRIPTION
     Program lists the contents of a binary CSB file or converts a single state from this file
     to a Matlab structure.

   Last modification: AMdR - Jun 07, 2017
***/
#ifndef CSB2MAT
#define CSB2MAT
#endif

#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include <sys/param.h>

#include "mex.h"
#include "matrix.h"
#include "mat.h"

// Type definition for environment and population
typedef struct envdim
{
  double   timeval;
  int      columns;
  int      data_offset;
  uint32_t memory_used;
} Envdim;

typedef struct popdim
{
  double timeval;
  int    population;
  int    cohorts;
  int    columns;
  int    data_offset;
  int    lastpopdim;
} Popdim;

/*==================================================================================================================================*/
static void     *mem_base = NULL;
static double   *parVals  = NULL;
static FILE     *fp       = NULL;

#define FIELDNAMELN               40

static void FreeMemory(void)
{
  if (mem_base) free(mem_base);
  mem_base = NULL;
  if (parVals) free(parVals);
  parVals = NULL;
  if (fp) fclose(fp);
  fp = NULL;

  return;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{
  char     tmpstr[256], fname[MAXPATHLEN], *input_buf, *cp;
  int      parDim, file_ok = 1, popinfile = 0, popindex;
  double   timeval = -1.0, closest_time = HUGE_VAL, tmpval;
  uint32_t filepos = 0, closest_fpos = 0, magic;
  Envdim   cur_env;
  long     MemAllocated = 0L;

  // check for proper number of arguments
  if ((nrhs != 1) && (nrhs != 3))
    mexErrMsgIdAndTxt("MATLAB:csb2mat:nrhs", "\nIncorrect number of command-line arguments.\n\nUse: %s(<CSB file name> [, index or time])\n\n",
                      mexFunctionName());

  // First argument must be a string
  if (mxIsChar(prhs[0]) != 1) mexErrMsgIdAndTxt("MATLAB:csb2mat:inputNotString", "First argument must be a character type.");

  // input must be a row vector
  if (mxGetM(prhs[0]) != 1) mexErrMsgIdAndTxt("MATLAB:csb2mat:inputNotVector", "First argument must be a string.");

  // copy the string data from prhs[0] into a C string fname.
  input_buf = mxArrayToString(prhs[0]);

  if (input_buf == NULL) mexErrMsgIdAndTxt("MATLAB:csb2mat:conversionFailed", "Could not convert first argument to string.");

  strcpy(fname, input_buf);

  if (nrhs == 3)
    {
      if (!mxIsInt32(prhs[1]) || (mxGetM(prhs[1]) != 1) || (mxGetN(prhs[1]) != 1))
        mexErrMsgIdAndTxt("MATLAB:csb2mat:inputNotVector", "Second argument must be a single integer value.");
      popindex = mxGetScalar(prhs[1]);

      if (!mxIsDouble(prhs[2]) || (mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1))
        mexErrMsgIdAndTxt("MATLAB:csb2mat:inputNotVector", "Third argument must be a single double value.");
      timeval =*mxGetPr(prhs[2]);
    }


  // check for proper number of output variables
  if (nlhs != 1) mexErrMsgIdAndTxt("MATLAB:csb2mat:nlhs", "\nA single output argument is required, not %d!", nlhs);

  mexAtExit(FreeMemory);

  fp = fopen(fname, "rb");                                                          // Open CSB file, error checking occurs in Matlab

  if (fread(&magic, sizeof(uint32_t), 1, fp) != 1) mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nCSB file does not contain any data!\n\n");

  file_ok = (fread(&parDim, sizeof(int), 1, fp) == 1);
  if (file_ok)
    {
      parVals = (double *)calloc(parDim, sizeof(double));
      file_ok = (fread(parVals, sizeof(double), (size_t)parDim, fp) == (size_t)parDim);
    }
  if (!file_ok)
    {
#if (DEBUG == 1)
      for (int i = 0; i < parDim; i++) mexPrintf("parameter[%d] = %G\n", i, parVals[i]);
#endif
      mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nCSB file seems to be invalid or corrupt!\n\n");
    }
  filepos = ftell(fp);

  if (nrhs == 1) mexPrintf("\nStates in file %s:\n\n", fname);
  while (fread(&cur_env, sizeof(Envdim), 1, fp) == 1)
    {
      fseek(fp, filepos, SEEK_SET);

      if (cur_env.memory_used > MemAllocated)
        {
          mem_base = (void *)realloc(mem_base, (size_t)cur_env.memory_used);
          if (!mem_base) mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nCould not allocate memory to read in data!\n\n");
          MemAllocated = cur_env.memory_used;
        }

      if (fread(mem_base, 1, (size_t)cur_env.memory_used, fp) != (size_t)cur_env.memory_used)
        mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nFailed to read values from file: %s!\n\n", fname);

      popinfile++;
      if (nrhs == 3)
        {
          if (popindex >= 0)
            {
              if (popindex == popinfile)
                {
                  closest_fpos = filepos;
                  break;
                }
            }
          else if (fabs(cur_env.timeval - timeval) < fabs(closest_time - timeval))
            {
              closest_time = cur_env.timeval;
              closest_fpos = filepos;
            }
        }
      else if (nrhs == 1)
        {
          sprintf(tmpstr, "%5d: State-%.6E", popinfile, cur_env.timeval);
          mexPrintf("%s\n", tmpstr);
        }

      fseek(fp, filepos, SEEK_SET);
      fseek(fp, cur_env.memory_used, SEEK_CUR);
      filepos = ftell(fp);
    }

  if (nrhs == 1)
    {
      mexPrintf("\n");
      plhs[0] = mxCreateStructMatrix(0, 0, 0, NULL);
    }
  else if (popindex > popinfile)
    {
      mexErrMsgIdAndTxt("MATLAB:csb2mat", "No such population state in file %s: index out of bounds. Index should be in the range 1-%d\n\n", fname,
                        popinfile);
      plhs[0] = mxCreateStructMatrix(0, 0, 0, NULL);
    }
  else if ((nrhs == 3) && (closest_fpos > (uint32_t)0))
    {
      register int n, i, j, k;
      double *     cdbl, *base_pnt, *poppnt;
      Envdim *     cenv;
      Popdim *     cpop;
      int          npops = 0, nprotect = 0, nalloc = 0, mag, num, indx;
      int          changedParsDim;
      int          demo = 0, evodyn = 0, evopars = 0, bifpar1 = 0, bifpar2 = 0;

      fseek(fp, closest_fpos, SEEK_SET);
      if (fread(&cur_env, sizeof(Envdim), 1, fp) != 1) mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nFailed to read values from file: %s!\n\n", fname);

      fseek(fp, closest_fpos, SEEK_SET);
      if (fread(mem_base, 1, (size_t)cur_env.memory_used, fp) != (size_t)cur_env.memory_used)
        mexErrMsgIdAndTxt("MATLAB:csb2mat", "\nFailed to read values from file: %s!\n\n", fname);

      cenv     = (Envdim *)mem_base;
      base_pnt = ((double *)mem_base + cenv->data_offset);

      // Count the populations
      cdbl = ((double *)mem_base + cenv->data_offset + cenv->columns);
      cpop = (Popdim *)cdbl;
      while (1)
        {
          npops++;
          if (cpop->lastpopdim) break;
          cdbl = (((double *)cpop) + cpop->data_offset + (cpop->cohorts*cpop->columns));
          cpop = (Popdim *)cdbl;
        }
      cdbl = ((double *)mem_base + cenv->data_offset + cenv->columns);
      cpop = (Popdim *)cdbl;

      demo    = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "PGR") != NULL);
      evodyn  = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "evolutionary time") != NULL);
      evopars = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "evolutionary parameters for parameter value") != NULL);
      bifpar1 = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "for parameter value ") != NULL);
      bifpar2 = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "for parameter values") != NULL);

      // Adjust the values of the bifurcation parameters
      changedParsDim = (int)floor(parVals[parDim - 1] + 0.5);
      for (i = 0; i < changedParsDim; i++)
        {
          indx = (int)floor(parVals[parDim - 1 - changedParsDim + i] + 0.5);
          if ((changedParsDim == 2) && (i == 1) && (indx == ((int)floor(parVals[parDim - 1 - changedParsDim] + 0.5))))
            continue;                                                                // PIP results
          parVals[indx] = base_pnt[cenv->columns - changedParsDim + i];
        }
      parDim -= changedParsDim + 1;

      {
        const int totalalloc = npops + 5;
        mxArray * mydata[totalalloc];
        char *    fieldnames[totalalloc];

        for (j = 0; j < totalalloc; j++)
          {
            mydata[j]     = NULL;
            fieldnames[j] = NULL;
          }

        nalloc = 0;
        if (evodyn)
          {
            mydata[nalloc]     = mxCreateDoubleMatrix(1, 1, mxREAL);
            fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
            memcpy(mxGetPr(mydata[nalloc]), &(cenv->timeval), sizeof(double));
            memcpy((void *)fieldnames[nalloc], "EvoTime", sizeof("EvoTime"));
            nalloc++;
          }
        else if (bifpar1)
          {
            mydata[nalloc]     = mxCreateDoubleMatrix(1, 1, mxREAL);
            fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
            memcpy(mxGetPr(mydata[nalloc]), &(base_pnt[cenv->columns - changedParsDim]), sizeof(double));
            memcpy((void *)fieldnames[nalloc], "BifPars", sizeof("BifPars"));
            nalloc++;
          }
        else if (bifpar2)
          {
            mydata[nalloc]     = mxCreateDoubleMatrix(1, changedParsDim, mxREAL);
            fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
            memcpy(mxGetPr(mydata[nalloc]), &(base_pnt[cenv->columns - changedParsDim]), changedParsDim*sizeof(double));
            memcpy((void *)fieldnames[nalloc], "BifPars", sizeof("BifPars"));
            nalloc++;
          }
        if (evodyn || evopars)
          {
            mydata[nalloc]     = mxCreateDoubleMatrix(1, changedParsDim - evopars, mxREAL);
            fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
            // In case of EVODYN start at i=0, otherwise skip the first (bifurcation) parameter
            memcpy(mxGetPr(mydata[nalloc]), &(base_pnt[cenv->columns - changedParsDim + evopars]), (changedParsDim - evopars)*sizeof(double));
            memcpy((void *)fieldnames[nalloc], "EvoPars", sizeof("EvoPars"));
            nalloc++;
          }

        mydata[nalloc]     = mxCreateDoubleMatrix(1, parDim, mxREAL);
        fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
        memcpy(mxGetPr(mydata[nalloc]), parVals, parDim*sizeof(double));
        memcpy((void *)fieldnames[nalloc], "Parameters", sizeof("Parameters"));
        nalloc++;

        strcpy(tmpstr, (char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)));
        cp          = strstr(tmpstr, ": ");
        if (cp) *cp = '\0';

        mydata[nalloc]     = mxCreateDoubleMatrix(1, cenv->columns - changedParsDim, mxREAL);
        fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
        memcpy(mxGetPr(mydata[nalloc]), base_pnt, (cenv->columns - changedParsDim)*sizeof(double));
        strcpy(fieldnames[nalloc], tmpstr);
        nalloc++;

        for (n = 0; n < npops; n++)
          {
            strcpy(tmpstr, (char *)(((double *)cpop) + (sizeof(Popdim)/sizeof(double) + 1)));
            cp          = strstr(tmpstr, ": ");
            if (cp) *cp = '\0';

            base_pnt           = ((double *)cpop + cpop->data_offset);
            mydata[nalloc]     = mxCreateDoubleMatrix(cpop->cohorts, cpop->columns, mxREAL);
            poppnt             = mxGetPr(mydata[nalloc]);
            fieldnames[nalloc] = (char *)mxCalloc(FIELDNAMELN, sizeof(char));
            strcpy(fieldnames[nalloc], tmpstr);
            nalloc++;

            for (j = 0; j < cpop->cohorts; j++)
              for (k = 0; k < cpop->columns; k++) poppnt[k*cpop->cohorts + j] =*(base_pnt + k*cpop->cohorts + j);

            if (cpop->lastpopdim) break;

            cdbl = (((double *)cpop) + cpop->data_offset + (cpop->cohorts*cpop->columns));
            cpop = (Popdim *)cdbl;
          }

        plhs[0] = mxCreateStructMatrix(1, 1, nalloc, (const char **)fieldnames);

        for (j = 0; j < nalloc; j++) mxSetFieldByNumber(plhs[0], 0, j, mydata[j]);
        for (j = 0; j < nalloc; j++)
          {
            //  if (mydata[j]) mxFree(mydata[j]);                                   // DO NOT DELETE< DESTROYS THE CONTENTS OF THE STRUCTURE
            if (fieldnames[j]) mxFree(fieldnames[j]);
          }
      }

#if (DEBUG == 1)
      double output;

      cdbl = ((double *)mem_base + cenv->data_offset + cenv->columns);
      cpop = (Popdim *)cdbl;

      // The description
      (void)mexPrintf("# %s\n", (char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)));
      for (i = 0; i < cenv->columns; i++) /* Write environment state  */
        {
          if (i == 0)
            output = base_pnt[i];
          else
            {
              (void)mexPrintf("\t");
              output = base_pnt[i];
            }
          if (((fabs(output) <= 1.0E4) && (fabs(output) >= 1.0E-4)) || (output == 0))
            (void)mexPrintf("%.10f", output);
          else
            (void)mexPrintf("%.6E", output);
        }
      (void)mexPrintf("\n\n");

      /* Write population state   */
      while (1)
        {
          // Description
          (void)mexPrintf("# %s\n", (char *)(((double *)cpop) + (sizeof(Popdim)/sizeof(double) + 1)));
          base_pnt = ((double *)cpop + cpop->data_offset);
          for (j = 0; j < cpop->cohorts; j++, (void)mexPrintf("\n"))
            for (k = 0; k < cpop->columns; k++)
              {
                if (k > 0) (void)mexPrintf("\t");
                output =*(base_pnt + k*cpop->cohorts + j);
                if (((fabs(output) <= 1.0E4) && (fabs(output) >= 1.0E-4)) || (output == 0))
                  (void)mexPrintf("%.10f", output);
                else
                  (void)mexPrintf("%.6E", output);
              }
          (void)mexPrintf("\n");
          if (cpop->lastpopdim) break;

          cdbl = (((double *)cpop) + cpop->data_offset + (cpop->cohorts*cpop->columns));
          cpop = (Popdim *)cdbl;
        }
#endif
    }

  return;
}


/*==================================================================================================================================*/
