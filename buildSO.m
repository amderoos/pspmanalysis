function [Mmodel exebasename] = buidSO(PSPMmodule, modelname, forcebuild, debugging)

%
%
% buildSO: Compiles a particular PSPM executable from the header file and the 
%          corresponding PSPM module (PSPMdemo, PSPMequi, PSPMevodyn or PSPMind) 
%
% Syntax:
%
%   [Mmodel exebasename] = buildSO(modelname, forcebuild, debugging)
%
% Arguments:
%
%   modelname: (string, required)
%               Basename of the file with model specification.
%
%   forcebuild: (boolean, required)
%               If true, force a rebuilding of the model before the computation
%
%   debugging:  (boolean, required)
%               if true, compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   exebasename:  Basename of the compiled executable
%
% Copyright (C) 2017, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%

if ((~length(modelname)) || (~ischar(modelname)))
  disp(' ');
  disp('You have to specify a model name');
  disp(' ');
  exebasename = '';
  if is_octave
      fflush(stdout);
  end
  return;
end

mmodel = 0;
if (strcmp(modelname(length(modelname)-1:end), '.h'))
    hfile = modelname;
    mmodel = 0;
elseif (strcmp(modelname(length(modelname)-1:end), '.m'))
    hfile = modelname;
    mmodel = 1;
elseif exist([modelname '.h'], 'file')
    hfile = [modelname, '.h'];
    mmodel = 0;
elseif exist([modelname '.m'], 'file')
    hfile = [modelname, '.m'];
    mmodel = 1;
end
Mmodel = mmodel;

if ~exist(hfile, 'file')
    msg = ['No model source file named ', hfile, ' can be found'];
    disp(' ');
    disp(msg);
    disp(' ');
    exebasename = '';
    if is_octave
        fflush(stdout);
    end
    return;
end

if (mmodel == 1)
    clear global PopulationNr IStateDimension LifeHistoryStages ImpactDimension
    clear global NumericalOptions EnvironmentState DefaultParameters
    clear global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc DiscreteChangesFunc EnvEquiFunc
    run(hfile)
end

fullfilename = mfilename('fullpath');
coredir = fileparts(fullfilename);
is_octave = exist ('OCTAVE_VERSION', 'builtin');

module = strrep(PSPMmodule, 'PSPM', '');

basename = hfile(1:length(hfile)-2);
exebasename = [basename, module];
ext = mexext;
exename = [exebasename, '.', ext];

if (mislocked(exebasename))
    munlock(exebasename);
end
clear(exebasename);
clear mex;

% Check the date on the executable file
build = ~exist(exename, 'file');
if ~build
    d1 = dir(hfile);
    d2 = dir(exename);
    build = d1.datenum > d2.datenum;
end

% Check the date on the library file PSPManalysis.libM.o
curwd = pwd;
cd(coredir);
buildlib = ~exist('PSPManalysis.libM.o', 'file');
if ~buildlib
    d0 = dir('PSPManalysis.libM.o');
    d1 = dir('biftest.c');
    buildlib = buildlib || (d1.datenum > d0.datenum);
    d1 = dir('curve.c');
    buildlib = buildlib || (d1.datenum > d0.datenum);
    d1 = dir('io.c');
    buildlib = buildlib || (d1.datenum > d0.datenum);
end
cd(curwd);

if build || buildlib || forcebuild
    if exist(exename, 'file')
        delete(exename);
    end
    msg = ['Building executable ', exename, ' using sources from ', coredir, ' ...'];
    disp(' ');
    disp(msg);
    disp(' ');
    if (is_octave)
        % To build the file using Octave, only tested on Mac OS:
        if debugging
            optstring = ' -Wall -g -v -DDEBUG=1';
        else
            optstring = '';
        end
        outstring   = [' -o ' exename ];
        cflagstring = ' -DOCTAVE_MEX_FILE';
        lapacklib   = ' -llapack';
        blaslib     = ' -lblas';
        utlib       = '';
    else
        % To build the file using Matlab
        outstring   = [' -output ' exename ];
        if strcmp(ext, 'mexw64')
            % To build the file on Windows:
            cpcmd = 'type';
            if debugging
                optstring = ' -g -v  -largeArrayDims -DDEBUG=1';
                cflagstring = ' COMPFLAGS="$COMPFLAGS -Wall"';
            else
                optstring = ' -O  -largeArrayDims';
                cflagstring = ' COMPFLAGS="$COMPFLAGS"';
            end
            lapacklib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libmwlapack.lib') '"'];
            blaslib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libmwblas.lib') '"'];
            utlib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libut.lib') '"'];
        else
            % To build the file on UNIX:
            cpcmd = 'cat';
            if debugging
                optstring = ' -g -v  -largeArrayDims -DDEBUG=1';
                cflagstring = ' CFLAGS=''\$CFLAGS -Wall''';
            else
                optstring = ' -O  -largeArrayDims';
                cflagstring = ' CFLAGS=''\$CFLAGS''';
            end
            lapacklib   = ' -lmwlapack';
            blaslib     = ' -lmwblas';
            utlib       = ' -lut';
        end
    end

    % Build the library file PSPManalysis.libM.o if needed
    if (buildlib)
        cd(coredir);
        system([cpcmd ' "biftest.c"' ' "curve.c"' ' "io.c" > "PSPManalysis.libM.c"']);
        cmd = ['mex ' optstring ' -c' cflagstring];
        cmd = [cmd ' -I. "PSPManalysis.libM.c"'];
        if debugging
            disp('');
            disp('Command line:');
            disp('');
            disp(cmd);
            disp('');
        end
        eval(cmd);
        delete('PSPManalysis.libM.c');
        cd(curwd);
    end

    if (mmodel == 1)
        globvars = who('global');
        global PopulationNr IStateDimension LifeHistoryStages;
        global NumericalOptions DefaultParameters;
        global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc;

        modelstring = ['-DMFUNCTIONS=1 '];
        modelstring = [modelstring ' -DPOPULATION_NR=' num2str(PopulationNr) ];
        modelstring = [modelstring ' -DSTAGES=' num2str(LifeHistoryStages) ];
        modelstring = [modelstring ' -DI_STATE_DIM=' num2str(IStateDimension) ];
        if strmatch('ImpactDimension', globvars)
            global ImpactDimension;
            modelstring = [modelstring ' -DINTERACT_DIM=' num2str(ImpactDimension) ];
        end
        if strmatch('EnvironmentState', globvars)
            global EnvironmentState;
            modelstring = [modelstring ' -DENVIRON_DIM=' num2str(length(fields(EnvironmentState))) ];
        end
        modelstring = [modelstring ' -DPARAMETER_NR=' num2str(length(fields(DefaultParameters))) ];
        if strmatch('DiscreteChangesFunc', globvars)
            global DiscreteChangesFunc;
            modelstring = [modelstring ' -DDISCRETECHANGES=1' ];
        else
            modelstring = [modelstring ' -DDISCRETECHANGES=0' ];
        end
        if strmatch('EnvEquiFunc', globvars)
            global EnvEquiFunc;
        end
        if exist('NumericalOptions', 'var')
            for i = 1:length(NumericalOptions)
              modelstring = [modelstring ' -D' char(NumericalOptions(i))];
            end
        end

    else
        modelstring = ['-DMFUNCTIONS=0 -DPROBLEMHEADER=' hfile ];
    end

    cmd = ['mex' optstring outstring cflagstring ' ' modelstring];
    cmd = [cmd ' -I. -I"' coredir '"'];
    
    cmd = [cmd ' "' coredir '/PSPM' module '.c"'];
    cmd = [cmd ' "' coredir '/PSPManalysis.libM.o"'];
    cmd = [cmd lapacklib blaslib utlib ];
    
    if debugging
        disp('');
        disp('Command line:');
        disp('');
        disp(cmd);
        disp('');
    end
    
    eval(cmd);
    if ~exist(exename, 'file')
        msg = ['Compilation of file ', exename, ' failed'];
        disp(' ');
        disp(msg);
        disp(' ');
        exebasename = '';
        if is_octave
            fflush(stdout);
        end
        return;
    end
    if is_octave && ~debugging
        delete(['PSPM' module '.o']);
        delete('biftest.o');
        delete('curve.o');
        delete('io.o');
    end
    if is_octave
        fflush(stdout);
    end
    return;
else
    msg = ['Executable ', exename, ' is up-to-date'];
    disp(' ');
    disp(msg);
    disp(' ');
    feval('munlock', exebasename);
    feval('clear', exebasename);
    if is_octave
        fflush(stdout);
    end
    return;
end

