# Generic Makefile for the compilation of a continuation problem of the class
# of models that can be analyzed with the generic program files PSPMdemo.c
# PSPMecodyn.c, PSPMequi.c, PSPMecodyn.c or PSPMind.c
#
# Possible targets to make:
#
#       make <model>demo		(builds demographic executable from .h file)
#       make <model>ecodyn	(builds EBT executable from .h file)
#       make <model>equi		(builds equilibrium executable from .h file)
#       make <model>evodyn	(builds evolutionary executable from .h file)
#       make <model>ind			(builds individual executable from .h file)
#
#       make clean                      (cleans up all target programs)
#       make cleanoutput                (cleans up all error and output files)
#       make allclean                   (combines previous two)
#
#
#==============================================================================
# Specify where the library modules are located (ADAPT TO YOUR INSTALLATION)
#==============================================================================

MODDIR = $(HOME)/programs/PSPManalysis

#==============================================================================
# Specify the optional compilation settings and uses of libraries:
#
# CLANG    : Use (1)/Don't use (0) Mac OS X clang compiler (for Mac OS X only)
# ICC      : Use (1)/Don't use (0) Intel's icc compiler (for Linux only)
# MKL      : Use (1)/Don't use (0) Intel's MKL libraries (for Linux only)
#
# Best settings:
#
# Linux    : ICC = 1, MKL = 0 
# Mac OS X : CLANG = 1 (to use clang)
#            CLANG = 0 (to use gcc)
#
#==============================================================================

CLANG    = 1
ICC      = 1
MKL      = 0

#==============================================================================
#  Compilation settings: These might need tinkering for your system
#==============================================================================

WARN       = -Wall
INCLUDES   = -I. -I$(MODDIR)
LDFLAGS    = -fno-common
STDLIBS    = -lm

# Debugging on a Mac OS/Yosemite Macbook Pro using gcc & ddd from macport
ifeq ("$(DEBUG)", $(filter "$(DEBUG)","debug" "1"))
CC        ?= gcc
WARN      +=  -Wpointer-arith -Wcast-qual -Wcast-align
CFLAGS     = -std=gnu99 -ggdb -g3 -DDEBUG=1 -flax-vector-conversions $(INCLUDES) $(WARN)
OPENMP    := $(shell TMPFILE=`mktemp`; mv $${TMPFILE} $${TMPFILE}.c; $(CC) -fopenmp -c $${TMPFILE}.c 2>/dev/null ; echo $$? ; rm -f $${TMPFILE}.?)
ifeq ($(OPENMP),0)
CFLAGS    += -fopenmp
endif
TMPDIR     = .
LAPACKLIBS = -llapack -lcblas

# Non-debugging configuration for Mac OS and Linux
else

TMPDIR     = /tmp

# Detect the OS: Command-line version only tested on Mac OS and Linux
UNAME_S   := $(shell uname -s)

# Mac OS settings
ifeq ($(UNAME_S),Darwin)
WARN      += -Wpointer-arith -Wcast-qual -Wcast-align
CFLAGS     = -std=gnu99 -O3 $(INCLUDES) $(WARN)
ifeq ("$(CLANG)", "1")
CC        ?= clang
else
CC        ?= gcc
CFLAGS    += -flax-vector-conversions
endif
OPENMP    := $(shell TMPFILE=`mktemp`; mv $${TMPFILE} $${TMPFILE}.c; $(CC) -fopenmp -c $${TMPFILE}.c 2>/dev/null ; echo $$? ; rm -f $${TMPFILE}.?)
ifeq ($(OPENMP),0)
CFLAGS    += -fopenmp
endif
TMPDIR     = /tmp
LAPACKLIBS = -llapack -lcblas
endif

# Linux settings
ifeq ($(UNAME_S),Linux)

# If selected test whether icc is available
ifeq ("$(ICC)", "1")
ICC_AVAIL := $(shell icc -v 1>&2 2> /dev/null; echo $$?)
endif

#=========== Use gcc: icc can not be executed or is not selected
ifneq ($(ICC_AVAIL),0)
CC        ?= gcc
WARN      +=  -Wpointer-arith -Wcast-qual -Wcast-align
CFLAGS     = -O3 $(INCLUDES) $(WARN)
OPENMP    := $(shell TMPFILE=`mktemp`; mv $${TMPFILE} $${TMPFILE}.c; $(CC) -fopenmp -c $${TMPFILE}.c 2>/dev/null ; echo $$? ; rm -f $${TMPFILE}.?)
ifeq ($(OPENMP),0)
CFLAGS    += -fopenmp
endif

#=========== Use icc: icc can be executed and is preferred
else
CC        ?= icc
# CFLAGS     = -O3 -ipo -no-prec-div -xHost $(INCLUDES) $(WARN)
CFLAGS     = -std=gnu99 -O3 -ipo -axAVX -msse3 $(INCLUDES) $(WARN)
OPENMP    := $(shell TMPFILE=`mktemp`; mv $${TMPFILE} $${TMPFILE}.c; $(CC) -openmp -c $${TMPFILE}.c 2>/dev/null ; echo $$? ; rm -f $${TMPFILE}.?)
ifeq ($(OPENMP),0)
CFLAGS    += -openmp
endif
WARN      += -w1 -Wcheck
endif
# End of ICC_AVAIL

# If selected use MKL, otherwise Lapack
ifeq ("$(MKL)", "1")
LAPACKLIBS = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread
else
LAPACKLIBS = -llapack -lcblas -llapack_atlas -latlas
endif

endif
# End of Linux

endif
# End of debugging/non-debugging

#==============================================================================
# Advanced tayloring
# The following settings might need adaptation for specific uses
#==============================================================================

# Determining the names of all potential target programs
# All .h files in the current directory are considered target programs

ALLHFILES   = $(wildcard *.h)
ALLPROBLEMS = $(patsubst %.h,%,$(ALLHFILES))

# Alternatively, if not all .h files in the current directory are target programs
# specify the targets here:

# ALLPROBLEMS = <model1> <model2>

#==============================================================================
# About the modules
#==============================================================================

MODULES    = biftest curve io

DEBUGOBJS  = $(patsubst %,./%.o,$(MODULES))
MODOBJS    = $(patsubst %,$(TMPDIR)/%.o,$(MODULES))
MODSRCS    = $(patsubst %,$(MODDIR)/%.c,$(MODULES))

EBTMODULES = ebtcohrt ebtdopri5 ebtinit ebtutils
EBTSRCS    = $(patsubst %,$(MODDIR)/escbox/%.c,$(EBTMODULES))

#==============================================================================
# Defining the make targets for the csb2txt program and to clean up
#==============================================================================

csb2txt: csb2txt.c
	$(CC) $(CFLAGS) -o $@ $< -lm
	@rm -Rf csb2txt.dSYM 

allclean: clean cleanoutput

clean: 
	@echo "Cleaning up all programs: "
	@for I in $(ALLPROBLEMS) ; do echo "Cleaning up $${I}...."; rm -f $${I}demo $${I}ecodyn $${I}equi $${I}evodyn $${I}ind $${I}.o $(DEBUGOBJS); rm -Rf $${I}demo.dSYM $${I}ecodyn.dSYM $${I}equi.dSYM $${I}evodyn.dSYM $${I}ind.dSYM $${I}demo.*.dSYM $${I}ecodyn.*.dSYM $${I}equi.*.dSYM $${I}evodyn.*.dSYM $${I}ind.*.dSYM; done
	@rm -f *.mexmaci64 *.mexw64 *.mexa64 *.mex *.o *.m~ *.so *.dll
	@/bin/rm -f $(MODDIR)/PSPManalysis.lib.o
	@/bin/rm -f csb2txt csb2rlist.so csb2rlist.dll

cleanoutput:
	@echo "Cleaning up all error and output files: "
	@for I in $(ALLPROBLEMS) ; do rm -f $${I}*.bif $${I}*.csb $${I}*.err $${I}*.mat $${I}*.out ; done

#==============================================================================
# The dependencies of the executables, the problem-specific object file
# and the module library file
#==============================================================================

.SECONDARY:

ifeq ("$(DEBUG)", $(filter "$(DEBUG)","debug" "1"))
$(MODDIR)/PSPManalysis.lib.o: $(MODSRCS)
	for I in $(MODULES) ; do $(COMPILE.c) -o $(TMPDIR)/$${I}.o $(MODDIR)/$${I}.c ; done
	ld -r -o $@ $(MODOBJS)
else
$(MODDIR)/PSPManalysis.lib.o: $(MODSRCS)
	for I in $(MODULES) ; do $(COMPILE.c) -o $(TMPDIR)/$${I}.o $(MODDIR)/$${I}.c ; done
	ld -r -o $@ $(MODOBJS)
	rm $(MODOBJS)
endif

%demo: %.h $(MODDIR)/PSPMdemo.c $(MODDIR)/PSPManalysis.lib.o
	$(LINK.c) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/PSPMdemo.c $(MODDIR)/PSPManalysis.lib.o $(LAPACKLIBS) $(EXTRALIBS) $(STDLIBS)

%ind: %.h $(MODDIR)/PSPMind.c $(MODDIR)/PSPManalysis.lib.o
	$(LINK.c) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/PSPMind.c $(MODDIR)/PSPManalysis.lib.o $(LAPACKLIBS) $(EXTRALIBS) $(STDLIBS)

%equi: %.h $(MODDIR)/PSPMequi.c $(MODDIR)/PSPManalysis.lib.o
	$(LINK.c) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/PSPMequi.c $(MODDIR)/PSPManalysis.lib.o $(LAPACKLIBS) $(EXTRALIBS) $(STDLIBS)

%evodyn: %.h $(MODDIR)/PSPMevodyn.c $(MODDIR)/PSPManalysis.lib.o
	$(LINK.c) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/PSPMevodyn.c $(MODDIR)/PSPManalysis.lib.o $(LAPACKLIBS) $(EXTRALIBS) $(STDLIBS)

%ecodyn: %.h $(MODDIR)/PSPMecodyn.c $(EBTSRCS)
	$(eval ENVDIM := $(shell egrep '^\s*\#define\s+ENVIRON_DIM'   $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval ENVDIM := $(shell egrep '^\s*\#define\s+ENVIRON_DIM'   $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval POPDIM := $(shell egrep '^\s*\#define\s+POPULATION_NR' $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval ISTDIM := $(shell egrep '^\s*\#define\s+I_STATE_DIM'   $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval STADIM := $(shell egrep '^\s*\#define\s+STAGES'        $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval PARDIM := $(shell egrep '^\s*\#define\s+PARAMETER_NR'  $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval INTDIM := $(shell egrep '^\s*\#define\s+INTERACT_DIM'  $< | sed 's/^[^0-9]*\([0-9]*\).*/\1/' ))
	$(eval EBTDIMS = -DENVIRON_DIM=$(ENVDIM) -DPOPULATION_NR=$(POPDIM) -DI_STATE_DIM=$(ISTDIM) -DSTAGES=$(STADIM) -DPARAMETER_NR=$(PARDIM) -DINTERACT_DIM=$(INTDIM) )
	$(LINK.c)  -I$(MODDIR)/escbox -I/opt/local/include $(EBTDIMS) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/PSPMecodyn.c $(EBTSRCS) $(STDLIBS) -L/opt/local/lib -lfftw3

#==============================================================================
