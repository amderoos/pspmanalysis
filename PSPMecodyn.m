function [curvepoints, curvedesc] = PSPMecodyn(modelname, startstate, timepars, varargin)

%
% PSPMecodyn: Computes the dynamics for a structured population model using the Escalator Boxcar Train
%
% Matlab syntax:
%
%   [curvepoints, curvedesc] = ...
%       PSPMecodyn(modelname, startstate, timepars, bifpars, parameters, options, 'clean', 'force', 'debug')
%
% Arguments:
%
%   modelname:   (string, required)
%                Basename of the file with model specification. The file
%                should have an extension '.m' or '.h'. For example, the model 'PNAS2002'
%                is specified in the file 'PNAS2002.m' or 'PNAS2002.h'
%
%   startstate:  (structure, required)
%                The initial environmental and population state from which to start the 
%                simulation of the dynamics. This structure should have the identical 
%                layout as the complete state output produced by the function PSPMequi().
%                As a minimum, the structure should contain an array 'Environment' specifying
%                the initial values of the environmental variables, and a matrix 'Pop00'
%                (assuming there is only a single population in the model), which specifies
%                on each row the number and individual state variables of a cohort of
%                while the different rows specify all the cohorts in the population.
%
%   timepars:    (row vector of length 4, required)
%                Vector of length 4 specifying the settings for the time integration:
%
%                timepars(1): Cohort cycle time, i.e. time interval between starts of new 
%                             boundary cohorts
%                timepars(2): Output time interval, i.e. time interval between data output
%                             to .out file
%                timepars(3): State output interval, i.e. time interval between complete
%                             state output to .csb file
%                timepars(4): Maximum integration time, i.e. maximum time value until which
%                             to continue the integration
%
%   bifpars:     (row vector of length 6, optional, can be the empty vector [])
%                Vector of length 6 specifying the settings for the bifurcation settings. If
%                not specified a normal time integration is carried out.
%
%                bifpars(1): Index of the bifurcation parameter
%                bifpars(2): Starting value of the bifurcation parameter
%                bifpars(3): Step size in the bifurcation parameter
%                bifpars(4): Final value of the bifurcation parameter
%                bifpars(5): Period of producing data output during each bifurcation interval
%                bifpars(6): Period of producing state output during each bifurcation interval
%
%   parameters:  (row vector, required, but can be the empty vector [])
%                Vector of a length equal to the length of the variable
%                'DefaultParameters' in case the model is specified in Matlab
%                (in a '.m' file) or equal to the constant PARAMETER_NR in case
%                the model is specified in C (in a '.h' file). The vector
%                specifies the values for the model parameters to
%                use in the computation. Vectors of other lengths, including
%                an empty vector will be ignored.
%
%   options:     (cell array, optional, can be the empty cell array {})
%                Cell array with pairs of an option name and a value (for
%                example {'info', '1'}).
%                Possible option names and their values are:
%
%                'info',   '<index>':  Level of performance information on the DOPRI5
%                                      integrator written to .err file (1, 2, 3 or 4)
%                "report", "<value>":  Interval between reporting of data output to
%                                      console ( > 0)
%
%   'clean':     (string, optional argument)
%                Remove all the result files of the model before the
%                computation
%
%   'force':     (string, optional argument)
%                Force a rebuilding of the model before the computation
%
%   'debug':     (string, optional argument)
%                Compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   curvepoints: Matrix with output for all computed points along the curve
%
%   curvedesc:   Column vector with strings, summarizing the numerical details
%                of the computed curve (i.e., initial point, parameter values,
%                numerical settings used)
%
% Copyright (C) 2017, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%

forcebuild = 0;
debugging = 0;
nVarargs = length(varargin);
myArgc = 0;
bifpars = [];
parameters = [];
options = {};

curvepoints = [];
curvedesc = [];
fname = '';

% Deduce the basename of the executable
if ((modelname(length(modelname)-1) == '.') && ((modelname(length(modelname)) == 'h') || (modelname(length(modelname)) == 'm')))
    basename = modelname(1:length(modelname)-2);
else
    basename = modelname;
end

fullfilename = mfilename('fullpath');
coredir = fileparts(fullfilename);
is_octave = exist ('OCTAVE_VERSION', 'builtin');

exebasename = [basename,'ecodyn'];
ext = mexext;
exename = [exebasename, '.', ext];

if (mislocked(exebasename))
    munlock(exebasename);
end
clear(exebasename);
clear mex;

for k = 1:nVarargs
    if ischar(varargin{k})
        if strcmp(varargin{k}, 'clean')
            resfnames = strcat(basename, '-', '*', '-', '*');
            delete(strcat(resfnames, '.bif'));
            delete(strcat(resfnames, '.err'));
            delete(strcat(resfnames, '.out'));
            delete(strcat(resfnames, '.mat'));
        elseif strcmp(varargin{k}, 'force') || strcmp(varargin{k}, 'forcebuild')
            forcebuild = 1;
        elseif strcmp(varargin{k}, 'debug')
            debugging = 1;
        else
            msg = ['Unknown function argument: ', varargin{k}];
            disp(' ');
            disp(msg);
            disp(' ');
            
        end
    else
        switch myArgc
            case 0
                bifpars = varargin{k};
            case 1
                parameters = varargin{k};
            case 2
                options = varargin{k};
        end
        myArgc = myArgc + 1;
    end
end

if ((~length(modelname)) || (~ischar(modelname)))
  disp(' ');
  disp('You have to specify a model name');
  disp(' ');
  exebasename = '';
  if is_octave
      fflush(stdout);
  end
  return;
end

if ((length(startstate) ~= 1) || ~isa(startstate,'struct'))
    error('Starting values should be a structure as produced by PSPMequi()');
end
if ((length(timepars) ~= 4) || ~isa(timepars,'double'))
    error('Time settings argument should be a vector of length 4 with double values');
end
if (length(bifpars) && ((length(bifpars) ~= 6) || (~isa(covars,'double'))))
    error('If specified bifurcation settings argument should be a vector of length 6 with double values');
end
if (length(parameters) && (~isa(parameters,'double'))) 
    error('If specified parameter values should be a vector with double values');
end
if (~isa(options,'cell'))
    error('If specified options should be a cell array');
end

mmodel = 0;
if (strcmp(modelname(length(modelname)-1:end), '.h'))
    hfile = modelname;
    mmodel = 0;
elseif (strcmp(modelname(length(modelname)-1:end), '.m'))
    hfile = modelname;
    mmodel = 1;
elseif exist([modelname '.h'], 'file')
    hfile = [modelname, '.h'];
    mmodel = 0;
elseif exist([modelname '.m'], 'file')
    hfile = [modelname, '.m'];
    mmodel = 1;
end
Mmodel = mmodel;

if ~exist(hfile, 'file')
    msg = ['No model source file named ', hfile, ' can be found'];
    disp(' ');
    disp(msg);
    disp(' ');
    exebasename = '';
    if is_octave
        fflush(stdout);
    end
    return;
end

if (mmodel == 1)
    clear global PopulationNr IStateDimension LifeHistoryStages ImpactDimension
    clear global NumericalOptions EnvironmentState DefaultParameters
    clear global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc DiscreteChangesFunc EnvEquiFunc
    run(hfile)
end

if ~exist(exename, 'file')
    build = true;
else
    d1 = dir(hfile);
    d2 = dir(exename);
    build = d1.datenum > d2.datenum;
end

if build || forcebuild
    if exist(exename, 'file')
        delete(exename);
    end

    if (mmodel == 1)
        globvars = who('global');
        global PopulationNr IStateDimension LifeHistoryStages ImpactDimension EnvironmentState;
        global NumericalOptions DefaultParameters;
        global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc EnvEquiFunc;

        modelstring = [' -DMFUNCTIONS=1 '];
        if strmatch('DiscreteChangesFunc', globvars)
            global DiscreteChangesFunc;
            modelstring = [modelstring ' -DDISCRETECHANGES=1' ];
        else
            modelstring = [modelstring ' -DDISCRETECHANGES=0' ];
        end
        if exist('NumericalOptions', 'var')
            for i = 1:length(NumericalOptions)
              modelstring = [modelstring ' -D' char(NumericalOptions(i))];
            end
        end

        EnvironmentDim = length(fields(EnvironmentState));
        ParameterNr = length(fields(DefaultParameters));
    else
        modelstring = [' -DMFUNCTIONS=0 -DPROBLEMHEADER=' hfile ];
        digits = '[1-9][0-9]*';
        fid = fopen(hfile);
        tline = fgetl(fid);
        while ischar(tline)
            expr = '\s*#define\s+POPULATION_NR\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                PopulationNr = str2num(tline((pos1 + 1):pos2));
            end
            expr = '\s*#define\s+ENVIRON_DIM\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                EnvironmentDim = str2num(tline((pos1 + 1):pos2));
            end
            expr = '\s*#define\s+I_STATE_DIM\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                IStateDimension = str2num(tline((pos1 + 1):pos2));
            end
            expr = '\s*#define\s+STAGES\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                LifeHistoryStages = str2num(tline((pos1 + 1):pos2));
            end
            expr = '\s*#define\s+PARAMETER_NR\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                ParameterNr = str2num(tline((pos1 + 1):pos2));
            end
            expr = '\s*#define\s+INTERACT_DIM\s+';
            [ok pos1] = regexp(tline, expr);
            if (ok > 0)
                [ok pos2] = regexp(tline, [expr digits]);
                ImpactDimension = str2num(tline((pos1 + 1):pos2));
            end
            tline = fgetl(fid);
        end
        fclose(fid);
    end


    if (PopulationNr < 1)
        error(['POPULATION_NR equals ' num2str(PopulationNr) ', but should be larger than 0!']);
    end
    if (EnvironmentDim < 1)
        error(['ENVIRON_DIM equals ' num2str(EnvironmentDim) ', but should be larger than 0!']);
    end
    if (IStateDimension < 1)
        error(['I_STATE_DIM equals ' num2str(IStateDimension) ', but should be larger than 0!']);
    end
    if (LifeHistoryStages < 1)
        error(['STAGES equals ' num2str(LifeHistoryStages) ', but should be larger than 0!']);
    end
    if (ParameterNr < 1)
        error(['PARAMETER_NR equals ' num2str(ParameterNr) ', but should be larger than 0!']);
    end
    if (ImpactDimension < 1)
        error(['INTERACT_DIM equals ' num2str(ImpactDimension) ', but should be larger than 0!']);
    end

    modelstring = [modelstring ' -DPOPULATION_NR=' num2str(PopulationNr) ];
    modelstring = [modelstring ' -DSTAGES=' num2str(LifeHistoryStages) ];
    modelstring = [modelstring ' -DI_STATE_DIM=' num2str(IStateDimension) ];
    modelstring = [modelstring ' -DINTERACT_DIM=' num2str(ImpactDimension) ];
    modelstring = [modelstring ' -DENVIRON_DIM=' num2str(EnvironmentDim) ];
    modelstring = [modelstring ' -DPARAMETER_NR=' num2str(ParameterNr) ];

    msg = ['Building executable ', exename, ' using sources from ', coredir, ' ...'];
    disp(' ');
    disp(msg);
    disp(' ');
    if (is_octave)
        % To build the file using Octave, only tested on Mac OS:
        if debugging
            optstring = ' -Wall -g -v';
        else
            optstring = '';
        end
        outstring   = [' -o ' exename ];
        cflagstring = ' -DOCTAVE_MEX_FILE';
        fftwlib     = ' -lfftw3';
        utlib       = '';
    else
        % To build the file using Matlab
        outstring   = [' -output ' exename ];
        if strcmp(ext, 'mexw64')
            % To build the file on Windows:
            if debugging
                optstring = ' -g -v  -largeArrayDims';
                cflagstring = ' COMPFLAGS="$COMPFLAGS -Wall"';
            else
                optstring = ' -O  -largeArrayDims';
                cflagstring = ' COMPFLAGS="$COMPFLAGS"';
            end
            fftwlib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libmwfftw3.lib') '"'];
            utlib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libut.lib') '"'];
        else
            % To build the file on UNIX:
            if debugging
                optstring = ' -g -v  -largeArrayDims';
                cflagstring = ' CFLAGS=''\$CFLAGS -Wall''';
            else
                optstring = ' -O  -largeArrayDims';
                cflagstring = ' CFLAGS=''\$CFLAGS''';
            end
            fftwlib     = ' -lmwfftw3';
            utlib       = ' -lut';
        end
    end
    
    cmd = ['mex' optstring outstring cflagstring modelstring ];
    cmd = [cmd ' -DHAS_FFTW=1'];
    cmd = [cmd ' -I. -I"' coredir '" -I"' coredir '/escbox"'];
    
    cmd = [cmd ' "' coredir '/PSPMecodyn.c"'];
    cmd = [cmd ' "' coredir '/escbox/ebtcohrt.c"'];
    cmd = [cmd ' "' coredir '/escbox/ebtdopri5.c"'];
    cmd = [cmd ' "' coredir '/escbox/ebtutils.c"'];
    cmd = [cmd utlib ];

    if debugging
        disp('');
        disp('Command line:');
        disp('');
        disp(cmd);
        disp('');
    end
    
    eval(cmd);
    if ~exist(exename, 'file')
        msg = ['Compilation of file ', exename, ' failed'];
        disp(' ');
        disp(msg);
        disp(' ');
        return;
    end
    if is_octave && ~debugging
        delete('PSPMecodyn.o');
        delete('ebtcohrt.o');
        delete('ebtdopri5.o');
        delete('ebtutils.o');
    end
else
    msg = ['Executable ', exename, ' is up-to-date'];
    disp(' ');
    disp(msg);
    disp(' ');
    feval('munlock', exebasename);
    feval('clear', exebasename);
end

drawnow;

fname = feval(exebasename, startstate, timepars, bifpars, parameters, options);

if (~isempty(fname))
    if exist(strcat(fname, '.out'), 'file')
        fid = fopen(strcat(fname, '.out'));
        line = fgetl(fid);
        headerlines = 0;
        HasData = 0;
        while ischar(line)
            if line(1) ~= '#'
                HasData = 1;
                break;
            end
            headerlines = headerlines + 1;
            curvedesc = [curvedesc '\n' line];
            line = fgetl(fid);
        end
        fclose(fid);
        curvedesc = [curvedesc '\n'];
        fprintf(curvedesc);

        if (HasData)
            curvepoints = dlmread(strcat(fname, '.out'), '', headerlines, 0);
        end
    end
end
end

