function [curvepoints, curvedesc, bifpoints, biftypes] = PSPMequi(modelname, biftype, startpoint, stepsize, parbnds, varargin)

%
% PSPMequi: Computes a bifurcation curve for a structured population model
%
% Syntax:
%
%   [curvepoints, curvedesc, bifpoints, biftypes] = ...
%       PSPMequi(modelname, biftype, startpoint, stepsize, parbnds, parameters, options,...
%                'clean', 'force', 'debug')
%
% Arguments:
%
%   modelname:   (string, required)
%                Basename of the file with model specification. The file
%                should have an extension '.m' or '.h'. For example, the model 'PNAS2002'
%                is specified in the file 'PNAS2002.m' or 'PNAS2002.h'
%
%   biftype:     (string, required)
%                Type of bifurcation to compute: BP, BPE, EQ, LP, ESS or PIP
%
%   startpoint:  (row vector, required)
%                The initial point from which to start the continuation of
%                the curve
%
%   stepsize:    (double value, required)
%                Value of the step size in the first bifurcation parameter
%
%   parbnds:     (row vector, required)
%                Vector of length 3 for EQ continuation, length 6 for BP, BPE, 
%                LP and PIP continuation and 3+4*N for ESS continuation.
%                The first triplet specifies:
%
%                parbnds(1): the index of the first bifurcation parameter
%                parbnds(2): lower threshold, below which value of the
%                            first bifurcation parameter the computation stops
%                parbnds(3): upper threshold, above which value of the
%                            first bifurcation parameter the computation stops
%
%                In case of two-parameter bifurcations, the second triplet specifies:
%
%                parbnds(4): the index of the second bifurcation parameter
%                parbnds(5): lower threshold, below which value of the
%                            second bifurcation parameter the computation stops
%                parbnds(6): upper threshold, above which value of the
%                            second bifurcation parameter the computation stops
%
%                In case of ESS continuation, consecutive sets of 4 values specify:
%
%                parbnds(4*n):   the index of population that is impacted by the
%                                parameter at its ESS value
%                parbnds(4*n+1): the index of the parameter at its ESS value
%                parbnds(4*n+2): lower threshold, below which value of this ESS
%                                parameter the computation stops
%                parbnds(4*n+3): upper threshold, above which value of this ESS
%                                parameter the computation stops
%
%   parameters:  (row vector, required, but can be the empty vector [])
%                Vector of a length equal to the length of the variable
%                'DefaultParameters' in case the model is specified in Matlab
%                (in a '.m' file) or equal to the constant PARAMETER_NR in case
%                the model is specified in C (in a '.h' file). The vector
%                specifies the values for the model parameters to
%                use in the computation. Vectors of other lengths, including
%                an empty vector will be ignored.
%
%   options:     (cell array, required, but can be the empty cell array {})
%                Cell array with pairs of an option name and a value (for
%                example {'popBP', '1'}) or single options (i.e. 'test').
%                Possible option names and their values are:
%
%                'envBP',  '<index>': Index of environment variable, of which
%                                     to continue the transcritical bifurcation
%                'popBP',  '<index>': Index of structured population, of which
%                                     to continue the transcritical bifurcation
%                'popEVO', '<index>': Index of structured population, for
%                                     which to compute the selection gradient or
%                                     perform PIP continuation
%                'parEVO', '<index>': Index of parameter, for which to compute
%                                     the selection gradient
%                'envZE',  '<index>': Index of environment variable in
%                                     trivial equilibrium (can be used
%                                     multiple times)
%                'popZE',  '<index>': Index of structured population in
%                                     trivial equilibrium (can be used
%                                     multiple times)
%                'isort',  '<index>': Index of i-state variable to use as
%                                     ruling variable for sorting the
%                                     structured populations
%                "report", "<value>": Interval between consecutive output of 
%                                     computed points to the console ( >= 1).
%                                     Minimum value of 1 implies output of 
%                                     every point
%                'noBP'             : Do not check for branching points while
%                                     computing equilibrium curves
%                'noLP'             : Do not check for limit points while
%                                     computing equilibrium curves
%                'single'           : Only compute the first point of the 
%                                     solution curve, do not continue the curve
%                'test'             : Perform only a single integration over
%                                     the life history, reporting dynamics
%                                     of survival, R0, i-state and
%                                     interaction variables
%
%   'clean':     (string. optional argument)
%                Remove all the result files of the model before the
%                computation
%
%   'force':     (string, optional argument)
%                Force a rebuilding of the model before the computation
%
%   'debug':     (string, optional argument)
%                Compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   curvepoints: Matrix with output for all computed points along the curve
%
%   curvedesc:   Column vector with strings, summarizing the numerical details
%                of the computed curve (i.e., initial point, parameter values,
%                numerical settings used)
%
%   bifpoints:   Matrix with the located bifurcation points along the curve
%
%   biftypes:    Column vector of strings, containing a description of the
%                type of bifurcation for each of the located bifurcation points
%
% Copyright (C) 2017, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%

forcebuild = 0;
debugging = 0;
nVarargs = length(varargin);
myArgc = 0;
parameters = [];
options = {};

curvepoints = [];
bifpoints = [];
biftypes = {};
curvedesc = [];
fname = '';

% Deduce the basename of the executable
if ((modelname(length(modelname)-1) == '.') && ((modelname(length(modelname)) == 'h') || (modelname(length(modelname)) == 'm')))
    basename = modelname(1:length(modelname)-2);
else
    basename = modelname;
end

% Process the variable arguments
for k = 1:nVarargs
    if ischar(varargin{k})
        if strcmp(varargin{k}, 'clean')
            resfnames = strcat(basename, '-', '*', '-', '*');
            delete(strcat(resfnames, '.bif'));
            delete(strcat(resfnames, '.err'));
            delete(strcat(resfnames, '.out'));
            delete(strcat(resfnames, '.mat'));
        elseif strcmp(varargin{k}, 'force') || strcmp(varargin{k}, 'forcebuild')
            forcebuild = 1;
        elseif strcmp(varargin{k}, 'debug')
            debugging = 1;
        else
            msg = ['Unknown function argument: ', varargin{k}];
            disp(' ');
            disp(msg);
            disp(' ');
            
        end
    else
        switch myArgc
            case 0
                parameters = varargin{k};
            case 1
                options = varargin{k};
        end
        myArgc = myArgc + 1;
    end
end

% Consistency checks of other arguments
if (~length(startpoint) || ~isa(startpoint,'double'))
    error('Starting values should be a vector with double values');
end
if ((length(stepsize) ~= 1) || ~isa(stepsize,'double'))
    error('Step size argument should be a single double value');
end
if ((~isa(parbnds,'double')) || (~((length(parbnds) == 3) || (length(parbnds) == 6) || (mod(length(parbnds)-3, 4) == 0))))
    error('Parameter bounds values should be a vector of double values of length 3, 6 or 3+4*N (in case of ESS continuation)');
end
if (length(parameters) && (~isa(parameters,'double'))) 
    error('If specified parameter values should be a vector with double values');
end
if (~isa(options,'cell'))
    error('If specified options should be a cell array');
end

% Build the executable
[Mmodel exebasename] = buildSO('PSPMequi', modelname, forcebuild, debugging);
if (~exist(exebasename, 'file'))
    msg = ['Executable ', exebasename, ' not found'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end

drawnow;

fname = feval(exebasename, biftype, startpoint, stepsize, parbnds, parameters, options);

if (Mmodel == 1)
    clear global PopulationNr IStateDimension LifeHistoryStages ImpactDimension
    clear global NumericalOptions EnvironmentState DefaultParameters
    clear global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc DiscreteChangesFunc EnvEquiFunc
end

if (~isempty(fname))
    if exist(strcat(fname, '.out'), 'file')
        fid = fopen(strcat(fname, '.out'));
        line = fgetl(fid);
        headerlines = 0;
        HasData = 0;
        while ischar(line)
            if line(1) ~= '#'
                HasData = 1;
                break;
            end
            headerlines = headerlines + 1;
            curvedesc = [curvedesc '\n' line];
            line = fgetl(fid);
        end
        fclose(fid);
        curvedesc = [curvedesc '\n'];
        fprintf(curvedesc);

        if (HasData)
            curvepoints = dlmread(strcat(fname, '.out'), '', headerlines, 0);
        end
    end
    
    if exist(strcat(fname, '.bif'), 'file')
        fid = fopen(strcat(fname, '.bif'));
        line = fgetl(fid);
        while ischar(line)
            pos = strfind(line, '****');
            if isempty(pos) 
                line = fgetl(fid); 
                continue; 
            end;
            part1 = line(1:(pos(1)-1));
            part2 = strtrim(line((pos(1)+4):(pos(2)-1)));
            row = textscan(part1, '%f');
            bifpoints = [bifpoints; transpose(cell2mat(row))];
            biftypes{end+1} = part2;
            line = fgetl(fid);
        end
        fclose(fid);
    end
end
end

