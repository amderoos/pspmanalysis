# PSPManalysis

An R/Matlab/C package for numerical analysis of physiologically structured population models

The software package PSPManalysis implements numerical procedures for the demographic, bifurcation and evolutionary analysis of physiologically structured population models (PSPMs). PSPMs represent a class of models that consistently translate continuous-time models of individual life history to the population level. The formulation of such models is discussed extensively in Metz & Diekmann (1986) and De Roos (1997) and is presented here only as far as needed for the use of the software.

The software allows for five different types of analyses of PSPMs:

* **Demographic analysis**: For linear PSPMs that do not account for density dependence or population feedback on the life history of individual organisms, the long-term population growth rate can be calculated. If the dynamics of such a linear PSPM would be simulated over time in the long run the population would grow exponentially or decline to zero with this population growth rate. The software also automatically calculates the sensitivity of this population growth rate with respect to all model parameters. Furthermore, the software calculates the stable population distribution, which characterizes the composition of the population during its exponential growth phase, and the reproductive value of the individuals in this stable population state as a function of their individual state.

* **Equilibrium analysis**: Equilibrium states can be computed for non-linear PSPMs that do account for density dependence or feedback of the population on the life history of individual organisms. These equilibrium states are computed as a function of a single model parameter, resulting in a parameterized curve of equilibrium states. Two types of special points can be detected on these equilibrium curves: limit points, also called saddle-node bifurcation points, and branching points or transcritical bifurcation points. Furthermore, the software allows for the computation of these two types of bifurcation points as a function of two model parameters. A detailed mathematical description of the methods involved in computing equilibrium curves can be found in Sanz & Getto (2016).

* **Analysis of evolutionary fixed points**: During the computation of equilibrium curves of a non-linear PSPM the software also can check whether an evolutionary singular point as defined by Adaptive Dynamics or ESS-theory (Dieckmann, 1997; Metz et al., 1996) is encountered. These singular points are subsequently classified as either a convergent stable strategy (CSS), an evolutionary branching point (EBP) or an evolutionary repellor (ERP) (Geritz et al., 1998). The software can also compute the value of a detected evolutionary singular point as a function of a second model parameter and can, starting from a detected evolutionary singular point, compute the pairwise invasibility plot (Dieckmann, 1997; Metz et al., 1996).

* **Ecological dynamics simulation**: The ecological dynamics of PSPM can be computed using the *Escalator Boxcar Train* (De Roos, 1988; De Roos et al., 1992), a numerical method especially designed for numerical integration of the partial differential equations that are the mathematical representations of PSPMs. A separate software package, [EBTtool](https://staff.fnwi.uva.nl/a.m.deroos/EBT/index.html), for computing the ecological dynamics of PSPMs has been available already for many years. The [EBTtool](https://staff.fnwi.uva.nl/a.m.deroos/EBT/index.html) consists of a graphical user interface including extensive plotting capabilities and a computational engine. A trimmed down version of this computational engine is included in the PSPManalysis package.

* **Evolutionary dynamics simulation**: The dynamics of life history trait values, which in the model occur as parameters, can be simulated over evolutionary time scales, using the canonical equation for adaptive dynamics as explained in (Dieckmann & Law, 1996). These evolutionary dynamic simulations are based on the assumption that the system approaches an ecological equilibrium in between mutation events, which change the value of the life history trait. The evolutionary rate of change is proportional to the selection gradient in the ecological equilibrium and the population birth rate.

The software package consists of a collection of routines implemented in ``C`` with front-ends that allows the software to be used from ``R``, ``Matlab`` or the Unix command-line. The implementation of the elements of the PSPM under study can be programmed in either ``R``, ``Matlab`` or ``C`` using the template files provided with the package. Implementation of the user-defined ingredients of the PSPM to be analyzed in ``R`` or ``Matlab`` is easier and to most users probably more familiar, but the user should be aware that implementing the user-defined ingredients of the PSPM in ``C`` will decrease computation times by roughly 2 orders of magnitude. Specifying the user-defined model ingredients in ``R`` or ``Matlab`` hence comes at the price of computations being excruciatingly slow. In many cases the added difficulty of using ``C`` will therefore pay off. 

All the necessary files to use the package under ``R`` are now distributed separately as a regular ``R`` package, the source of which can be found in the directory [R](R). This ``R`` package is distributed via CRAN and can be hence be installed using the ``R`` command:

```
install.packages(“PSPManalysis”)
```
The ``R`` package contains a very detailed manual, which discusses the full functionality of the package and illustrates its use with step-by-step instructions. An older (outdated) manual for using this package with the Matlab-fronted or from the Unix command-line can be found in the directory [doc](doc).

The basic methodology to numerically compute the equilibrium of a PSPM has been presented in Kirkilionis et al. (2001) and Diekmann et al. (2003), while De Roos (2008) presented the modification of the latter approach to compute the demographic characteristics of a linear PSPM. Sanz & Getto (2016) provide a mathematical description hwo to compute entire branches of equilibria of a PSPM  as a function of a model parameter.

For more information you can also check out the [presentation](https://staff.fnwi.uva.nl/a.m.deroos/downloads/PSPManalysis/AMdeRoos-PSPManalysis.mp4) I gave about this software package during an ESMTB summerschool in Palermo in 2019.

**Also refer to the [Wiki](https://bitbucket.org/amderoos/pspmanalysis/wiki/Home) document for more information!!**

----

## Acknowledgments

![ERC logo](https://staff.fnwi.uva.nl/a.m.deroos/PSPManalysis/files/logo-erc-2.png)

The development of this software has been made possible by financial support from the European Research Council under the European Union's Seventh Framework Programme (FP/2007-2013) / ERC Grant Agreement No. 322814

## References
A.M. De Roos, 1988. Numerical methods for structured population models: the Escalator boxcar train. Numer. Meth. Partial Diff. Eqs. 4(3): 173-195.

A.M. De Roos, O. Diekmann & J.A.J. Metz, 1992. Studying the dynamics of structured population models: A versatile technique and its application to Daphnia population dynamics. Amer. Natur. 139: 123-147.

A.M. De Roos, 2008. Demographic analysis of continuous-time life-history models. Ecol. Lett. 11(1): 1-15.

U. Dieckmann, 1997. Can adaptive dynamics invade? Trends in Ecology & Evolution 12 (4): 128–31.

U. Dieckmann & R. Law. 1996. The dynamical theory of coevolution: A derivation from stochastic ecological processes. Journal of Mathematical Biology 34 (5-6): 579–612.

O. Diekmann, M. Gyllenberg & J.A.J. Metz, 2003. Steady-State Analysis of Structured Population Models. Theoretical Population Biology 63 (4): 309-338.

S.A.H. Geritz, E. Kisdi, G. Meszéna, & J.A.J. Metz. 1998. Evolutionarily singular strategies and the adaptive growth and branching of the evolutionary tree. Evolutionary Ecology 12 (1): 35–57.

M.A. Kirkilionis, O. Diekmann, B. Lisser, M. Nool, A.M. De Roos & B.P. Sommeijer, 2001. Numerical continuation of equilibria of physiologically structured population models. I. Theory. Math. Models Meth. Appl. Sci. 11(6): 1101-1127.

J.A.J. Metz & O. Diekmann, 1986. The dynamics of physiologically structured populations. Vol. 68. Lecture Notes in Biomathematics. Springer-Verlag, Heidelberg.

J.A.J. Metz, S.A.H Geritz, G. Meszéna, F.J.A. Jacobs, & J.S. van Heerwaarden. 1996. Adaptive dynamics, a geometrical study of the consequences of nearly faithful reproduction. In Stochastic and Spatial Structures of Dynamical Systems, edited by S.J. van Strien & S.M. Verduyn-Lunel, 183–231. Amsterdam: KNAW Verhandelingen.

J. S. Sanz & P. Getto, 2016. Numerical Bifurcation Analysis of Physiologically Structured Populations: Consumer–Resource, Cannibalistic and Trophic Models. Bull Math Biol 78, 1546–1584.

*****


WHAT'S NEW?
-----------
                                
## Oct 21, 2020

* Version 0.3.4 is now available on CRAN

## Dec 8, 2017

* All the necessary files to use the package under ``R`` are now distributed separately as a regular ``R`` package.

## Sep 10, 2017

* The package now includes an additional program **PSPMecodyn** that allows computation of the population dynamics from an initial state containing environment variables and population states. This program uses the Escalator Boxcar Train method to carry out the numerical integration of the structured population dynamics 

* The package now allows the model to be specified in either R code or Matlab code. The appropriate layout of the model files can be gleaned from the test files Medfly.R, Medfly.m, PNAS2002.R and PNAS2002.m. However, using an R-based or Matlab-based model specification is really, really slow and hence not advisable.

## Oct 4, 2016

* The package now includes an additional program **PSPMind** that allows computation of the individual life history at a specific set of environmental conditions

* The package now uses **OpenMP** (only available on Mac OS X and Linux) to speed up computations in case individuals can be born with one of a set of states at birth

* A number of command-line options have been added to the **PSPMequi** program to allow more tailoring of the execution by the user 

## March 16, 2016

* The manual is now available in two version, one for using the package under **R**, the other for using it with **Matlab**. 

* It has been tested now on Mac OS (Macbook Pro running Yosemite) as well as Debian Linux, in all its 3 versions: using the command-line interface, the **R** interface as well as the **Matlab** interface.

* The numerical procedures have been optimized for speed and accuracy.
  
## Augustus 20, 2015

* The program package now has a complete front end  for Matlab, R (R studio) and the Mac OS Terminal 
  command-line. The manual does not document the R interface yet. Look in the file PSPManalysis.R for the
  syntax of the various commands, or look at the (many) example scripts in de 'Test' directory.
  
* The package now includes a program PSPMevodyn (can be used from Matlab, R or the Mac OS Terminal
  command-line) that simulates the evolutionary dynamics of an PSPM in an arbitrary number of parameters
  using the canonical equation as presented by Dieckmann & Law (1996, Journal of Mathematical Biology 34:
  579-612). The derivative of the parameters with respect to evolutionary time are the product of the
  population birth rate (as a measure of total population size) and the selection gradient dR0/dp
  
