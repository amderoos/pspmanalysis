/*
  Minterface.h -  Header file containing all the function that take care of interacting with Matlab, 
                  in case the life history model is specified by Matlab functions.

  Last modification: AMdR - Oct 25, 2017
*/


/*

 *==============================  Timing of PSPMequi  ================================================================================

  PSPMequi('PNAS2002.m', 'EQ', [0.000253602, 8.8569E-06, 0, 4.00802E-06, 2.33334E-06], -0.1, [1, 0, 0.0004], [], {'single'}, 'clean', 'force');
  tic; PSPMequi('PNAS2002.m', 'EQ', [0.000253602, 8.8569E-06, 0, 4.00802E-06, 2.33334E-06], -0.1, [1, 0, 0.0004], [], {}); toc

  Elapsed time is 364.867129 seconds
  Elapsed time is 347.106345 seconds.

  C-specified model:

  PSPMequi('PNAS2002', 'EQ', [0.000253602, 8.8569E-06, 0, 4.00802E-06, 2.33334E-06], -0.1, [1, 0, 0.0004], [], {'single'}, 'clean', 'force');
  tic; PSPMequi('PNAS2002', 'EQ', [0.000253602, 8.8569E-06, 0, 4.00802E-06, 2.33334E-06], -0.1, [1, 0, 0.0004], [], {}); toc

  Elapsed time is 2.578787 seconds
  Elapsed time is 2.516067 seconds

 *==============================  Timing of PSPMecodyn  ==============================================================================

 load('Checks/Initstate')

 tic; PSPMecodyn('PNAS2002.m', State_3_000000E_04, [1.0 1.0 0.0 500.0], 'force'); toc

 Elapsed time is 454.966352 seconds

 C-specified model:

 tic; PSPMecodyn('PNAS2002', State_3_000000E_04, [1.0 1.0 0.0 500.0], 'force'); toc

 Elapsed time is 2.800181 seconds.
 Elapsed time is 2.556675 seconds.
 Elapsed time is 2.519051 seconds

*/

/*
 *====================================================================================================================================
 * Static variables specifically needed when the model is specified in Matlab
 *====================================================================================================================================
 */

static int                        M_first;

static mxArray                    *M_EnvType = NULL, *M_Env = NULL;
static double                     *M_EnvPnt[ENVIRON_DIM];

static mxArray                    *M_Parms;
static double                     *M_ParmsPnt[PARAMETER_NR];

static mxArray                    *M_lifestage;
static int                        *M_lifestagePnt;
static mxArray                    *M_istate;
static double                     *M_istatePnt[POPULATION_NR][I_STATE_DIM];
static mxArray                    *M_bstate;
static double                     *M_bstatePnt[POPULATION_NR][I_STATE_DIM];
static mxArray                    *M_BirthStateNr;
static int                        *M_BirthStateNrPnt;

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
static mxArray                    *M_I;
static double                     *M_IPnt = NULL;
#endif

static mxArray                    *M_StateAtBirth;
static mxArray                    *M_LifeHistoryRates;
static mxArray                    *M_LifeStageEndings;
#if (DISCRETECHANGES == 1)
static mxArray                    *M_DiscreteChanges;
#endif
#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
static mxArray                    *M_EnvEqui;
#endif

void Minterface_Init()
{
  const char  **fnames;                                                             // pointers to field names
  int         ifield, nfields;
  mxArray     *tmp;

  M_first = 1;

  // Get (=create) a local copy of the default parameter structure from the global workspace
  M_Parms = mexGetVariable("global", "DefaultParameters");
  if (mxGetNumberOfElements(M_Parms) != 1) 
    mexErrMsgIdAndTxt("MATLAB:M_interface:DefPar", "DefaultParameters should be a single structure!");
  nfields = mxGetNumberOfFields(M_Parms);
  if (nfields != ParameterNr) mexErrMsgIdAndTxt("MATLAB:M_interface:DefParElements", "DefaultParameters should have ParameterNr fields!");
  for (ifield = 0; ifield < ParameterNr; ifield++)
    {
      parameternames[ifield] = mxGetFieldNameByNumber(M_Parms, ifield);
      tmp                    = mxGetFieldByNumber(M_Parms, 0, ifield);
      if (mxIsDouble(tmp))
        {
          M_ParmsPnt[ifield] = (double *)mxGetPr(tmp);
          parameter[ifield]  = *(M_ParmsPnt[ifield]);
        }
      else
        mexErrMsgIdAndTxt("MATLAB:M_interface:DefParElements", "DefaultParameters element %d not of type 'double'!", ifield);
    }

  // Get (=create) a local copy of the environmental state structure from the global workspace
  M_EnvType = mexGetVariable("global", "EnvironmentState");
  if (M_EnvType != NULL)
    {
      if (mxGetNumberOfElements(M_EnvType) != 1)
        mexErrMsgIdAndTxt("MATLAB:M_interface:EnvType", "EnvironmentState should be a single structure!");
      nfields = mxGetNumberOfFields(M_EnvType);
      if (nfields != EnvironDim)
        mexErrMsgIdAndTxt("MATLAB:M_interface:EnvTypeElements", "EnvironmentState should have %d (not %d) fields!", EnvironDim, nfields);
#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
        for (ifield = 0; ifield < EnvironDim; ifield++)
        {
          tmp = mxGetFieldByNumber(M_EnvType, 0, ifield);
          if (mxIsChar(tmp) && (!strcmp(mxArrayToString(tmp), "GENERALODE")))
            EnvironmentType[ifield] = GENERALODE;
          else if (mxIsChar(tmp) && (!strcmp(mxArrayToString(tmp), "PERCAPITARATE")))
            EnvironmentType[ifield] = PERCAPITARATE;
          else if (mxIsChar(tmp) && (!strcmp(mxArrayToString(tmp), "POPULATIONINTEGRAL")))
            EnvironmentType[ifield] = POPULATIONINTEGRAL;
          else
            mexErrMsgIdAndTxt("MATLAB:M_interface:EnvTypeElements",
                              "EnvironmentState field %d (%s) not defined as \"GENERALODE\", \"PERCAPITARATE\", or \"POPULATIONINTEGRAL\"!", ifield, mxArrayToString(tmp));
        }
#endif

      // allocate memory  for storing pointers and get field name pointers
      fnames = mxCalloc(nfields, sizeof(*fnames));
      for (ifield = 0; ifield < nfields; ifield++)
        fnames[ifield] = mxGetFieldNameByNumber(M_EnvType, ifield);

      // create a 1x1 struct matrix for actual values of the environment variables
      M_Env = mxCreateStructMatrix(1, 1, nfields, fnames);
      mxFree((void *)fnames);
      // Each field should be a single double value
      for (ifield = 0; ifield < nfields; ifield++)
        {
          mxSetFieldByNumber(M_Env, 0, ifield, mxCreateDoubleMatrix(1, 1, mxREAL));
          tmp = mxGetFieldByNumber(M_Env, 0, ifield);
          M_EnvPnt[ifield] = (double *)mxGetPr(tmp);
        }
    }
  else
    M_Env = mxCreateDoubleMatrix(0, 0, mxREAL);                                     // Empty matrix as placeholder
  
  // Get the handles of the user-defined functions
  M_StateAtBirth = mexGetVariable("global", "StateAtBirthFunc");
  if ((mxGetNumberOfElements(M_StateAtBirth) != 1) || (!mxIsClass(M_StateAtBirth, "function_handle")))
    mexErrMsgIdAndTxt("MATLAB:M_interface:StateAtBirth", "StateAtBirth should be a single function handle (not %s)!", mxGetClassName(M_StateAtBirth));

  M_LifeHistoryRates = mexGetVariable("global", "LifeHistoryRatesFunc");
  if ((mxGetNumberOfElements(M_LifeHistoryRates) != 1) || (!mxIsClass(M_LifeHistoryRates, "function_handle")))
    mexErrMsgIdAndTxt("MATLAB:M_interface:LifeHistoryRates", "LifeHistoryRates should be a single function handle!");

  M_LifeStageEndings = mexGetVariable("global", "LifeStageEndingsFunc");
  if ((mxGetNumberOfElements(M_LifeStageEndings) != 1) || (!mxIsClass(M_LifeStageEndings, "function_handle")))
    mexErrMsgIdAndTxt("MATLAB:M_interface:LifeStageEndings", "LifeStageEndings should be a single function handle!");

#if (DISCRETECHANGES == 1)
  M_DiscreteChanges = mexGetVariable("global", "DiscreteChangesFunc");
  if ((mxGetNumberOfElements(M_DiscreteChanges) != 1) || (!mxIsClass(M_DiscreteChanges, "function_handle")))
    mexErrMsgIdAndTxt("MATLAB:M_interface:DiscreteChanges", "DiscreteChanges should be a single function handle!");
#endif

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  M_EnvEqui = mexGetVariable("global", "EnvEquiFunc");
  if ((mxGetNumberOfElements(M_EnvEqui) != 1) || (!mxIsClass(M_EnvEqui, "function_handle")))
    mexErrMsgIdAndTxt("MATLAB:M_interface:EnvEqui", "EnvEqui should be a single function handle!");
#endif

  return;
}


void Minterface_End(void)
{
  // Destroy the default parameter structure
  mxDestroyArray(M_Parms);
  if (M_EnvType != NULL)
    {
      mxDestroyArray(M_EnvType);
    }
  mxDestroyArray(M_Env);
    
  mxDestroyArray(M_StateAtBirth);
  mxDestroyArray(M_LifeHistoryRates);
  mxDestroyArray(M_LifeStageEndings);
  
#if (DISCRETECHANGES == 1)
  mxDestroyArray(M_DiscreteChanges);
#endif

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  mxDestroyArray(M_EnvEqui);
#endif
  
  mxDestroyArray(M_istate);
  mxDestroyArray(M_bstate);
  mxDestroyArray(M_lifestage);
  mxDestroyArray(M_BirthStateNr);

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  mxDestroyArray(M_I);
#endif

return;
}


/*
 *====================================================================================================================================
 *  SECTION 2: DEFINITION OF THE INDIVIDUAL LIFE HISTORY
 *====================================================================================================================================
 */

/*
 * Specify the number of states at birth for the individuals in all structured
 * populations in the problem in the vector BirthStates[].
 */

void SetBirthStates(int BirthStates[PopulationNr], double E[])
{
  int               pp, ifield, nfields;
  const mwSize      *ndims;
  mxArray           *plhs[1], *prhs[3];
  mxArray           *tmp;
  const char        **fnames;                                                       // pointers to field names
  
  // Load the current values of environmental state variables and parameters into
  // the Matlab structures
  for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];

#if defined(ENVIRON_DIM) && (ENVIRON_DIM > 0)
  for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield])   = E[ifield];
#endif

  prhs[0] = M_StateAtBirth;
  prhs[1] = M_Env;
  prhs[2] = M_Parms;

  // Call the function
  mexCallMATLAB (1, plhs, 3, prhs, "feval");

  ndims   = mxGetDimensions(plhs[0]);
  nfields = mxGetNumberOfFields(plhs[0]);
#if (POPULATION_NR == 1)
  if ((!mxIsStruct(plhs[0])) || (ndims[0] != 1) || (nfields != IStateDim))
    mexErrMsgIdAndTxt("MATLAB:M_interface:SetBirthStatesDim", "SetBirthStates() should return a one-dimensional array of structures, each with %d fields!", IStateDim);
  BirthStates[0] = (int)ndims[1];
#else
  if ((!mxIsStruct(plhs[0])) || (nfields != IStateDim))
    mexErrMsgIdAndTxt("MATLAB:M_interface:SetBirthStatesDim", "SetBirthStates() should return a one- or two-dimensional array of structures, each with %d fields!", IStateDim);

  if (ndims[0] == 1)                                                                // Each population has a single state at birth
    {
      if (ndims[1] != PopulationNr)
        mexErrMsgIdAndTxt("MATLAB:M_interface:SetBirthStatesDim", "SetBirthStates() should return a one-dimensional array of structures of length %d, each structure with %d fields!", PopulationNr, IStateDim);
      for (pp = 0; pp < PopulationNr; pp++) BirthStates[pp] = 1;
    }
  else                                                                              // Multiple states at birth and multiple populations
    {
      if (ndims[0] != PopulationNr)
        mexErrMsgIdAndTxt("MATLAB:M_interface:SetBirthStatesDim", "SetBirthStates() should return a two-dimensional array of structures with %d rows, each structure with %d fields!", PopulationNr, IStateDim);
      for (pp = 0; pp < PopulationNr; pp++) BirthStates[pp] = ndims[1];  
    }
#endif

  // Now setup the necessary mxArray variables to serve as input to routines
  if (M_first)
    {
      // allocate memory  for storing pointers and get field name pointers
      fnames = mxCalloc(IStateDim, sizeof(*fnames));
      for (ifield = 0; ifield < nfields; ifield++)
        fnames[ifield] = mxGetFieldNameByNumber(plhs[0], ifield);

      // create a 1xPOPULATION_NR struct matrix for actual values of the i-state and b-state variables
      M_istate = mxCreateStructMatrix(1, PopulationNr, nfields, fnames);
      M_bstate = mxCreateStructMatrix(1, PopulationNr, nfields, fnames);
      mxFree((void *)fnames);

      // Each field should be a single double value
      for (pp = 0; pp < PopulationNr; pp++)
        for (ifield = 0; ifield < nfields; ifield++)
          {
            mxSetFieldByNumber(M_istate, pp, ifield, mxCreateDoubleMatrix(1, 1, mxREAL));
            mxSetFieldByNumber(M_bstate, pp, ifield, mxCreateDoubleMatrix(1, 1, mxREAL));
            tmp                     = mxGetFieldByNumber(M_istate, pp, ifield);
            M_istatePnt[pp][ifield] = (double *)mxGetPr(tmp);
            tmp                     = mxGetFieldByNumber(M_bstate, pp, ifield);
            M_bstatePnt[pp][ifield] = (double *)mxGetPr(tmp);
          }

      M_lifestage       = mxCreateNumericMatrix(1, PopulationNr, mxINT32_CLASS, mxREAL);
      M_lifestagePnt    = (int *)mxGetPr(M_lifestage);
      M_BirthStateNr    = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
      M_BirthStateNrPnt = (int *)mxGetPr(M_BirthStateNr);

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
      M_I    = mxCreateDoubleMatrix(PopulationNr, InteractDim, mxREAL);
      M_IPnt = (double *)mxGetPr(M_I);
#endif
      M_first = 0;
    }

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);
  
  return;
}


/*
 * Specify all the possible states at birth for all individuals in all
 * structured populations in the problem. BirthStateNr represents the index of
 * the state of birth to be specified. Each state at birth should be a single,
 * constant value for each i-state variable.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 */

void StateAtBirth(double *istate[PopulationNr], int BirthStateNr, double E[])
{
  int               pp, ifield, nfields;
  const mwSize      *ndims;
  mxArray           *plhs[1], *prhs[3];
  mxArray           *tmp;
  const char        **fnames;                                                       // pointers to field names

  // Load the current values of environmental state variables and parameters into
  // the Matlab structures
  for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];

#if defined(ENVIRON_DIM) && (ENVIRON_DIM > 0)
  for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield])   = E[ifield];
#endif

  prhs[0] = M_StateAtBirth;
  prhs[1] = M_Env;
  prhs[2] = M_Parms;

  // Call the function
  mexCallMATLAB(1, plhs, 3, prhs, "feval");

  ndims   = mxGetDimensions(plhs[0]);
  nfields = mxGetNumberOfFields(plhs[0]);

  for (pp = 0; pp < PopulationNr; pp++)
    for (ifield = 0; ifield < IStateDim; ifield++)
      {
        // Matlab has column-wise storage of matrix elements !!!
        tmp = mxGetFieldByNumber(plhs[0], (BirthStateNr*PopulationNr + pp), ifield);
        istate[pp][ifield] = mxGetPr(tmp)[0];
      }

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);
  
  return;
}


/*
 * Specify the threshold determining the end point of each discrete life
 * stage in individual life history as function of the i-state variables and
 * the individual's state at birth for all populations in every life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 */

void IntervalLimit(int lifestage[PopulationNr], double *istate[PopulationNr], double *bstate[PopulationNr], int BirthStateNr, double E[],
                   double limit[PopulationNr])
{
  int       pp, ifield;
  size_t    rows, cols;
  mxArray   *plhs[1], *prhs[7];
  mxArray   *maturation;

  for (pp = 0; pp < PopulationNr; pp++)
    {
      M_lifestagePnt[pp] = lifestage[pp] + 1;
      for (ifield = 0; ifield < IStateDim; ifield++)
        {
          *(M_istatePnt[pp][ifield]) = istate[pp][ifield];
          *(M_bstatePnt[pp][ifield]) = bstate[pp][ifield];
        }
      *M_BirthStateNrPnt = BirthStateNr + 1;

#if defined(ENVIRON_DIM) && (ENVIRON_DIM > 0)
      for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield]) = E[ifield];
#endif
      for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];
    }

  prhs[0] = M_LifeStageEndings;
  prhs[1] = M_lifestage;
  prhs[2] = M_istate;
  prhs[3] = M_bstate;
  prhs[4] = M_BirthStateNr;
  prhs[5] = M_Env;
  prhs[6] = M_Parms;

  // Call the function
  mexCallMATLAB (1, plhs, 7, prhs, "feval");
  maturation = plhs[0];

  rows = mxGetM(maturation);
  cols = mxGetN(maturation);
  if ((rows == PopulationNr) && (cols == 1))
    memcpy(limit, mxGetPr(maturation), PopulationNr*sizeof(double));
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:maturation", "\nMaturation not specified as a matrix with %d row(s) and a single column!\n", PopulationNr);
  
  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);

  return;
}


/*
 * Specify the development, fecundity and mortality of individuals and its impact on
 * its environment as a function of the i-state variables and the individual's state
 * at birth for all populations in every life stage.
 *
 * This routine is only used in EBT simulations
 */

#if (defined(PSPMECODYN))

void SetLifeHistoryRates(int lifestage[PopulationNr], double *istate[PopulationNr],
                         double *bstate[PopulationNr], int BirthStateNr, double E[],
                         double development[PopulationNr][IStateDim],
                         double *fecundity[PopulationNr],
                         double mortality[PopulationNr],
                         double impact[PopulationNr][InteractDim])
{
  int       ii, pp, ifield;
  size_t    rows, cols;
  mxArray   *plhs[4], *prhs[7];
  mxArray   *maturation;

  for (pp = 0; pp < PopulationNr; pp++)
    {
      M_lifestagePnt[pp] = lifestage[pp] + 1;
      for (ifield = 0; ifield < IStateDim; ifield++)
        {
          *(M_istatePnt[pp][ifield]) = istate[pp][ifield];
          *(M_bstatePnt[pp][ifield]) = bstate[pp][ifield];
        }
      *M_BirthStateNrPnt = BirthStateNr + 1;

      for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield]) = E[ifield];
      for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];
    }

  prhs[0] = M_LifeHistoryRates;
  prhs[1] = M_lifestage;
  prhs[2] = M_istate;
  prhs[3] = M_bstate;
  prhs[4] = M_BirthStateNr;
  prhs[5] = M_Env;
  prhs[6] = M_Parms;

  // Call the function
  mexCallMATLAB (4, plhs, 7, prhs, "feval");

  // Get the pointers to the output components
  if (development)
    {
      rows = mxGetM(plhs[0]);
      cols = mxGetN(plhs[0]);
      if ((rows == PopulationNr) && (cols == IStateDim))
        {
          // Matlab has column-wise storage of matrix elements !!!
          for (ii = 0; ii < IStateDim; ii++)
            for (pp = 0; pp < PopulationNr; pp++) 
              development[pp][ii] = *(mxGetPr(plhs[0]) + ii*PopulationNr + pp);
        }
      else
        mexErrMsgIdAndTxt("MATLAB:Minterface:development", "\nDevelopment not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, IStateDim);
    }
  
  if (fecundity)
    {
      rows = mxGetM(plhs[1]);
      cols = mxGetN(plhs[1]);
      if ((rows == PopulationNr) && (cols == MaxStatesAtBirth))
        {
          // Matlab has column-wise storage of matrix elements !!!
          for (ii = 0; ii < MaxStatesAtBirth; ii++)
            for (pp = 0; pp < PopulationNr; pp++) 
              fecundity[pp][ii] = *(mxGetPr(plhs[1]) + ii*PopulationNr + pp);
        }
      else
        mexErrMsgIdAndTxt("MATLAB:Minterface:fecundity", "\nFecundity not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, rows, MaxStatesAtBirth, cols);
    }

  if (mortality)
    {
      rows = mxGetM(plhs[2]);
      cols = mxGetN(plhs[2]);
      if ((rows == PopulationNr) && (cols == 1))
        memcpy(mortality, mxGetPr(plhs[2]), PopulationNr*sizeof(double));
      else
        mexErrMsgIdAndTxt("MATLAB:Minterface:mortality", "\nMortality not specified as a matrix with %d row(s) and a single column!\n", PopulationNr);
    }

  if (impact)
    {
      rows = mxGetM(plhs[3]);
      cols = mxGetN(plhs[3]);
      if ((rows == PopulationNr) && (cols == InteractDim))
        {
          // Matlab has column-wise storage of matrix elements !!!
          for (ii = 0; ii < InteractDim; ii++)
            for (pp = 0; pp < PopulationNr; pp++) 
            impact[pp][ii] = *(mxGetPr(plhs[3]) + ii*PopulationNr + pp);
        }
      else
        mexErrMsgIdAndTxt("MATLAB:Minterface:impact", "\nImpact not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, InteractDim);
    }

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);
  mxDestroyArray(plhs[1]);
  mxDestroyArray(plhs[2]);
  mxDestroyArray(plhs[3]);

  return;
}

#else

/*
 * Specify the development, fecundity and mortality of individuals and its impact on
 * its environment as a function of the i-state variables and the individual's state
 * at birth for all populations in every life stage.
 *
 * Also assign these rates to the appropriate ODEs (not used in EBT simulations)
 */

void SetDerivatives(const int totalOdeDim, const int *birthStateNr, const int BirthStateNr, int populationStart[PopulationNr],
                    int lifeStage[PopulationNr], double *bState[PopulationNr], double age, double *y, double *dyda, double *discard)
{
  int       pp, ifield, jj;
  mxArray   *plhs[4], *prhs[7];
  size_t    rows, cols;
  double    *iStatePnt[PopulationNr], *development, *fecundity, *mortality;
#if (defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  double    *impact;
#endif
  double    *derpnt;
  double    survival;
#if (defined(PSPMDEMO) && (PULSED == 0))
  double    cumrepro;
#endif

  memset(dyda, 0, totalOdeDim*sizeof(double));
  for (pp = 0; pp < PopulationNr; pp++)
    {
      M_lifestagePnt[pp] = lifeStage[pp] + 1;
      iStatePnt[pp]      = (populationStart[pp] < 0) ? bState[pp] : (y + populationStart[pp]);

      for (ifield = 0; ifield < IStateDim; ifield++)
        {
          *(M_istatePnt[pp][ifield]) = iStatePnt[pp][ifield];
          *(M_bstatePnt[pp][ifield]) = bState[pp][ifield];
        }
      *M_BirthStateNrPnt = BirthStateNr + 1;

#if defined(ENVIRON_DIM) && (ENVIRON_DIM > 0)
      for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield]) = Evar[ifield];
#endif
      for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];
    }

  prhs[0] = M_LifeHistoryRates;
  prhs[1] = M_lifestage;
  prhs[2] = M_istate;
  prhs[3] = M_bstate;
  prhs[4] = M_BirthStateNr;
  prhs[5] = M_Env;
  prhs[6] = M_Parms;

  // Call the function
#if (defined(PSPMDEMO))
  mexCallMATLAB (3, plhs, 7, prhs, "feval");
#else
  mexCallMATLAB (4, plhs, 7, prhs, "feval");
#endif
  
  // Get the pointers to the output components
  rows = mxGetM(plhs[0]);
  cols = mxGetN(plhs[0]);
  if ((rows == PopulationNr) && (cols == IStateDim)) development = mxGetPr(plhs[0]);
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:development", "\nDevelopment not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, IStateDim);

#if ((defined(PSPMDEMO) && (PULSED == 0)) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  rows = mxGetM(plhs[1]);
  cols = mxGetN(plhs[1]);
  if ((rows == PopulationNr) && (cols == MaxStatesAtBirth)) fecundity = mxGetPr(plhs[1]);
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:fecundity", "\nFecundity not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, rows, MaxStatesAtBirth, cols);
#endif

  rows = mxGetM(plhs[2]);
  cols = mxGetN(plhs[2]);
  if ((rows == PopulationNr) && (cols == 1)) mortality = mxGetPr(plhs[2]);
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:mortality", "\nMortality not specified as a matrix with %d row(s) and a single column!\n", PopulationNr);

#if (defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  rows = mxGetM(plhs[3]);
  cols = mxGetN(plhs[3]);
  if ((rows == PopulationNr) && (cols == InteractDim)) impact = mxGetPr(plhs[3]);
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:impact", "\nImpact not specified as a matrix with %d row(s) and %d column(s)!\n", PopulationNr, InteractDim);
#endif    

  // Now assign the derivatives
  for (pp = 0; pp < PopulationNr; pp++)
    {
      if (populationStart[pp] < 0) continue;

      derpnt = (dyda + populationStart[pp]);
      derpnt = (dyda + populationStart[pp]);
      
      // Matlab has column-wise storage of matrix elements !!!
      for (jj = 0; jj < IStateDim; jj++, derpnt++)
        *derpnt = development[jj*PopulationNr + pp];
#if defined(PSPMDEMO)
      *derpnt = -(mortality[pp] + PGRvar[pp]);
      derpnt++;
#else
      *derpnt = -(mortality[pp]);
      derpnt++;
#endif
      survival = exp(iStatePnt[pp][IStateDim]);
// Here we assume that the fecundity matrix returned is a rectangular matrix with equal number of
// columns (MaxStatesAtBirth) for all populations. Only the first birthStateNr[pp] entries for each
// population pp are used. Hence it is assumed that the user fills up the possibly remaining entries
// (MaxStatesAtBirth - birthStateNr[pp]) with nonsense values
#if (defined(PSPMDEMO) && (PULSED == 0))
      cumrepro = 0.0;
#endif
      for (jj = 0; jj < birthStateNr[pp]; jj++, derpnt++)
        {
          // Matlab has column-wise storage of matrix elements !!!
          *derpnt = fecundity[jj*PopulationNr + pp]*survival;
#if (defined(PSPMDEMO) && (PULSED == 0))
          cumrepro += age*(*derpnt);
#endif
        }

#if (defined(PSPMDEMO) && (PULSED == 0))
      *derpnt = cumrepro;                                                           // Generation time, last element
#else
      // Matlab has column-wise storage of matrix elements !!!
      for (jj = 0; jj < InteractDim; jj++, derpnt++)
        *derpnt = impact[jj*PopulationNr + pp]*survival;

#if (defined(PSPMEQUI) || defined(PSPMEVODYN)) && (FULLSTATEOUTPUT > 0)
      if (DoStateOutput)
        {
          for (jj = 0; jj < IStateDim; jj++, derpnt++)
            *derpnt = iStatePnt[pp][jj]*survival;                                    // Average i-state in cohort
          *derpnt   = survival;                                                     // Number of individuals in cohort is last
        }
#endif
#endif
    }

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);
  mxDestroyArray(plhs[1]);
  mxDestroyArray(plhs[2]);
#if (defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))
  mxDestroyArray(plhs[3]);
#endif

  return;
}
#endif // (defined(PSPMECODYN))


/*
 * Specify the possible discrete changes (jumps) in the individual state
 * variables when ENTERING the stage specified by 'lifestage[]'.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 */

void DiscreteChanges(int lifestage[PopulationNr], double *istate[PopulationNr], double *bstate[PopulationNr], int BirthStateNr, double E[])
{
#if (DISCRETECHANGES == 1)
  int         pp, ifield;
  mxArray     *plhs[1], *prhs[7];
  mxArray     *tmp;
  
  for (pp = 0; pp < PopulationNr; pp++)
    {
      M_lifestagePnt[pp] = lifestage[pp] + 1;
      for (ifield = 0; ifield < IStateDim; ifield++)
        {
          *(M_istatePnt[pp][ifield]) = istate[pp][ifield];
          *(M_bstatePnt[pp][ifield]) = bstate[pp][ifield];
        }
      *M_BirthStateNrPnt = BirthStateNr + 1;

#if defined(ENVIRON_DIM) && (ENVIRON_DIM > 0)
      for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield]) = E[ifield];
#endif
      for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];
    }

  prhs[0] = M_DiscreteChanges;
  prhs[1] = M_lifestage;
  prhs[2] = M_istate;
  prhs[3] = M_bstate;
  prhs[4] = M_BirthStateNr;
  prhs[5] = M_Env;
  prhs[6] = M_Parms;

  // Call the function
  mexCallMATLAB (1, plhs, 7, prhs, "feval");

  // Only copy values for those populations that really change stage 
  for (pp = 0; pp < PopulationNr; pp++) 
    if (lifestage[pp] > 0)
      {
        for (ifield = 0; ifield < IStateDim; ifield++)
          {
            tmp = mxGetFieldByNumber(plhs[0], pp, ifield);
            istate[pp][ifield] = mxGetPr(tmp)[0];
          }
      }

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);  
#endif

  return;
}

#if (defined(PSPMECODYN) || defined(PSPMEQUI) || defined(PSPMEVODYN) || defined(PSPMIND))

void EnvEqui(double E[], double I[PopulationNr][InteractDim], double condition[EnvironDim])
{
  int       ii, pp, ifield;
  size_t    rows, cols;
  mxArray   *plhs[1], *prhs[4];

  for (pp = 0; pp < PopulationNr; pp++)
    {
      // Matlab has column-wise storage of matrix elements !!!
      for (ii = 0; ii < InteractDim; ii++)
        *(M_IPnt + ii*PopulationNr + pp) = I[pp][ii];
    }
  for (ifield = 0; ifield < EnvironDim;  ifield++) *(M_EnvPnt[ifield]) = E[ifield];
  for (ifield = 0; ifield < ParameterNr; ifield++) *(M_ParmsPnt[ifield]) = parameter[ifield];

  prhs[0] = M_EnvEqui;
  prhs[1] = M_I;
  prhs[2] = M_Env;
  prhs[3] = M_Parms;
  
  // Call the function
  mexCallMATLAB (1, plhs, 4, prhs, "feval");
    
  // Get the pointers to the output components
  rows = mxGetM(plhs[0]);
  cols = mxGetN(plhs[0]);
  if ((rows == 1) && (cols == EnvironDim)) memcpy(condition, mxGetPr(plhs[0]), EnvironDim*sizeof(double));
  else
    mexErrMsgIdAndTxt("MATLAB:Minterface:equilibrium", "\nEquilibrium condition not specified as a matrix with %d row(s) and %d columns!\n", 1, EnvironDim);

  // Deallocate memory for of output
  mxDestroyArray(plhs[0]);  

  return;
}

#endif

/*==================================================================================================================================*/
