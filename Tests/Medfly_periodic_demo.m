clear
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
end

prompt = '>> [data desc] = PSPMdemo(''Medfly_periodic'', [7 11.6 0.1 11 20]);';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data desc] = PSPMdemo('Medfly_periodic', [7 11.6 0.1 11 20]);
end
format SHORTG;
disp(data);
disp(' '); disp(' ');

if (HasGUI)
    plot(data(:,1),data(:,2),'LineWidth',1,'Color',[.6 0 0])
    if exist('Checks/EcolLet_Medfly_period.mat', 'file')
        load('Checks/EcolLet_Medfly_period.mat');
        hold on;
        plot(results(1:end,1), results(1:end,2))
    end
    set(gca,'fontsize',14);
    xlabel('Forcing period', 'fontsize', 16) % x-axis label
    ylabel('Population growth rate', 'fontsize', 16) % y-axis label
end
