disp(' ');
disp('Individual life history simulation at given environmental conditions');
disp(' ');
prompt = ['>> popstate = PSPMind(''PNAS2002_5bs'', [1.30341E-05 3.84655E-05 4.00802E-06], [], {''isort'', ''1''});'];

str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    popstate = PSPMind('PNAS2002_5bs', [1.30341E-05 3.84655E-05 4.00802E-06], [], {'isort', '1'});
end
disp(' '); disp(' ');
