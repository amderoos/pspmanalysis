clear

modelname=mfilename;
modelname=strrep(modelname, 'equi_demo', '');

%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
    fntsz=12;
    fntsz2=8;
    lwaxis=1.0;
    lwmarker=0.8;
    lwcurve=1.5;
    msz=8;
    
    % Set up the figure
    hfig = figure('Color',[1.0 1.0 1.0]);
    position = get(hfig,'Position');
    outerpos = get(hfig,'OuterPosition');
    borders = outerpos - position;
    set(hfig, 'OuterPosition', [scrsz(3)/2+borders(3)/2 scrsz(2)+borders(4)/2 scrsz(3)/2.5-borders(3)-4 scrsz(4)-borders(4)-4]);
    
    left= 0.12;
    bottom1=0.69;
    bottom2=0.38;
    bottom3=0.07;
    width=0.85;
    height=0.3;
    
    h1 = axes('Position',[left bottom1 width height], 'LineWidth', lwaxis, 'XLim', [0.5E-6,4.0E-4], 'YScale', 'log', 'YLim', [5.0E-7, 5.0E-4]);
    hy1 = ylabel('Predator');
    set(gca, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hy1, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(gca, 'XTickLabel', [])
    
    h2 = axes('Position',[left bottom2 width height], 'LineWidth', lwaxis, 'XLim', [0.5E-6,4.0E-4], 'YScale', 'log', 'YLim', [5.0E-7, 1.0E-3]);
    hy2 = ylabel('Consumer');
    set(gca, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hy2, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(gca, 'XTickLabel', [])
    
    h3 = axes('Position',[left bottom3 width height], 'LineWidth', lwaxis, 'XLim', [0.5E-6,4.0E-4], 'YScale', 'log', 'YLim', [5.0E-6, 5.0E-4]);
    hx3=xlabel('Maximum resource density');
    hy3=ylabel('Resource density');
    set(gca, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hx3, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hy3, 'FontName', 'sans-serif', 'fontsize', fntsz);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Starting from the (known) trivial equilibrium with only resource');
disp(' ');
prompt = ['>> [data1, desc1, bdata1, btype1] = PSPMequi(''', modelname, ''', ''EQ'', [1.0E-06 1.0E-06], 0.5, [1 0 4E-4], [], {''popZE'', ''0'', ''envZE'', ''1'', ''envZE'', ''2''}, ''clean'', ''force'');'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data1, desc1, bdata1, btype1] = PSPMequi(modelname, 'EQ', [1.0E-06 1.0E-06], 0.5, [1 0 4E-4], [], {'popZE', '0', 'envZE', '1', 'envZE', '2'}, 'clean', 'force');
end

disp(' ');
disp('>> bdata1');
disp(' ');
disp('bdata1 = ');
disp(' ');
disp(bdata1);
disp(' ');
disp('>> btype1');
disp(' ');
disp('btype1 = ');
disp(' ');
disp(btype1);
disp(' '); disp(' ');

if (HasGUI)
    % Plot the data
    figure(hfig);
    axes(h3);
    hold on;
    plot(data1(:,1), data1(:,2),'Color',[0 .6 0], 'LineWidth', lwcurve);
    plot(bdata1(:,1), bdata1(:,2),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
    text(bdata1(:,1)+0.05E-4, bdata1(:,2)-0.15E-5,btype1, 'FontSize', fntsz2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Start from the detected branching point to compute the consumer-resource equilibrium');
disp(' ');
prompt = ['>> [data2, desc2, bdata2, btype2] = PSPMequi(''', modelname,''', ''EQ'', bdata1([1 2 5]), 0.2, [1 0 4E-4], [], {''envZE'', ''1'', ''envZE'', ''2''});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, desc2, bdata2, btype2] = PSPMequi(modelname, 'EQ', bdata1([1 2 5]), 0.2, [1 0 4E-4], [], {'envZE', '1', 'envZE', '2'});
end
disp(' ');
disp('>> bdata2');
disp(' ');
disp('bdata2 = ');
disp(' ');
disp(bdata2);
disp(' ');
disp('>> btype2');
disp(' ');
disp('btype2 = ');
disp(' ');
disp(btype2);
disp(' '); disp(' ');

if (HasGUI)
    % Plot the data
    figure(hfig);
    axes(h3);
    hold on;
    plot(data2(:,1), data2(:,2),'Color',[0 .6 0], 'LineWidth', lwcurve);
    plot(bdata2(:,1), bdata2(:,2),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
    text(bdata2(:,1), bdata2(:,2)+0.3E-5,btype2, 'FontSize', fntsz2);
    
    axes(h2);
    hold on;
    plot(data2(:,1), data2(:,7)+data2(:,8),'Color',[0 0 .6], 'LineWidth', lwcurve);
    plot(data2(:,1), data2(:,9),'Color',[.6 0 0], 'LineWidth', lwcurve);
    
    plot(bdata2(:,1), bdata2(:,9),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
    text(bdata2(:,1), bdata2(:,9)+1.2E-6,btype2, 'FontSize', fntsz2);
    
    plot(bdata2(:,1), bdata2(:,7)+bdata2(:,8),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
    text(bdata2(:,1), bdata2(:,7)+bdata2(:,8)-1.5E-4,btype2, 'FontSize', fntsz2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Start from the detected branching point of the predator to compute the predator consumer-resource equilibrium');
disp(' ');
prompt = ['>> [data3, desc3, bdata3, btype3] = PSPMequi(''', modelname, ''', ''EQ'', bdata2([1 2 3 7 5]), -0.1, [1 0 4E-4], [], {});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data3, desc3, bdata3, btype3] = PSPMequi(modelname, 'EQ', bdata2([1 2 3 7 5]), -0.1, [1 0 4E-4], [], {});
end
disp(' ');
disp('>> bdata3');
disp(' ');
disp('bdata3 = ');
disp(' ');
disp(bdata3);
disp(' ');
disp('>> btype3');
disp(' ');
disp('btype3 = ');
disp(' ');
disp(btype3);
disp(' '); disp(' ');

if (HasGUI)
    % Plot the data
    bz = size(bdata3);
    
    figure(hfig);
    axes(h3);
    hold on;
    plot(data3(:,1), data3(:,2),'Color',[0 .6 0], 'LineWidth', lwcurve);
    
    for i=1:bz(1)
        plot(bdata3(i,1), bdata3(i,2),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata3(i,1)+0.08E-4, bdata3(i,2),btype3, 'FontSize', fntsz2);
    end
    
    axes(h2);
    hold on;
    plot(data3(:,1), data3(:,7)+data3(:,8),'Color',[0 0 .6], 'LineWidth', lwcurve);
    plot(data3(:,1), data3(:,9),'Color',[.6 0 0], 'LineWidth', lwcurve);
    
    for i=1:bz(1)
        plot(bdata3(i,1), bdata3(i,9),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata3(i,1)-0.2E-4, bdata3(i,9),btype3, 'FontSize', fntsz2);
        
        plot(bdata3(i,1), bdata3(i,7)+bdata3(i,8),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata3(i,1)-0.2E-4, bdata3(i,7)+bdata3(i,8),btype3, 'FontSize', fntsz2);
    end
    
    axes(h1);
    hold on;
    plot(data3(:,1), data3(:,3),'Color','black', 'LineWidth', lwcurve);
    
    for i=1:bz(1)
        plot(bdata3(i,1), bdata3(i,3),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata3(i,1)+0.08E-4, bdata3(i,3),btype3, 'FontSize', fntsz2);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Continuation of the transcritical bifurcation curve of the structured population in 2 parameters (maximum resource density and background mortality)');
disp(' ');
prompt = ['>> [data4, desc4] = PSPMequi(''', modelname, ''', ''BP'', [bdata1([1 2]) 0.01], 0.05, [1 0 4.0E-4 11 0 0.1], [], {''envZE'', ''1'', ''envZE'', ''2'', ''popBP'', ''0''});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data4, desc4] = PSPMequi(modelname, 'BP', [bdata1([1 2]) 0.01], 0.05, [1 0 4.0E-4 11 0 0.1], [], {'envZE', '1', 'envZE', '2', 'popBP', '0'});
end
disp(' '); disp(' ');

if (HasGUI)
    % Set up the figure
    % figure(hfig);
    % hfig2 = figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)/2]);
    hfig2 = figure('Color',[1.0 1.0 1.0]);
    outerpos1 = get(hfig,'OuterPosition');
    set(hfig2, 'OuterPosition', [scrsz(1) outerpos1(2)+0.4*outerpos1(4) 0.8*outerpos1(3) 0.5*outerpos1(4)]);

    left= 0.14;
    bottom=0.12;
    width=0.83;
    height=0.85;

    h4 = axes('Position',[left bottom width height], 'LineWidth', lwaxis, 'XLim', [0.5E-6,4.0E-4], 'YLim', [0.01, 0.09]);
    hx4=xlabel('Maximum resource density');
    hy4=ylabel('Consumer mortality');
    set(gca, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hx4, 'FontName', 'sans-serif', 'fontsize', fntsz);
    set(hy4, 'FontName', 'sans-serif', 'fontsize', fntsz);
    
    % Plot the data
    axes(h4);
    hold on
    plot(data4(:,1), data4(:,6),'Color',[0 .6 0], 'LineWidth', lwcurve);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Continuation of the transcritical bifurcation curve of the predator in 2 parameters (maximum resource density and background mortality)');
disp(' ');
prompt = ['>> [data5, desc5] = PSPMequi(''', modelname, ''', ''BPE'', [bdata2([1 2 5]) 0.01], -0.1, [1 0 4.0E-4 11 0 0.1], [], {''envBP'', ''1'', ''envZE'', ''2''});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data5, desc5] = PSPMequi(modelname, 'BPE', [bdata2([1 2 5]) 0.01], -0.1, [1 0 4.0E-4 11 0 0.1], [], {'envBP', '1', 'envZE', '2'});
end
disp(' '); disp(' ');

if (HasGUI)
    % Plot the data
    figure(hfig);
    figure(hfig2);
    axes(h4);
    hold on
    plot(data5(:,1), data5(:,6),'Color',[.6 0 0], 'LineWidth', lwcurve);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Continuation of the saddle-node bifurcation in 2 parameters (maximum resource density and background mortality)');
disp(' ');
prompt = ['>> [data6, desc6] = PSPMequi(''', modelname, ''', ''LP'', [bdata3([1:5]) 0.01], 0.1, [1 0 4.0E-4 11 0 0.1], [], {});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data6, desc6] = PSPMequi(modelname, 'LP', [bdata3([1:5]) 0.01], 0.1, [1 0 4.0E-4 11 0 0.1], [], {});
end
disp(' '); disp(' ');

if (HasGUI)
    % Plot the data
    figure(hfig);
    figure(hfig2);
    axes(h4);
    hold on
    plot(data6(:,1), data6(:,6),'Color',[0 0 0], 'LineWidth', lwcurve);
end

