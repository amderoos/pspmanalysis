clear
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
end

disp(' ');
disp('Continuation of the equilibrium in the Kooijman DEB model as a function of a toxic maintenance effect (Martins et al. 2013, Am. Nat.)');
disp(' ');
prompt = '>> [data, desc, bdata, btype] = PSPMequi(''KooijmanDEB'', ''EQ'', [1.31634938 10.0 0.390274776 -6.89E-07], -0.5, [18 0.0 1.5], [], {}, ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data, desc, bdata, btype] = PSPMequi('KooijmanDEB', 'EQ', [1.31634938 10.0 0.390274776 -6.89E-07], -0.5, [18 0.0 1.5], [], {}, 'clean', 'force');
end
disp(bdata);
disp(btype);
disp(' '); disp(' ');

if (HasGUI)
    plot(data(:,1),data(:,10),'LineWidth',1,'Color',[0 0 .6])
    hold on
    plot(data(:,1),data(:,11),'LineWidth',1,'Color',[.6 0 0])
    set(gca,'fontsize',14);
    xlabel('Toxic effect size', 'fontsize', 16) % x-axis label
    ylabel('Juvenile & adult biomass', 'fontsize', 16) % y-axis label
    xlim([0 1.4]);
    ylim([0 10000]);
end
