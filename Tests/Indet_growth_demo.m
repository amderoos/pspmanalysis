clear

modelname=mfilename;
modelname=strrep(modelname, '_demo', '');

%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
    fntsz=12;
    fntsz2=8;
    lwaxis=1.0;
    lwmarker=0.8;
    lwcurve=1.5;
    msz=8;
    
    % Set up the figure
    hfig = figure('Color',[1.0 1.0 1.0]);
    set(hfig, 'OuterPosition', [scrsz(3)/2+2 scrsz(2) scrsz(3)/2-4 0.6*scrsz(4)]);
end

left= 0.13;
bottom=0.1;
width=0.75;
height=0.85;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Detection of the ESS value of the bifurcation parameter of the structured population (ingestion exponent)');
disp(' ');
prompt = ['>> [data1, desc1, bdata1, btype1] = PSPMequi(''', modelname, ''', ''EQ'', [1.0 0.22 0.0], -0.05, [6 0.5 2.0], [], {''popEVO'', ''0'', ''parEVO'', ''6'', ''popEVO'', ''0'', ''parEVO'', ''9''});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data1, desc1, bdata1, btype1] = PSPMequi(modelname, 'EQ', [1.0 0.22 0.0], -0.05, [6 0.5 2.0], [], {'popEVO', '0', 'parEVO', '6', 'popEVO', '0', 'parEVO', '9'});
    disp(bdata1);
    disp(btype1);
    disp(' '); disp(' ');
    if (HasGUI)
        plot(data1(:,1),data1(:,2),'LineWidth',lwcurve,'Color',[0 .6 0])
        hold on
        plot(bdata1(:,1), bdata1(:,2),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata1(:,1)-0.01, bdata1(:,2)-0.002,btype1, 'FontName', 'sans-serif', 'fontsize', fntsz2);
        h1left = gca; % handle to left axes
        set(h1left, 'FontName', 'sans-serif', 'fontsize', 10);
        xlabel(h1left, 'Ingestion exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel(h1left, 'Equilibrium resource density', 'FontName', 'sans-serif', 'fontsize', fntsz,'Color',[0 .6 0]);
        xlim(h1left, [0.5 1.0]);
        ylim(h1left, [0.2 0.25]);
        set(h1left, 'YTick', [ 0.2 0.21 0.22 0.23 0.24 0.25]);
        set(h1left, 'Position', [left bottom width height]);
        
        h1right = axes('Position',get(h1left,'Position'),...
            'XAxisLocation','top',...
            'YAxisLocation','right',...
            'Color','none');
        xlim(h1right, [0.5 1.0]);
        set(h1right,'xtick',[])
        set(h1right,'xticklabel',[])
        
        % Plot the data
        axes(h1right);
        hold on
%         ylim(h1right, [-1 1]);
%         set(h1right, 'fontsize', fntsz);
%         ylabel(h1right, 'dR0/dp', 'FontName', 'sans-serif', 'fontsize', fntsz,'Color','blue');
%         plot(data1(:,1),data1(:,8),'LineWidth',lwcurve,'Color','blue')
%         plot(xlim(), [0 0], 'LineStyle', '--', 'LineWidth',lwaxis,'Color','black');
%         plot(bdata1(:,1), bdata1(:,8),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
%         text(bdata1(:,1), bdata1(:,8),btype1);
        ylim(h1right, [0.78 0.8200001]);
        set(h1right, 'YTick', [0.78 0.79 0.8 0.81 0.82]);
        set(h1right, 'FontName', 'sans-serif', 'fontsize', 10);
        ylabel(h1right, 'Consumer biomass', 'FontName', 'sans-serif', 'fontsize', fntsz,'Color','blue');
        plot(data1(:,1),data1(:,5)+data1(:,6),'LineWidth',lwcurve,'Color','blue')
        plot(bdata1(:,1), bdata1(:,5)+bdata1(:,6),'*', 'Color','red', 'MarkerSize', msz, 'LineWidth', lwmarker);
        text(bdata1(:,1)-0.01, bdata1(:,5)+bdata1(:,6)+0.0015,btype1, 'FontName', 'sans-serif', 'fontsize', fntsz2);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Continuation of the ESS value of the ingestion exponentof the structured population as a function of the first bifurcation parameter (maintenance exponent)');
disp(' ');
prompt = ['>> [data2, desc2] = PSPMequi(''', modelname, ''', ''ESS'', [1.0 bdata1(1,[2 3 1])], 0.1, [9 0.5 2.0 0 6 0.5 2.0], [], {});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, desc2] = PSPMequi(modelname, 'ESS', [1.0 bdata1(1,[2 3 1])], 0.1, [9 0.5 2.0 0 6 0.5 2.0], [], {});
    disp(' '); disp(' ');

    if (HasGUI)
        if exist('hfig', 'var') figure(hfig); end;
        hfig2 = figure('Color',[1.0 1.0 1.0], 'OuterPosition',[scrsz(3)/2+2 scrsz(2)+0.4*scrsz(4) scrsz(3)/2-4 0.6*scrsz(4)]);
        h2 = axes('Position',[left bottom 0.85 0.85], 'LineWidth', lwaxis, 'XLim', [1.0 2.0], 'YLim', [1.0 2.0], 'fontsize', fntsz);
        xlabel('Maintenance exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel('Ingestion exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
        set(gca, 'FontName', 'sans-serif', 'fontsize', 10, 'YTick', [1.0 1.2 1.4 1.6 1.8 2.0]);
        
        % Plot the data
        axes(h2);
        hold on
        plot(data2(:,1),data2(:,4),'LineWidth',lwcurve,'Color',[.6 0 0])
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Construction of the PIP of the resident and mutant value of the first parameter of the structured population (ingestion exponent)');
disp(' ');
prompt = ['>> [data3, desc3] = PSPMequi(''', modelname, ''', ''PIP'', bdata1(1,[1:3 1]), 0.1, [6 0.5 2.0 6 0.5 2.0], [], {''popEVO'', ''0''});'];
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data3, desc3] = PSPMequi(modelname, 'PIP', bdata1(1,[1:3 1]),  0.1, [6 0.5 2.0 6 0.5 2.0], [], {'popEVO', '0'});
    [data4, desc4] = PSPMequi(modelname, 'PIP', bdata1(1,[1:3 1]), -0.1, [6 0.5 2.0 6 0.5 2.0], [], {'popEVO', '0'});
    disp(' '); disp(' ');

    if (HasGUI)
        if exist('hfig', 'var')  figure(hfig); end;
        if exist('hfig2', 'var') figure(hfig2); end;
        hfig3 = figure('Color',[1.0 1.0 1.0], 'OuterPosition',[scrsz(1)+2 scrsz(2)+0.4*scrsz(4) scrsz(3)/2-4 0.6*scrsz(4)]);
        h3 = axes('Position',[left bottom 0.85 0.85], 'LineWidth', lwaxis, 'XLim', [0.5 2.0], 'YLim', [0.5 2.0], 'fontsize', fntsz);
        xlabel('Resident ingestion exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel('Mutant ingestion exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
        set(gca, 'FontName', 'sans-serif', 'fontsize', 10, 'YTick', [0.5 1.0 1.5 2.0], 'XTick', [0.5 1.0 1.5 2.0]);
        
        % Plot the data
        axes(h3);
        hold on
        plot(data3(:,1), data3(:,4),'LineWidth',lwcurve,'Color',[.6 0 0])
        plot(data4(:,1), data4(:,4),'LineWidth',lwcurve,'Color',[.6 0 0])
        plot(xlim(), ylim(),'LineWidth',lwaxis,'Color','black');
    end
    
    if exist([modelname, '_resmut.h'], 'file')
        disp(' ');
        disp('Checking the constructed PIP by transcritical bifurcation continuation of an explicit resident-mutant system');
        disp(' ');
        prompt = ['>> [data5, desc5] = PSPMequi(''', modelname, '_resmut'', ''BP'', data4(end,1:4), 0.1, [6 0.5 2.0 12 0.5 2.0], [], {''popBP'' , ''1''});'];
        str = input(prompt,'s');
        if isempty(str)
            str = 'Y';
        end
        if strcmp(str, 'q') || strcmp(str, 'Q')
            error('Test script interrupted by user');
        elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
            [data5, desc5] = PSPMequi([modelname, '_resmut'], 'BP', data4(end,1:4), 0.1, [6 0.5 2.0 12 0.5 2.0], [], {'popBP', '1'});
            disp(' '); disp(' ');

            if (HasGUI)
                if exist('hfig', 'var') figure(hfig); end;
                if exist('hfig2', 'var') figure(hfig2); end;
                if exist('hfig3', 'var') figure(hfig3); end;
                axes(h3);
                hold on
                plot(data5(:,1), data5(:,5),'Color','blue')
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Simulating the evolution in the ingestion exponent over evolutionary time');
disp(' ');
prompt = ['>> [data6, desc6] = PSPMevodyn(''', modelname, ''', [0.22 0.03554 1.0], [0.05 10], [0 6 0.5 1.5], [], [], {});'];

str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data6, desc6] = PSPMevodyn(modelname, [0.22 0.03554 1.0], [0.05 10], [0 6 0.5 1.5], [], [], {});
    if (HasGUI)
        if exist('hfig', 'var')  figure(hfig); end;
        if exist('hfig2', 'var') figure(hfig2); end;
        if exist('hfig3', 'var') figure(hfig3); end;
        hfig4 = figure('Color',[1.0 1.0 1.0], 'OuterPosition',[scrsz(1)+2 scrsz(2) scrsz(3)/2-4 0.6*scrsz(4)]);
        h4 = axes('Position',[left bottom 0.85 0.85], 'LineWidth', lwaxis, 'XLim', [0 3.0], 'YLim', [0.5 1.1], 'fontsize', fntsz);
        axes(h4);
        plot(data6(:,1),data6(:,4),'LineWidth',2,'Color',[0 0 .6])
        set(gca,'fontsize',10);
        xlabel('Evolutionary time', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel('Ingestion exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' ');
disp('Simulating the simultaneous evolution in the ingestion and maintenance exponent over evolutionary time');
disp(' ');
prompt = ['>> [data7, desc7] = PSPMevodyn(''', modelname, ''', [0.22 0.03554 1.0 1.0], [0.05 100], [0 6 0.5 1.5 0 9 0.5 1.5], [], [], {});'];

str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data7, desc7] = PSPMevodyn(modelname, [0.22 0.03554 1.0 1.0], [0.05 100], [0 6 0.5 1.5 0 9 0.5 1.5], [], [], {});
    if (HasGUI)
        if exist('hfig', 'var')  figure(hfig); end;
        if exist('hfig2', 'var') figure(hfig2); end;
        if exist('hfig3', 'var') figure(hfig3); end;
        if exist('hfig4', 'var') figure(hfig4); end;
        hfig5 = figure('Color',[1.0 1.0 1.0], 'OuterPosition',[scrsz(1)+2 scrsz(2) scrsz(3)/2-4 0.6*scrsz(4)]);
        h5 = axes('Position',[left bottom 0.85 0.85], 'LineWidth', lwaxis, 'XLim', [0 50.0], 'YLim', [0.5 1.1], 'fontsize', fntsz);
        axes(h5);
        hold on;
        plot(data7(:,1),data7(:,4),'LineWidth',2,'Color',[0 0 .6])
        plot(data7(:,1),data7(:,5),'LineWidth',2,'Color',[0 .6 0])
        set(gca,'fontsize',10);
        xlabel('Evolutionary time', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel('Ingestion/Maintenance exponent', 'FontName', 'sans-serif', 'fontsize', fntsz);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

