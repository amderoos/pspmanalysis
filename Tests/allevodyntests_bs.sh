make -f ../Makefile allclean Indet_growth_5bsevodyn

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Simulating the evolution in the ingestion exponent over evolutionary time'
echo ' '
echo Excuting: ./Indet_growth_5bsevodyn -evoPars 1 0.22 0.03554 1.0 0.05 10 0 6 0.5 1.5
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growth_5bsevodyn -evoPars 1 0.22 0.03554 1.0 0.05 10 0 6 0.5 1.5

echo ' ' ; echo ' '
echo '==========================================================================================================================================================
'
echo ' '
echo 'Simulating the simultaneous evolution of the ingestion and maintenance exponent over evolutionary time'
echo ' '
echo Excuting: ./Indet_growth_5bsevodyn -evoPars 2 0.22 0.03554 1.0 1.0 0.05 10 0 6 0.5 1.5 0 9 0.5 1.5
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growth_5bsevodyn -evoPars 2 0.22 0.03554 1.0 1.0 0.05 10 0 6 0.5 1.5 0 9 0.5 1.5

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
read -p "Press [Enter] key to clean all test results, <CTRL-C> to prevent it..."
make -f ../Makefile allclean

