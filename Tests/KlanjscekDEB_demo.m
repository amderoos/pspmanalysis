clear
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
end

disp(' ');
disp('Computing the population growth rate of a structured population as a function of food density');
disp(' ');
prompt = '>> [data, desc] = PSPMdemo(''KlanjscekDEB'', [0 1.0 -0.02 0.4 1.0], ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data, desc] = PSPMdemo('KlanjscekDEB', [0 1.0 -0.02 0.4 1.0], 'clean', 'force');
end
disp(' '); disp(' ');

disp(' ');
disp('Computing the population growth rate of a structured population with 2 states at birth as a function of food density');
disp(' ');
prompt = '>> [data2, desc2] = PSPMdemo(''KlanjscekDEB2'', [0 1.0 -0.02 0.4 1.0], ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, desc2] = PSPMdemo('KlanjscekDEB2', [0 1.0 -0.02 0.4 1.0], 'clean', 'force');
end
disp(' '); disp(' ');

disp(' ');
disp('Computing the population growth rate of a structured population with pulsed reproduction as a function of food density');
disp(' ');
prompt = '>> [data3, desc3] = PSPMdemo(''KlanjscekDEBpulsed'', [0 1.0 -0.02 0.4 1.0], ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data3, desc3] = PSPMdemo('KlanjscekDEBpulsed', [0 1.0 -0.02 0.4 1.0], 'clean', 'force');
end
disp(' '); disp(' ');

if (HasGUI)
    plot(data(:,1),data(:,2),'LineWidth',1,'Color',[.6 0 0],'LineWidth',2)
    hold on
    plot(data2(:,1),data2(:,2),'LineWidth',1,'Color',[0 .6 0],'LineWidth',2,'LineStyle','--')
    if exist('Checks/EcolLet_Klancjscek2_results.mat', 'file')
        load('Checks/EcolLet_Klancjscek2_results.mat');
        plot(results(1:end,1), results(1:end,2),'Color',[1 0 0])
    end
    plot(data3(:,1),data3(:,2),'LineWidth',1,'Color',[0 0 0.6],'LineWidth',2,'LineStyle','-.')
    if exist('Checks/EcolLet_KlancjscekPulsed_results.mat', 'file')
        load('Checks/EcolLet_KlancjscekPulsed_results.mat');
        plot(results(1:end,1), results(1:end,2),'Color',[1 0 0])
    end
    set(gca,'fontsize',14);
    xlabel('Food density', 'fontsize', 16) % x-axis label
    ylabel('Population growth rate', 'fontsize', 16) % y-axis label
    xlim([0.4, 1.0]);
    ylim([0 0.7]);
end
