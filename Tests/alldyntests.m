clear
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
end

disp(' ');
prompt = '>> PSPMecodyn(''PNAS2002'', State_3_000000E_04, [1 1 10 1000], [], [], {''report'', ''50''});';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    output = PSPMequi('PNAS2002', 'EQ', [3.0E-04 1.561E-04 1.270E-04 4.008E-06 2.761E-04], 0.1, [1 0 1], [], {'single'}, 'clean', 'force');
    load('PNAS2002-EQ-0000.mat', 'State_3_000000E_04');
    data1 = PSPMecodyn('PNAS2002', State_3_000000E_04, [1 1 10 1000], [], [], {'report', '50'}, 'force');    initstate = PSPMind('PNAS2002', [1.561276e-04 1.270327e-04 4.008016e-06 0.01], [], {'isort', '1'});

    semilogy(data1(:,1),data1(:,7)+data1(:,8),'LineWidth',2,'Color',[.6 0 0])
    ylim([2.0E-6 2.0E-3])
    hold on
    plot(data1(:,1),data1(:,9),'LineWidth',2,'Color',[0 0 .6])
    set(gca,'fontsize',14);
    xlabel('Time', 'fontsize', 16) % x-axis label
    ylabel('Densities', 'fontsize', 16) % y-axis label
    hold on
end

disp(' ');
prompt = '>> initstate = PSPMind(''PNAS2002'', [1.561276e-04 1.270327e-04 4.008016e-06 0.01], [], {''isort'', ''1''});\n>> PSPMecodyn(''PNAS2002'', initstate, [1 1 10 1000], [], [], {''report'', ''50''});';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    initstate = PSPMind('PNAS2002', [1.561276e-04 1.270327e-04 4.008016e-06 0.01], [], {'isort', '1'});
    data2 = PSPMecodyn('PNAS2002', initstate, [1 1 10 1000], [], [], {'report', '50'});

    plot(data2(:,1),data2(:,7)+data2(:,8),'LineWidth',2,'Color',[.6 0 0])
    ylim([2.0E-6 2.0E-3])
    hold on
    plot(data2(:,1),data2(:,9),'LineWidth',2,'Color',[0 0 .6])
    set(gca,'fontsize',14);
    xlabel('Time', 'fontsize', 16) % x-axis label
    ylabel('Densities', 'fontsize', 16) % y-axis label
    hold on
end

disp(' ');
prompt = '>> initstate = struct(''Environment'', [1.561276e-04 1.270327e-04 4.008016e-06], ''Pop00'', [0.001 0 7.0; 1.0E-5 300 111]);\n>> PSPMecodyn(''PNAS2002'', initstate, [1 1 10 1000], [], [], {''report'', ''50''});';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    initstate = struct('Environment', [1.561276e-04 1.270327e-04 4.008016e-06], 'Pop00', [0.001 0 7.0; 1.0E-5 300 111]);
    data3 = PSPMecodyn('PNAS2002', initstate, [1 1 10 1000], [], [], {'report', '50'});

    plot(data3(:,1),data3(:,7)+data3(:,8),'LineWidth',2,'Color',[.6 0 0])
    ylim([2.0E-6 2.0E-3])
    hold on
    plot(data3(:,1),data3(:,9),'LineWidth',2,'Color',[0 0 .6])
    set(gca,'fontsize',14);
    xlabel('Time', 'fontsize', 16) % x-axis label
    ylabel('Densities', 'fontsize', 16) % y-axis label

end
