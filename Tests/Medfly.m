function [] = Medfly
%
% Medfly.m -  Matlab file specifying the elementary life-history functions of
%             the Medfly example model in:
%    
%             A.M. de Roos, 2008. Demographic analysis of continuous-time
%             life-history models. Ecol. Lett. 11(1): 1-15
%
%             The model includes a single age-structured population.
%
%   Model i-state variables:
%     1 : Age
%
%   Last modification: AMdR - Sep 05, 2017
%

global PopulationNr IStateDimension LifeHistoryStages NumericalOptions DefaultParameters;

%
% Model dimension variables: PopulationNr, IStateDimension and LifeHistoryStages
%
% These 3 variables are required, an error will occur when one of them is missing. The
% interpretation of these variables is as follows:
%
% PopulationNr:       The number of populations in the model that are structured,
%                     that is, of which the life history of individuals is explicitly
%                     modelled
% IStateDimension:    The number of individual state variables that characterise the
%                     individuals in the structured populations. This number should be
%                     the same for all structured populations in the model
% LifeHistoryStages:  The number of distinct and discrete stages in the life history
%                     of the individuals. A part of the individual life history is
%                     considered a stage, when at the boundary of this part one of the
%                     life history processes (development, fecundity, mortality or
%                     impact) changes discontinuously

PopulationNr      = 1;
IStateDimension   = 1;
LifeHistoryStages = 2;

%
% Variable name: NumericalOptions  (optional)
%
% Define a cell array called "NumericalOptions" consisting of strings to tweak the numerical
% settings that the program uses during the computations. Examples of such numerical
% settings are 'MIN_SURVIVAL=1.0E-9' and 'RHSTOL=1.0E-6', which set the survivial at which
% the individual is considered dead and the tolerance value determining when a solution has
% been found, respectively.
%
%
% Options used here:
%
%     'MIN_SURVIVAL=1.0E-9'   : Survival at which individual is considered dead
%     'MAX_AGE=100000'        : Give some absolute maximum for individual age
%     'DYTOL=1.0E-7'          : Variable tolerance
%     'RHSTOL=1.0E-8'         : Function tolerance

NumericalOptions = {         ...
    'MIN_SURVIVAL=1.0E-9',   ...
    'MAX_AGE=100000',        ...
    'DYTOL=1.0E-7',          ...
    'RHSTOL=1.0E-8'};

%
% Variable name: DefaultParameters  (required)
%
% Define a structure called "DefaultParameters" with a number of fields equal to the number of
% parameters in the model. Each field of the structure "DefaultParameters" should be given a name
% that defines the meaning of the parameter in the model and that can be used in the functions
% below that define the life history processes. The value of each field of this structure should
% be  the default for the particular parameter.

DefaultParameters = struct( ...
    'Beta0',    47.0,       ...
    'Beta1',    0.04,       ...
    'Aj',       11.0,       ...
    'Mu0',      0.00095,    ...
    'Mu1',      0.0581);

%
% Function name: StateAtBirth  (required)
%
% Specify for all structured populations in the problem all possible values of the individual state
% at birth.
%
% Function arguments:
%
%  E:     Structure with the current values of the environmental state variables.
%  Pars:  Structure with the model parameters
%
% Required return:
%
% A structure, called "Bstate", with a number of fields equal to the number of i-state variables.
% Each element should specify the numeric value of the particular i-state variable with which the
% individual is born.
% The members of the structure have to be given meaningful names, which can be used conveniently
% in the functions below that define the life history processes.
%
% If individuals can differ in their individual state at birth but the model accounts for only
% a single structured population this function should return an array of structures, each structure
% containing a number of fields equal to the number of i-state variables and each field specifying
% the value of the individual state variable of a particular state at birth.
%
% In case the model accounts for multiple, structured populations but individuals are born with a
% single state at birth this function should return an array of structures, each structure
% containing a number of fields equal to the number of i-state variables and each field specifying
% the value of the state at birth for an individual of a particular population.
%
% In case the model accounts for multiple, structured populations and individuals can differ in
% their individual state at birth this function should return a two-dimensional matrix of structures,
% with the first dimension having a length equal to the number of structured populations in the
% problem and the second dimension equal to the number of possible states at birth, with each
% structure containing a number of fields equal to the number of i-state variables.

    function [Bstate] = StateAtBirth(E, Pars)
        % We model a single structured population with a single i-state variable (age)
        Bstate.Age = 0.0;
    end

%
% Function name: LifeHistoryRates  (required)
%
% Specify for all structured populations in the problem the rates of the various life history
% processes (development, fecundity and mortality) of an individual as a function of its
% i-state variables, the individual's state at birth and the life stage that the individual
% is currently in.
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% The output consists of 3 components, named "development", "fecundity" and "mortality".
% The components should have the following structure:
%
% development:  A one-dimensional array  of length equal to the number of i-state variables.
%               Each element specifies the rate of development for the particular i-state variable.
%               In case the model accounts for multiple, structured populations this component
%               should be a two-dimensional matrix with the number of rows equal to the number of
%               structured populations in the problem and the number of columns equal to the number
%               of i-state variables.
% fecundity:    The value of the current fecundity of the individual.
%               In case the model accounts for multiple, structured populations this argument
%               is a two-dimensional matrix of fecundities with the number of rows equal to the
%               number of structured populations in the problem and a single column.
%               In case individuals can be born with different states at birth the component
%               should have a number of columns equal to the number of states at birth. Each
%               column should specify the number of offspring produced with the particular
%               state at birth.
% mortality:    A single value specifying the current mortality rate that the individual experiences.
%               In case the model accounts for multiple, structured populations this argument
%               is a two-dimensional matrix  of mortality rates with the number of rows equal to
%               the number of structured populations in the problem and a single column.

    function [development, fecundity, mortality] = LifeHistoryRates(Stage, Istate, Bstate, BStateNr, E, Pars)
        % We model a single structured population (nrow=1) with a single i-state variable (age)
        development = 1.0;
        mortality   = Pars.Mu0*exp(Pars.Mu1*Istate.Age);
        switch Stage
            case 1
                fecundity  = 0.0;
            case 2
                fecundity  = Pars.Beta0*exp(-Pars.Beta1*(Istate.Age - Pars.Aj));
        end
    end

%
% Function name: LifeStageEndings  (required)
%
% Specify for all structured populations in the problem the threshold value at which the current life
% stage of the individual ends and the individual matures to the next life history stage. The threshold
% value may depend on the current i-state variables of the individual, its state at birth and the life 
% stage that it currently is in.
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% The output consists of a single component "maturation" with the following structure:
%
% maturation:   A single value specifying when the current life stage of the individual ends and
%               the individual matures to the next life history stage. The end of the current life
%               history stage occurs when this threshold value becomes 0 and switches sign from
%               negative to  positive. For the final life stage (which never ends) return a constant
%               negative value (for example, -1)
%               In case the model accounts for multiple, structured populations this output
%               is a two-dimensional matrix with the number of rows equal to the number
%               of structured populations in the problem and a single column.

    function maturation = LifeStageEndings(Stage, Istate, Bstate, BStateNr, E, Pars)
        switch Stage
            case 1
                maturation = Istate.Age - Pars.Aj;
            case 2
                maturation = -1;
        end
    end

%
% Function name: DiscreteChanges  (optional)
%
% If discrete changes in the individual state occur at the moment individuals mature to the next
% life history stage, specify for all structured populations in the problem these discrete changes
% (jumps) in the individual state variables on ENTERING a particular life stage. The life stage that is
% entered is specified by the one-dimensional array 'Stage'.
%
% NOTE: LEAVE THIS FUNCTION UNSPECIFIED IF THERE ARE NO DISCRETE CHANGES, AS THIS SAVES COMPUTATION TIME
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array  of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% A structure 'Newstate' that has the same layout as the variable 'Istate' and contains the new
% values of the i-state variables after the stage transition.
% In case the model accounts for multiple, structured populations 'Newstate'should be an array
% of such structures, one for each population.

%     function Newstate = DiscreteChanges(Stage, Istate, Bstate, BStateNr, E, Pars)
%         Newstate = Istate;
%     end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DO NOT CHANGE THE LINES BELOW. THEY DEFINE THE NECESSARY FUNCTION HANDLES
% TO MAKE THE MODEL FUNCTIONS AVAILABLE GLOBALLY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc;

    StateAtBirthFunc     = @StateAtBirth;
    LifeHistoryRatesFunc = @LifeHistoryRates;
    LifeStageEndingsFunc = @LifeStageEndings;
    try
        DiscreteChanges();
    catch ME
        if (strcmp(ME.identifier,'MATLAB:minrhs'))
            global DiscreteChangesFunc;
            DiscreteChangesFunc = @DiscreteChanges;
        else
            clear global DiscreteChangesFunc;
        end
    end
end
