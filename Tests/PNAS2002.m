function [] = PNAS2002
%
% PNAS2002.m -  Matlab file specifying the elementary life-history functions of
%               the tri-trophic model analyzed in:
%
%               A.M. de Roos & L. Persson, 2002. Size-dependent life-history
%               traits promote catastrophic collapses of top predators.
%               Proc. Natl. Acad. Sciences 99(20): 12907-12912.
%
%               The model includes a basic resource, a size-structured consumer
%               and an unstructured predator population.
%
%   Model i-state variables:
%     1 : Age
%     2 : Length
%
%   Model environmental state variables:
%     1 : Resource
%     2 : Predators
%     3 : Vulnerable consumer biomass
%
%   Model interaction (and output) variables:
%     1 : Total resource ingestion by the consumer population
%     2 : Total biomass of small juveniles
%     3 : Total biomass of non-vulnerable juveniles
%     4 : Total adult biomass
%
%  Last modification: AMdR - Apr 18, 2018
%

global PopulationNr IStateDimension LifeHistoryStages ImpactDimension NumericalOptions EnvironmentState DefaultParameters;

%
% Model dimension variables: PopulationNr, IStateDimension, LifeHistoryStages and ImpactDimension
%
% These 4 variables are required, an error will occur when one of them is missing. The
% interpretation of these variables is as follows:
%
% PopulationNr:       The number of populations in the model that are structured,
%                     that is, of which the life history of individuals is explicitly
%                     modelled
% IStateDimension:    The number of individual state variables that characterise the
%                     individuals in the structured populations. This number should be
%                     the same for all structured populations in the model
% LifeHistoryStages:  The number of distinct and discrete stages in the life history
%                     of the individuals. A part of the individual life history is
%                     considered a stage, when at the boundary of this part one of the
%                     life history processes (development, fecundity, mortality or
%                     impact) changes discontinuously
% ImpactDimension:    The number of functions that represent the impact of an individual
%                     on its environment. At the population level these impacts
%                     affect the dynamics of the environemtal state variables and hence
%                     determine their equilibrium values. Impact functions can, however,
%                     also be used to extract certain output information on the
%                     structured populations (such as number of biomass of juveniles and
%                     adults) as all population-level values of the impact functions
%                     are reported as output.

PopulationNr      = 1;
IStateDimension   = 2;
LifeHistoryStages = 3;
ImpactDimension   = 4;

%
% Variable name: NumericalOptions  (optional)
%
% Define a cell array called "NumericalOptions" consisting of strings to tweak the numerical
% settings that the program uses during the computations. Examples of such numerical
% settings are 'MIN_SURVIVAL=1.0E-9' and 'RHSTOL=1.0E-6', which set the survivial at which
% the individual is considered dead and the tolerance value determining when a solution has
% been found, respectively.
%
%
% Options used here:
%
%     'MIN_SURVIVAL=1.0E-9'   : Survival at which individual is considered dead
%     'MAX_AGE=100000'        : Give some absolute maximum for individual age
%     'DYTOL=1.0E-7'          : Variable tolerance
%     'RHSTOL=1.0E-6'         : Function tolerance
%     'ALLOWNEGATIVE=0'       : Negative solution values allowed?
%     'COHORT_NR=100'         : Number of cohorts in population state output

NumericalOptions = {         ...
    'MIN_SURVIVAL=1.0E-9',   ...
    'MAX_AGE=100000',        ...
    'DYTOL=1.0E-7',          ...
    'RHSTOL=1.0E-6',         ...
    'ALLOWNEGATIVE=0',       ...
    'COHORT_NR=100'};
%
% Variable name: EnvironmentState  (required)
%
% Define a structure called "EnvironmentState" with a number of fields equal to the number of
% environmental state variables in the problem. Each field of the structure "EnvironmentState"
% should be given a name that defines the meaning of the environmental variable and that can be
% used in the functions below that define the life history processes.
%
% The value of each field of this structure should be one of the strings 'PERCAPITARATE',
% 'GENERALODE' or 'POPULATIONINTEGRAL', defining the nature or type of environmental state
% variable. The 3 different types are defined as follows:
%
% Set an entry to 'PERCAPITARATE' if the dynamics of E[j] follow an ODE and 0
% is a possible equilibrium state of E[j]. The ODE is then of the form
% dE[j]/dt = P(E,I)*E[j], with P(E,I) the per capita growth rate of E[j].
% Specify the equilibrium condition as condition[j] = P(E,I), do not include
% the multiplication with E[j] to allow for detecting and continuing the
% transcritical bifurcation between the trivial and non-trivial equilibrium.
%
% Set an entry to 'GENERALODE' if the dynamics of E[j] follow an ODE and 0 is
% NOT an equilibrium state of E. The ODE then has a form dE[j]/dt = G(E,I).
% Specify the equilibrium condition as condition[j] = G(E,I).
%
% Set an entry to 'POPULATIONINTEGRAL' if E[j] is a (weighted) integral of the
% population distribution, representing for example the total population
% biomass. E[j] then can be expressed as E[j] = I[p][i]. Specify the
% equilibrium condition in this case as condition[j] = I[p][i].

EnvironmentState = struct('R', 'GENERALODE', 'P', 'PERCAPITARATE', 'Bv', 'POPULATIONINTEGRAL');

%
% Variable name: DefaultParameters  (required)
%
% Define a structure called "DefaultParameters" with a number of fields equal to the number of
% parameters in the model. Each field of the structure "DefaultParameters" should be given a name
% that defines the meaning of the parameter in the model and that can be used in the functions
% below that define the life history processes. The value of each field of this structure should
% be  the default for the particular parameter.

DefaultParameters = struct(...
    'Rho',     0.1,        ...
    'Rmax',    3.0E-4,     ...
    'Lb',      7.0,        ...
    'Lv',      27.0,       ...
    'Lj',      110.0,      ...
    'Lm',      300.0,      ...
    'Omega',   9.0E-6,     ...
    'Imax',    1.0E-4,     ...
    'Rh',      1.5E-5,     ...
    'Gamma',   0.006,      ...
    'Rm',      0.003,      ...
    'Mub',     0.01,       ...
    'A',       5000.0,     ...
    'Th',      0.1,        ...
    'Epsilon', 0.5,        ...
    'Delta',   0.01);

%
% Function name: StateAtBirth  (required)
%
% Specify for all structured populations in the problem all possible values of the individual state
% at birth.
%
% Function arguments:
%
%  E:     Structure with the current values of the environmental state variables.
%  Pars:  Structure with the model parameters
%
% Required return:
%
% A structure, called "Bstate", with a number of fields equal to the number of i-state variables.
% Each element should specify the numeric value of the particular i-state variable with which the
% individual is born.
% The members of the structure have to be given meaningful names, which can be used conveniently
% in the functions below that define the life history processes.
%
% If individuals can differ in their individual state at birth but the model accounts for only
% a single structured population this function should return an array of structures, each structure
% containing a number of fields equal to the number of i-state variables and each field specifying
% the value of the individual state variable of a particular state at birth.
%
% In case the model accounts for multiple, structured populations but individuals are born with a
% single state at birth this function should return an array of structures, each structure
% containing a number of fields equal to the number of i-state variables and each field specifying
% the value of the state at birth for an individual of a particular population.
%
% In case the model accounts for multiple, structured populations and individuals can differ in
% their individual state at birth this function should return a two-dimensional matrix of structures,
% with the first dimension having a length equal to the number of structured populations in the
% problem and the second dimension equal to the number of possible states at birth, with each
% structure containing a number of fields equal to the number of i-state variables.

    function [Bstate] = StateAtBirth(E, Pars)
        % We model a single structured population with two i-state variables:
        % 1: age (initial value 0); 2: length (initial value equal to parameter Lb)
        Bstate.Age    = 0.0;
        Bstate.Length = Pars.Lb;
    end

%
% Function name: LifeHistoryRates  (required)
%
% Specify for all structured populations in the problem the rates of the various life history
% processes (development, fecundity, mortality and impact on the environment)  of an individual 
% as a function of its i-state variables, the individual's state at birth and the life 
% stage that the individual is currently in.
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% The output consists of 5 components, named "development", "fecundity", "mortality", "impact" and "maturation".
% The components should have the following structure:
%
% development:  A one-dimensional array  of length equal to the number of i-state variables.
%               Each element specifies the rate of development for the particular i-state variable.
%               In case the model accounts for multiple, structured populations this component
%               should be a two-dimensional matrix with the number of rows equal to the number of
%               structured populations in the problem and the number of columns equal to the number
%               of i-state variables.
% fecundity:    The value of the current fecundity of the individual.
%               In case the model accounts for multiple, structured populations this argument
%               is a two-dimensional matrix of fecundities with the number of rows equal to the
%               number of structured populations in the problem and a single column.
%               In case individuals can be born with different states at birth the component
%               should have a number of columns equal to the number of states at birth. Each
%               column should specify the number of offspring produced with the particular
%               state at birth.
% mortality:    A single value specifying the current mortality rate that the individual experiences.
%               In case the model accounts for multiple, structured populations this argument
%               is a two-dimensional matrix of mortality rates with the number of rows equal to
%               the number of structured populations in the problem and a single column.
% impact:       A single value or a one-dimensional array  of a length equal to the number of impact
%               functions that need to be monitored for the individual. The value (or the values of
%               the one-dimensional array ) should specify the current contribution of the individual
%               to this population-level impact.
%               In case the model accounts for multiple, structured populations this component
%               should be a two-dimensional matrix with the number of rows equal to the number of
%               structured populations in the problem and the number of columns equal to the number
%               of impact functions.

    function [development, fecundity, mortality, impact] = LifeHistoryRates(Stage, Istate, Bstate, BStateNr, E, Pars)
        % We model a single structured population (nrow=1) with two i-state variables:
        % 1: age (developmental rate 1.0); 2: Length (vonBertalanffy growth rate)
        % These rates are independent of the life stage that individuals are in
        
        funcresp    = E.R/(Pars.Rh + E.R);
        development = [1.0, Pars.Gamma*(Pars.Lm*funcresp - Istate.Length)];
        switch Stage
            case 1
                fecundity  = 0.0;
                mortality  = Pars.Mub + + Pars.A*E.P/(1+Pars.A*Pars.Th*E.Bv);
                impact     = [Pars.Imax*funcresp*Istate.Length^2, Pars.Omega*Istate.Length^3, 0, 0];
            case 2
                fecundity  = 0.0;
                mortality  = Pars.Mub;
                impact     = [Pars.Imax*funcresp*Istate.Length^2, 0, Pars.Omega*Istate.Length^3, 0];
            case 3
                fecundity  = Pars.Rm*funcresp*Istate.Length^2;
                mortality  = Pars.Mub;
                impact     = [Pars.Imax*funcresp*Istate.Length^2, 0, 0, Pars.Omega*Istate.Length^3];
        end
    end

%
% Function name: LifeStageEndings  (required)
%
% Specify for all structured populations in the problem the threshold value at which the current life
% stage of the individual ends and the individual matures to the next life history stage. The threshold
% value may depend on the current i-state variables of the individual, its state at birth and the life 
% stage that it currently is in.
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% The output consists of a single component "maturation" with the following structure:
%
% maturation:   A single value specifying when the current life stage of the individual ends and
%               the individual matures to the next life history stage. The end of the current life
%               history stage occurs when this threshold value becomes 0 and switches sign from
%               negative to  positive. For the final life stage (which never ends) return a constant
%               negative value (for example, -1)
%               In case the model accounts for multiple, structured populations this output
%               is a two-dimensional matrix with the number of rows equal to the number
%               of structured populations in the problem and a single column.

    function maturation = LifeStageEndings(Stage, Istate, Bstate, BStateNr, E, Pars)
        switch Stage
            case 1
                maturation = Istate.Length - Pars.Lv;
            case 2
                maturation = Istate.Length - Pars.Lj;
            case 3
                maturation = -1;
        end
    end

%
% Function name: DiscreteChanges  (optional)
%
% If discrete changes in the individual state occur at the moment individuals mature to the next
% life history stage, specify for all structured populations in the problem these discrete changes
% (jumps) in the individual state variables on ENTERING a particular life stage. The life stage that is
% entered is specified by the one-dimensional array 'Stage'.
%
% NOTE: LEAVE THIS FUNCTION UNSPECIFIED IF THERE ARE NO DISCRETE CHANGES, AS THIS SAVES COMPUTATION TIME
%
% Function arguments:
%
%  Stage:     Integer value specifying the life stage that the individual is currently in.
%             These stages are numbered 1 (youngest) and up.
%             In case the model accounts for multiple, structured populations this argument
%             is a one-dimensional array  of integer values.
%  Istate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the current value of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  Bstate:    A structure with a number of fields equal to the number of i-state variables.
%             Each field has the name as specified in the function StateAtBirth() above and
%             specifies the value at birth of the particular i-state variable of the individual.
%             In case the model accounts for multiple, structured populations this argument
%             is an array of such structures, one for each population.
%  BStateNr:  The integer index of the state of birth to be specified, ranging from 1 and up.
%  E:         Structure with the current values of the environmental state variables.
%  Pars:      Structure with the model parameters
%
% Required return:
%
% A structure 'Newstate' that has the same layout as the variable 'Istate' and contains the new
% values of the i-state variables after the stage transition.
% In case the model accounts for multiple, structured populations 'Newstate'should be an array
% of such structures, one for each population.

%     function Newstate = DiscreteChanges(Stage, Istate, Bstate, BStateNr, E, Pars)
%         Newstate = Istate;
%     end

% Specify for each of the environmental state variables the condition that determines its
% equilibrium value, dependent on the values of the environmental state variables themselves,
% the population impact values and the paramenters.
%
%  I:     Two-dimensional matrix of population impact values. The number of rows of this matrix
%         equals the number of structured populations in the problem, while the number of columns
%         equals the number of impact factors of an individual on its environment, as specified
%         by the output variable 'impact' of the function LifeHistoryRates(). The interpretation
%         of the latter is up to the user.
%  E:     Structure with the current values of the environmental state variables.
%  Pars:  Structure with the model parameters
%
% Required return:
%
%  A one-dimensional array  with a length equal to the number of environmental state variables
%  in the problem. Each entry i of this one-dimensional array  should specify the equilibrium
%  condition for the particular environmental state variable (i).

    function EquiCondition = EnvEqui(I, E, Pars)
        EquiCondition = [Pars.Rho*(Pars.Rmax - E.R) - I(1,1), Pars.Epsilon*Pars.A*I(1,2)/(1+Pars.A*Pars.Th*I(1,2)) - Pars.Delta, I(1,2)];
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DO NOT CHANGE THE LINES BELOW. THEY DEFINE THE NECESSARY FUNCTION HANDLES
% TO MAKE THE MODEL FUNCTIONS AVAILABLE GLOBALLY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc EnvEquiFunc;

    StateAtBirthFunc     = @StateAtBirth;
    LifeHistoryRatesFunc = @LifeHistoryRates;
    LifeStageEndingsFunc = @LifeStageEndings;
    EnvEquiFunc          = @EnvEqui;
    try
        DiscreteChanges();
    catch ME
        if (strcmp(ME.identifier,'MATLAB:minrhs'))
            global DiscreteChangesFunc;
            DiscreteChangesFunc = @DiscreteChanges;
        else
            clear global DiscreteChangesFunc;
        end
    end
end
