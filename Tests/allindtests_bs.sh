make -f ../Makefile allclean PNAS2002_5bsind

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Computing the individual life history in a given environment'
echo ' '
echo Excuting: ./PNAS2002_5bsind -isort 1 1.30341E-05 3.84655E-05 4.00802E-06
echo ' '
read -p "Press [Enter] key to start test..."

./PNAS2002_5bsind -isort 1 1.30341E-05 3.84655E-05 4.00802E-06; ../csb2txt PNAS2002_5bs-IND-0000.csb

echo ' '

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Computing the individual life history in a given environment'
echo ' '
echo Excuting: ./PNAS2002_5bsind -isort 1 1.30341E-05 3.84655E-05 4.00802E-06 5.0
echo ' '
read -p "Press [Enter] key to start test..."

./PNAS2002_5bsind -isort 1 1.30341E-05 3.84655E-05 4.00802E-06 5.0; ../csb2txt PNAS2002_5bs-IND-0001.csb

echo ' '
read -p "Press [Enter] key to clean all test results, <CTRL-C> to prevent it..."
make -f ../Makefile allclean


