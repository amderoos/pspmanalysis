clear
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
    fntsz=12;
    fntsz2=8;
    lwaxis=1.0;
    lwmarker=0.8;
    lwcurve=1.5;
    msz=8;
    
    % Set up the figure
    hfig = figure('Color',[1.0 1.0 1.0]);
end

disp(' ');
disp('Computing the population growth rate of a structured population as a function of food density');
disp(' ');
prompt = '>> [data, desc] = PSPMdemo(''PNAS2002'', [11 0.01 0.001 0.01 0.1], ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data, desc] = PSPMdemo('PNAS2002', [11 0.01 0.001 0.01 0.1], 'clean', 'force');
end
disp(' '); disp(' ');

if (HasGUI)
    figure(hfig);
    set(gca, 'FontName', 'sans-serif', 'fontsize', 10);
    xlabel('Consumer mortality rate', 'FontName', 'sans-serif', 'fontsize', fntsz) % x-axis label
    ylabel('Population growth rate', 'FontName', 'sans-serif', 'fontsize', fntsz) % y-axis label
    xlim([0.01, 0.1]);
    ylim([-0.02 0.08]);
    set(gca, 'YTick', [-0.02 0 0.02 0.04 0.06 0.08]);
    hold on
    plot([-0.02 0.1], [0.0 0.0], 'LineStyle', '--', 'LineWidth', 1, 'Color', 'black');
    
    hold on;
    plot(data(:,1),data(:,2),'LineWidth',lwcurve,'Color',[.6 0 0])

    left= 0.12;
    bottom=0.1;
    width=0.75;
    height=0.85;
   
    load('PNAS2002-PGR-0000.mat', 'State_1_000000E_02')
    load('PNAS2002-PGR-0000.mat', 'State_5_000000E_02')
        if exist('hfig', 'var') figure(hfig); end;
        hfig2 = figure('Color',[1.0 1.0 1.0], 'OuterPosition',[scrsz(3)/2+2 scrsz(2)+0.4*scrsz(4) scrsz(3)/2-4 0.6*scrsz(4)]);
        
        h2 = axes('Position',[left bottom width height], 'LineWidth', lwaxis, 'XLim', [7 110.0], 'YLim', [0.0 1.1], 'fontsize', fntsz);
        xlabel('Body size (mm)', 'FontName', 'sans-serif', 'fontsize', fntsz);
        ylabel('Stable size distribution', 'FontName', 'sans-serif', 'fontsize', fntsz,'Color',[.6 0 0]);
        set(gca, 'FontName', 'sans-serif', 'fontsize', 10, 'YTick', [0.0 0.2 0.4 0.6 0.8 1.0]);
 
        axes(h2);
        hold on
        plot(State_1_000000E_02.Pop00_Istate01, State_1_000000E_02.Pop00_StableDist, 'LineWidth',lwcurve,'Color',[.6 0 0])
%        plot(State_5_000000E_02.Pop00_Istate01, State_5_000000E_02.Pop00_StableDist, 'LineStyle', '--', 'LineWidth',lwcurve,'Color',[0 .6 0])

        h1right = axes('Position',get(h2,'Position'),...
            'XAxisLocation','top',...
            'YAxisLocation','right',...
            'Color','none');
        xlim(h1right, [0.0 110.0]);
        set(h1right,'xtick',[])
        set(h1right,'xticklabel',[])
        
        % Plot the data
        axes(h1right);
        hold on
%        ylim(h1right, [0.78 0.8200001]);
%        set(h1right, 'YTick', [0.78 0.79 0.8 0.81 0.82]);
        set(h1right, 'FontName', 'sans-serif', 'fontsize', 10);
        ylabel(h1right, 'Reproductive value', 'FontName', 'sans-serif', 'fontsize', fntsz,'Color',[0 0 0.6]);
        plot(State_1_000000E_02.Pop00_Istate01, State_1_000000E_02.Pop00_ReproVal, 'LineWidth',lwcurve,'Color',[0 0 0.6]);
%        plot(State_5_000000E_02.Pop00_Istate01, State_5_000000E_02.Pop00_ReproVal, 'LineStyle', '--', 'LineWidth',lwcurve,'Color','blue')
end
