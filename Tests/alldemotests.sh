make -f ../Makefile allclean Medflydemo KlanjscekDEBdemo KlanjscekDEB2demo KlanjscekDEBpulseddemo Medfly_periodicdemo

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing computation of population growth rate and sensitivities'
echo ' '
echo Excuting: ./Medflydemo
echo ' '
read -p "Press [Enter] key to start test..."

time ./Medflydemo

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing computation of population growth rate and sensitivities'
echo ' '
echo Excuting: ./Medflydemo -par1 2 11 0.1 11 20
echo ' '
read -p "Press [Enter] key to start test..."

time ./Medflydemo -par1 2 11 0.1 11 20

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing continuation of population growth rate as a function of a parameter'
echo ' '
echo Excuting: ./KlanjscekDEBdemo -par1 0 1.0 -0.02 0.405695 1.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./KlanjscekDEBdemo -par1 0 1.0 -0.02 0.405695 1.0

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing continuation of population growth rate as a function of a parameter'
echo ' '
echo Excuting: ./KlanjscekDEB2demo -par1 0 1.0 -0.02 0.405695 1.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./KlanjscekDEB2demo -par1 0 1.0 -0.02 0.405695 1.0

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing continuation of population growth rate as a function of a parameter'
echo ' '
echo Excuting: ./KlanjscekDEBpulseddemo -par1 0 1.0 -0.02 0.4 1.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./KlanjscekDEBpulseddemo -par1 0 1.0 -0.02 0.405695 1.0

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing computation of population growth rate and sensitivities'
echo ' '
echo Excuting: ./Medfly_periodicdemo -par1 7 11.6 0.1 11 20
echo ' '
read -p "Press [Enter] key to start test..."

time ./Medfly_periodicdemo -par1 7 11.6 0.1 11 20

echo ' '
echo ' '
read -p "Press [Enter] key to clean all test results, <CTRL-C> to prevent it..."
make -f ../Makefile allclean

