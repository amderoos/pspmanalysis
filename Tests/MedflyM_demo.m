clear 
%# use text-based alternative (input)
HasGUI = exist ('OCTAVE_VERSION', 'builtin') && (length(loaded_graphics_toolkits ()) > 0);
HasGUI = HasGUI || (usejava('jvm') && usejava('desktop'));

if (HasGUI)
    %# use GUI dialogs (questdlg)
    close all
    set(0,'Units', 'pixels')
    scrsz = get(0,'ScreenSize');
    HasGUI = true;
end

disp(' ');
prompt = '>> PSPMdemo(''Medfly.m'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    PSPMdemo('Medfly.m');
end

prompt = '>> PSPMdemo(''Medfly.m'', ''clean'', ''force'');';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    PSPMdemo('Medfly.m', 'clean', 'force');
end

prompt = '>> [data desc] = PSPMdemo(''Medfly.m'', [2 11 0.1 11 20]);';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data desc] = PSPMdemo('Medfly.m', [2 11 0.1 11 20]);
end
format short g;
disp(data);
disp(' '); disp(' ');

if (HasGUI)
    plot(data(:,1),data(:,2),'LineWidth',1,'Color',[.6 0 0])
    set(gca,'fontsize',14);
    xlabel('Juvenile period', 'fontsize', 16) % x-axis label
    ylabel('Population growth rate', 'fontsize', 16) % y-axis label
end
