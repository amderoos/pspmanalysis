make -f ../Makefile allclean PNAS2002equi Indet_growthequi Indet_growth_resmutequi KooijmanDEBequi

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing transcritical bifurcation detection of the structured population from the trivial equilibrium'
echo ' '
echo Excuting: ./PNAS2002equi -popZE 0 -envZE 1 -envZE 2 EQ 8.5E-06 8.5E-06 0.05 1 0 9E-6
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -popZE 0 -envZE 1 -envZE 2 EQ 8.5E-06 8.5E-06 0.05 1 0 9E-6

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing continuation of non-trivial equilibrium of the structured population from its transcritical bifurcation'
echo ' '
echo Excuting: ./PNAS2002equi -envZE 1 -envZE 2 EQ 8.85690E-06 8.85690E-06 0.0 0.05 1 0 9.0E-6
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -envZE 1 -envZE 2 EQ 8.85690E-06 8.85690E-06 0.0 0.05 1 0 9.0E-6

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing transcritical bifurcation detection of the structured population from the non-trivial equilibrium'
echo ' '
echo Excuting: ./PNAS2002equi -envZE 1 -envZE 2 EQ 1.51883E-05 8.85690E-06 6.03621E-08 -0.05 1 0 1
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -envZE 1 -envZE 2 EQ 1.51883E-05 8.85690E-06 6.03621E-08 -0.05 1 0 1

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the transcritical bifurcation curve of the structured population in 2 parameters (maximum resource density and background mortality)'
echo ' '
echo Excuting: ./PNAS2002equi -popBP 0 -envZE 1 -envZE 2 BP 8.856903E-06 8.856903E-06 1.00000E-02  0.05 1 0 9.0E-6 11 0 0.05
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -popBP 0 -envZE 1 -envZE 2 BP 8.856903E-06 8.856903E-06 1.00000E-02  0.05 1 0 9.0E-6 11 0 0.05

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo Testing saddle-node bifurcation detection in the full system
echo ' '
echo Executing: ./PNAS2002equi EQ 8.88128E-05 1.37697E-05 4.05820E-05 4.00802E-06 5.71176E-05 -0.05 1 0 9E-5
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi EQ 8.88128E-05 1.37697E-05 4.05820E-05 4.00802E-06 5.71176E-05 -0.05 1 0 9E-5

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the saddle-node bifurcation curve in 2 parameters (maximum resource density and background mortality)'
echo ' '
echo Excuting: ./PNAS2002equi LP 8.84757E-05  1.30562E-05  3.77245E-05  4.00802E-06  5.10254E-05  1.00000E-02  0.05 1 0 0.95E-4 11 0 0.05
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi LP 8.84757E-05  1.30562E-05  3.77245E-05  4.00802E-06  5.10254E-05  1.00000E-02  0.05 1 0 0.95E-4 11 0 0.05

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing transcritical bifurcation detection of the predator (environment variable 1) from the non-trivial equilibrium'
echo ' '
echo Excuting: ./PNAS2002equi EQ 2.46529E-04 8.86957E-06 6.62276E-07 4.00802E-06 2.53694E-06 0.05 1 0 1
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi EQ 2.46529E-04 8.86957E-06 6.62276E-07 4.00802E-06 2.53694E-06 0.05 1 0 1

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Testing transcritical bifurcation detection of the predator (environment variable 1) from the trivial equilibrium'
echo ' '
echo Excuting: ./PNAS2002equi -envZE 1 -envZE 2 EQ 2.46529E-04 8.85690E-06 2.26590E-06 0.05 1 0 2.6E-4
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -envZE 1 -envZE 2 EQ 2.46529E-04 8.85690E-06 2.26590E-06 0.05 1 0 2.6E-4

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the transcritical bifurcation curve (extinction boundary) of the predator in 2 parameters (maximum resource density and predator mortality)'
echo ' '
echo Excuting: ./PNAS2002equi -envBP 1 -envZE 2 BPE 2.53602E-04  8.85690E-06  2.33334E-06  1.00000E-02 0.05 1 0 2.6E-4 11 0.0 0.05
echo ' '
read -p "Press [Enter] key to start test..."

time ./PNAS2002equi -envBP 1 -envZE 2 BPE 2.53602E-04  8.85690E-06  2.33334E-06  1.00000E-02 0.05 1 0 2.6E-4 11 0.0 0.05

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Detection of the ESS value of the bifurcation parameter of the structured population (ingestion exponent)'
echo ' '
echo Excuting: ./Indet_growthequi -popEVO 0 -parEVO 6 -popEVO 0 -parEVO 9 EQ 1.02273E+00 2.25643E-01 5.02982E-02 -0.1 6 0.5 2.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growthequi -popEVO 0 -parEVO 6 -popEVO 0 -parEVO 9 EQ 1.02273E+00 2.25643E-01 5.02982E-02 -0.1 6 0.5 2.0

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the ESS value of first parameter of the structured population (ingestion exponent) as a function of the second bifurcation parameter (maintenance exponent)'
echo ' '
echo Excuting: ./Indet_growthequi -essPars 1 ESS 1.0 2.15593E-01 1.75890E-02 9.38266E-01 0.1 9 0.5 2.0 0 6 0.5 3.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growthequi -essPars 1 ESS 1.0 2.15593E-01 1.75890E-02 9.38266E-01 0.1 9 0.5 2.0 0 6 0.5 3.0

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the PIP of first parameter of the structured population (ingestion exponent)'
echo ' '
echo Excuting: ./Indet_growthequi -popEVO 0 PIP 0.938266 0.215593 0.017589 0.938266 -0.1 6 0.5 2.0 6 0.5 2.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growthequi -popEVO 0 PIP 0.938266 0.215593 0.017589 0.938266 -0.1 6 0.5 2.0 6 0.5 2.0

echo ' ' ; echo ' '
echo '==========================================================================================================================================================
'

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Checking the constructed PIP by transcritical bifurcation continuation of an explicit resident-mutant system'
echo ' '
echo Excuting: ./Indet_growth_resmutequi -popBP 1 BP 0.497016 0.242096 0.006969 1.05912 0.1 6 0.5 2.0 12 0.5 2.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./Indet_growth_resmutequi -popBP 1 BP 0.497016 0.242096 0.006969 1.05912 0.1 6 0.5 2.0 12 0.5 2.0

echo ' ' ; echo ' '
echo '==========================================================================================================================================================
'

echo ' '
echo 'Continuation of the equilibrium in Kooijmans DEB model as a function of the toxic effect on maintenance'
echo ' '
echo Excuting: ./KooijmanDEBequi EQ 1.31634938 10.0  0.390274776 -6.89E-07 -0.5 18 0.0 1.5
echo ' '
read -p "Press [Enter] key to start test..."

time ./KooijmanDEBequi EQ 1.31634938 10.0  0.390274776 -6.89E-07 -0.5 18 0.0 1.5

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
read -p "Press [Enter] key to clean all test results, <CTRL-C> to prevent it..."
make -f ../Makefile allclean

