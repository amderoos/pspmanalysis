Using the mex compiler under Matlab
===================================

When using **Matlab** the package needs the `mex` compiler to compile C files into a shared library that can be loaded for computations. The `mex` compiler requires a stand-alone C compiler to function properly. Newer versions of Mac OS X do not automatically include a compiler with the basic distribution and hence require installation of the command-line tools of **XCode**, the development environment on Mac OS X. For freely available and supported compilers on Windows systems check the [Matlab website](http://www.mathworks.com). In **Matlab** run the command **mex -setup** to select the C-compiler to use. Make sure that your setup of `mex` is working properly, consult the **Matlab** documentation for more information if necessary.

On Mac OS and Linux systems the `mex` compiler uses a shell script **mexopts.sh** to tailor the compilation settings to your system. The **Matlab** distribution includes a standard version of this shell script, but this has not worked very well for me. I therefore include here an adapted version of this shell script that works for me on Mac OS and Linux using Matlab 2012b. To use this version of the shell script it has to be placed in the directory where your **Matlab** version is looking for its configuration files. You can find out the name of this directory by issuing the command `prefdir` on the **Matlab** command line, as shown below:

    >> prefdir
    
    ans =

    /Users/andre/.matlab/R2012b

If you put the **mexopts.sh** supplied here in that particular directory it will be used instead of the one supplied by default. The variables in the **mexopts.sh** that might have to be tuned for your system are the variables `CC`, `CFLAGS`, `LDFLAGS` and on Mac OS X possibly `CLIBS`. 

The section in the **mexopts.sh** file supplied here that I have changed to get the package working properly on my Mac OS X (Yosemite) system is:

            # 
            # CLANG settings: Include -fopenmp as argument for threading support
            # 
            CC='/opt/local/bin/clang'
            CFLAGS="-fopenmp -fno-common -arch $ARCHS -isysroot $MW_SDKROOT -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            LDFLAGS="-fopenmp -arch $ARCHS -Wl,-syslibroot,$MW_SDKROOT -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            # 
            # GCC settings
            # 
            # CC='/opt/local/bin/gcc'
            # CFLAGS="-fopenmp -fno-common -arch $ARCHS -isysroot $MW_SDKROOT -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            # LDFLAGS="-fopenmp -arch $ARCHS -Wl,-syslibroot,$MW_SDKROOT -mmacosx-version-min=$MACOSX_DEPLOYMENT_TARGET"
            # 
            # Link with Matlab's libiomp5.dylib to prevent incompatibilities
            #
            CLIBS="$MLIBS -L$TMW_ROOT/sys/os/$Arch -liomp5"
            # 

In the above code, the variable `CC` is set to the compiler that I use on my Mac (I checked the package both with the clang and gcc compiler). I do not use the clang compiler that is standard supplied with XCode 7.2 on Mac OS X Yosemite, as this version of clang is old and does not support parallelisation of the program via **OpenMP**. The clang and gcc compiler I use I have installed from **macports**. Parallelisation speeds up the computations significantly in case individuals of a particular population can be born with different states at birth. Furthermore, to make use of the automatic parallelisation offered by **OpenMP** the command-line argument `-fopenmp` is added to both the `CFLAGS` and the `LDFLAGS` variable.

To get the package working properly on a Debian Linux system I have changed the variables `CC`, `CFLAGS` and `LDFLAGS` in the section for Linux systems to specify the compiler to use (I checked the package both with Intel's **icc** compiler and the `gcc` compiler) and have added the command-line argument for automatic parallelisation using **OpenMP** to the `CFLAGS` and `LDFLAGS` variables (options `-openmp` and `-fopenmp` for Intel's **icc** and the `gcc` compiler, respectively), as shown below:

            # 
            # ICC settings: Include -openmp as argument for threading support
            # 
            CC='icc'
            CFLAGS="-std=gnu99 -O3 -ipo -axAVX -msse3 -openmp -D_GNU_SOURCE"
            LDFLAGS="-openmp -pthread -shared -Wl,--version-script,$TMW_ROOT/extern/lib/$Arch/$MAPFILE -Wl,--no-undefined"
            # 
            # GCC settings: Include -fopenmp as argument for threading support
            # 
            # CC='gcc'
            # CFLAGS='-D_GNU_SOURCE -fopenmp'
            # LDFLAGS="-fopenmp -pthread -shared -Wl,--version-script,$TMW_ROOT/extern/lib/$Arch/$MAPFILE -Wl,--no-undefined"
            # 



