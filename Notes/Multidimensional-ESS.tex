\documentclass[11pt]{article}
\usepackage{geometry}
\usepackage{graphicx}
\geometry{a4paper}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{authblk}
\usepackage{setspace}
\usepackage[fleqn]{mathtools}
\usepackage[sort&compress,round]{natbib}
\renewcommand{\baselinestretch}{1.5}

\title{Hessian and Jacobian matrix calculation}

\author{Andr\'e M. de Roos\thanks{A.M.deRoos@uva.nl}}

\affil{Institute for Biodiversity and Ecosystem Dynamics, University of Amsterdam, Amsterdam, The Netherlands}

\renewcommand\Authands{ and }

\newcommand{\epsh}{\epsilon_h}
\newcommand{\epsp}{\epsilon_p}
\newcommand{\essh}{\tilde{h}}
\newcommand{\essp}{\tilde{p}}

\begin{document}
\maketitle
\subsection*{Prelude}
A mixed derivative of a function $f(h,p)$ of 2 arguments $h$ and $p$ can be approximated as:
\begin{equation}
  \dfrac{\partial^2 f(h,p)}{\partial h \partial p}\;\approx\;\dfrac{f(h + \epsh, p + \epsp) - f(h + \epsh, p - \epsp) - f(h - \epsh, p + \epsp) + f(h - \epsh, p - \epsp)}{4\epsh \epsp}
\end{equation}
as long as the function $f(h,p)$ has continuous derivatives up to order 4 within the rectangle with corner points $(h - \epsh, p - \epsp)$ and $(h + \epsh, p + \epsp)$. The error is in the order or $\epsh^2\epsp^2$.

\subsection*{Selection gradients}
In Adapative Dynamics (AD) we have the derivative of $R_0$ w.r.t. the mutant parameter as selection gradient. Let's refer to this selection gradient as $\partial_hR_0(h,p, E(h^\prime,p^\prime))$ and $\partial_pR_0(h,p, E(h^\prime,p^\prime))$, in which $h$ and $h^\prime$ are the trait values of the mutant and the resident, respectively. Analogously, for $p$ and $p^\prime$. $E(h^\prime,p^\prime)$ is the environmental condition as determined by the resident. The selection gradient w.r.t. $h$ at a particular ESS point with trait combination $(\essh,\essp)$ can now be approximated as:
\begin{equation}
\label{eq:selgradh}
\partial_hR_0(h,p, E(h^\prime,p^\prime))\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}\approx \dfrac{R_0(h+\epsh, p, E(h^\prime,p^\prime)) - R_0(h-\epsh, p, E(h^\prime,p^\prime))}{2\epsh}\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
\end{equation}
while the selection gradient w.r.t. $p$ can be approximated as:
\begin{equation}
\label{eq:selgradp}
\partial_pR_0(h,p, E(h^\prime,p^\prime))\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}\approx \dfrac{R_0(h, p+\epsp, E(h^\prime,p^\prime)) - R_0(h, p-\epsp, E(h^\prime,p^\prime))}{2\epsp}\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
\end{equation}

\subsection*{Hessian matrix}
Let's define the Hessian matrix of the canonical equation as:
\begin{equation}
  \mathbf{H}\;=\;\begin{pmatrix}
    h_{11} & h_{12} \\ h_{21} & h_{22}
  \end{pmatrix}
\end{equation}
The Hessian matrix elements represent the second order derivatives of the selection gradients $\partial_hR_0(h,p, E(h^\prime,p^\prime))$ and $\partial_pR_0(h,p, E(h^\prime,p^\prime))$ with respect to the \textit{mutant} parameters. The elements can be computed by numerical differentiation of the expressions~(\ref{eq:selgradh}) and (\ref{eq:selgradp}). To compute the diagonal element $h_{11}$ of the Hessian matrix, for example, first the selection gradient $\partial_hR_0(h,p, E(h^\prime,p^\prime))$ can be approximated by its central difference approximation with step size $\epsh/2$:
\begin{displaymath}
\partial_hR_0(h,p, E(h^\prime,p^\prime))\approx \dfrac{R_0(h+\epsh/2, p, E(h^\prime,p^\prime)) - R_0(h-\epsh/2, p, E(h^\prime,p^\prime))}{\epsh}    
\end{displaymath}
To compute the second order derivative with respect to $h$ of the selection gradient $\partial_hR_0(h,p, E(h^\prime,p^\prime))$ each of the two terms in the numerator of this latter expression can be approximated by its central finite difference approximation with step size $\epsh/2$:
\begin{align*}
  &\dfrac{\partial}{\partial h}\dfrac{R_0(h+\epsh/2, p, E(h^\prime, p^\prime)) - R_0(h-\epsh/2, p, E(h^\prime, p^\prime))}{\epsh}\approx\\[2ex]
  &\dfrac{{\dfrac{R_0(h+\epsh, p, E(h^\prime, p^\prime)) - R_0(h, p, E(h^\prime, p^\prime))}{\epsh}}{ - \dfrac{R_0(h, p, E(h^\prime, p^\prime)) - R_0(h-\epsh, p, E(h^\prime, p^\prime))}{\epsh}}}{\epsh}\\[2ex]
  &=\;\dfrac{R_0(h+\epsh, p, E(h^\prime, p^\prime)) - 2 R_0(h, p, E(h^\prime, p^\prime))+ R_0(h-\epsh, p, E(h^\prime, p^\prime))}{\epsh^2}
\end{align*}
The diagonal elements of the Hessian can therefore be computed as:
\begin{displaymath}
  h_{11}\;=\;\dfrac{R_0(\essh+\epsh, \essp, E(\essh, \essp)) - 2R_0(\essh, \essp, E(\essh, \essp))+ R_0(\essh-\epsh, \essp, E(\essh, \essp))}{\epsh^2}
\end{displaymath}
and
\begin{displaymath}
  h_{22}\;=\;\dfrac{R_0(\essh, \essp+\epsp, E(\essh, \essp)) - 2R_0(\essh, \essp, E(\essh, \essp))+ R_0(\essh, \essp-\epsp, E(\essh, \essp))}{\epsp^2}
\end{displaymath}
To compute the off-diagonal elements of the Hessian matrix, the derivative with respect to $p$ of the (numerical approximation to the) selection gradient~(\ref{eq:selgradh}) is approximated with its central difference approximation with step size $\epsp$:
\begin{align*}
  &\dfrac{\partial}{\partial p}\dfrac{R_0(h+\epsh, p, E(h^\prime, p^\prime)) - R_0(h-\epsh, p, E(h^\prime, p^\prime))}{2\epsh}\\[2ex]
  &\quad\approx\dfrac{\splitdfrac{\dfrac{R_0(h+\epsh, p+\epsp, E(h^\prime, p^\prime)) - R_0(h+\epsh, p-\epsp, E(h^\prime, p^\prime))}{2\epsp}}{\qquad\qquad - \dfrac{R_0(h-\epsh, p+\epsp, E(h^\prime, p^\prime)) - R_0(h-\epsh, p-\epsp, E(h^\prime, p^\prime))}{2\epsp}}}{2\epsh}\\[2ex]
  &\quad=\;\dfrac{\splitdfrac{R_0(h+\epsh, p+\epsp, E(h^\prime, p^\prime)) - R_0(h+\epsh, p-\epsp, E(h^\prime, p^\prime))}{\qquad - R_0(h-\epsh, p+\epsp, E(h^\prime, p^\prime)) + R_0(h-\epsh, p-\epsp, E(h^\prime, p^\prime))}}{4\epsh\epsp}
\end{align*}
The expression for $h_{12}$ hence becomes:
\begin{displaymath}
  h_{12}\;=\;\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp+\epsp, E(\essh, \essp)) - R_0(\essh-\epsh, \essp+\epsp, E(\essh, \essp))}{\qquad - R_0(\essh+\epsh, \essp-\epsp, E(\essh, \essp)) + R_0(\essh-\epsh, \essp-\epsp, E(\essh, \essp))}}{4\epsh\epsp}
\end{displaymath}
while the same expression results for $h_{21}$ when using the central difference approximation to the derivative of the selection gradient~(\ref{eq:selgradp}) with respect to $h$ with a step size equal to $\epsh$. The Hessian matrix is therefore always symmetric and hence $h_{12}=h_{21}$. 

The absence of a factor 4 in the expressions for the diagonal elements $h_{11}$ and $h_{22}$ compared to the expressions for $h_{12}$ and $h_{21}$ results from the fact that the diagonal elements are computed with a step size of $\epsh/2$ instead of $\epsh$. For symmetry purposes it is perhaps better to use step sizes $\epsh$ and $\epsp$ throughout. In this case, the expressions for the elements of the Hessian matrix become:
\begin{align}
  h_{11}&=\;\dfrac{R_0(\essh+2\epsh, \essp, E(\essh, \essp)) - 2R_0(\essh, \essp, E(\essh, \essp))+ R_0(\essh-2\epsh, \essp, E(\essh, \essp))}{4\epsh^2}\\[2ex]
  h_{12}&=\;h_{21}\;=\;\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp+\epsp, E(\essh, \essp)) - R_0(\essh-\epsh, \essp+\epsp, E(\essh, \essp))}{\qquad - R_0(\essh+\epsh, \essp-\epsp, E(\essh, \essp)) + R_0(\essh-\epsh, \essp-\epsp, E(\essh, \essp))}}{4\epsh\epsp}\\[2ex]
  h_{22}&=\;\dfrac{R_0(\essh, \essp+2\epsp, E(\essh, \essp)) - 2R_0(\essh, \essp, E(\essh, \essp))+ R_0(\essh, \essp-2\epsp, E(\essh, \essp))}{4\epsp^2}
\end{align}

\clearpage  
\subsection*{Jacobian matrix and matrix of cross derivatives}
Let's define the Jacobian matrix of the canonical equation as:
\begin{equation}
  \mathbf{J}\;=\;\begin{pmatrix}
    j_{11} & j_{12} \\ j_{21} & j_{22}
  \end{pmatrix}
\end{equation}
The Jacobian matrix of the canonical equation is the complete derivative of the selection gradients $\partial_hR_0(h,p, E(h,p))$ and $\partial_pR_0(h,p, E(h,p))$ with respect to $h$ and $p$, respectively. Notice that the derivatives $\partial_h$ and $\partial_p$ in these selection gradients are with respect to the first arguments $h$ and $p$, only. The derivatives are hence with respect to the \textit{mutant} parameters. The Jacobian, however, contains the full derivatives, which means derivatives with respect to both the mutant and the resident parameters. Therefore,
\begin{align*}
  j_{11}&=\dfrac{d}{dh}\left(\partial_hR_0(h,p, E(h,p))\right)\biggl|_{\shortstack{$\scriptstyle h=\essh$\\$\scriptstyle p=\essp$}}\\[2ex]
  &=\dfrac{\partial^2}{\partial h^2}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
  \;+\;\dfrac{\partial}{\partial h^\prime}\dfrac{\partial}{\partial h}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  j_{12}&=\dfrac{d}{dp}\left(\partial_hR_0(h,p, E(h,p))\right)\biggl|_{\shortstack{$\scriptstyle h=\essh$\\$\scriptstyle p=\essp$}}\\[2ex]
  &=\dfrac{\partial^2}{\partial h \partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
  \;+\;\dfrac{\partial}{\partial p^\prime}\dfrac{\partial}{\partial h}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  j_{21}&=\dfrac{d}{dh}\left(\partial_pR_0(h,p, E(h,p))\right)\biggl|_{\shortstack{$\scriptstyle h=\essh$\\$\scriptstyle p=\essp$}}\\[2ex]
  &=\dfrac{\partial^2}{\partial h \partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
  \;+\;\dfrac{\partial}{\partial h^\prime}\dfrac{\partial}{\partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  j_{22}&=\dfrac{d}{dp}\left(\partial_pR_0(h,p, E(h,p))\right)\biggl|_{\shortstack{$\scriptstyle h=\essh$\\$\scriptstyle p=\essp$}}\\[2ex]
  &=\dfrac{\partial^2}{\partial p^2}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
  \;+\;\dfrac{\partial}{\partial p^\prime}\dfrac{\partial}{\partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
\end{align*}
From these expressions it can be seen that the Jacobian is the sum of the Hessian matrix $\mathbf{H}$ and the matrix $\mathbf{C}$ of cross-derivatives, defined as:
\begin{equation}
  \mathbf{C}\;=\;\begin{pmatrix}
    c_{11} & c_{12} \\ c_{21} & c_{22}
  \end{pmatrix}
\end{equation}
with elements
\begin{align*}
  c_{11}&=\dfrac{\partial}{\partial h^\prime}\dfrac{\partial}{\partial h}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  c_{12}&=\dfrac{\partial}{\partial p^\prime}\dfrac{\partial}{\partial h}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  c_{21}&=\dfrac{\partial}{\partial h^\prime}\dfrac{\partial}{\partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$ \\ $\scriptstyle p=p^\prime=\essp$}}\\[2ex]
  c_{22}&=\dfrac{\partial}{\partial p^\prime}\dfrac{\partial}{\partial p}\left(R_0(h,p, E(h^\prime,p^\prime))\right)\biggl|_{\shortstack{$\scriptstyle h=h^\prime=\essh$\\$\scriptstyle p=p^\prime=\essp$}}
\end{align*}
Hence, 
\begin{equation}
  \mathbf{J}\;=\;\mathbf{H}\;+\;\mathbf{C}
\end{equation}
To compute the element $c_{11}$ of the matrix of cross-derivatives the selection gradient $\partial_hR_0(h,p, E(h^\prime,p^\prime))$ can be approximated by its central difference approximation with step size $\epsh$:
\begin{displaymath}
\partial_hR_0(h,p, E(h^\prime,p^\prime))\approx \dfrac{R_0(h+\epsh, p, E(h^\prime,p^\prime)) - R_0(h-\epsh, p, E(h^\prime,p^\prime))}{2\epsh}    
\end{displaymath}
Subsequently, the derivative with respect to $h^\prime$ of each of the terms in the numerator of this expression can be replaced by its central difference approximation with step size $\epsh$, leading to:
\begin{align*}
&\dfrac{\partial}{\partial h^\prime}\dfrac{\partial}{\partial h}\left(R_0(h,p, E(h^\prime,p^\prime))\right) \approx \dfrac{\partial}{\partial h^\prime}\dfrac{R_0(h+\epsh, p, E(h^\prime,p^\prime)) - R_0(h-\epsh, p, E(h^\prime,p^\prime))}{2\epsh}\\[4ex]
&\qquad\approx \dfrac{\splitdfrac{\dfrac{R_0(h+\epsh, p, E(h^\prime+\epsh,p^\prime)) - R_0(h+\epsh, p, E(h^\prime-\epsh,p^\prime))}{2\epsh}}{\qquad - \dfrac{R_0(h-\epsh, p, E(h^\prime+\epsh,p^\prime)) - R_0(h-\epsh, p, E(h^\prime-\epsh,p^\prime))}{2\epsh}}}{2\epsh}\\[2ex]
&\qquad\approx \dfrac{\splitdfrac{R_0(h+\epsh, p, E(h^\prime+\epsh,p^\prime)) - R_0(h-\epsh, p, E(h^\prime+\epsh,p^\prime))}{\qquad - R_0(h+\epsh, p, E(h^\prime-\epsh,p^\prime)) + R_0(h-\epsh, p, E(h^\prime-\epsh,p^\prime))}}{4\epsh^2}
\end{align*}
The central difference approximations to the elements of the matrix of cross derivatives are therefore given by:
\begin{align*}
  c_{11}&=\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp, E(\essh+\epsh,\essp)) - R_0(\essh-\epsh, \essp, E(\essh+\epsh,\essp))}{\qquad - R_0(\essh+\epsh, \essp, E(\essh-\epsh,\essp)) + R_0(\essh-\epsh, \essp, E(\essh-\epsh,\essp))}}{4\epsh^2}\\[2ex]
  c_{12}&=\dfrac{\splitdfrac{R_0(\essh, \essp+\epsp, E(\essh+\epsh,\essp)) - R_0(\essh, \essp-\epsp, E(\essh+\epsh,\essp))}{\qquad - R_0(\essh, \essp+\epsp, E(\essh-\epsh,\essp)) + R_0(\essh, \essp-\epsp, E(\essh-\epsh,\essp))}}{4\epsh\epsp}\\[2ex]
  c_{21}&=\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp, E(\essh,\essp+\epsp)) - R_0(\essh-\epsh, \essp, E(\essh,\essp+\epsp))}{\qquad - R_0(\essh+\epsh, \essp, E(\essh,\essp-\epsp)) + R_0(\essh-\epsh, \essp, E(\essh,\essp-\epsp))}}{4\epsh\epsp}\\[2ex]
  c_{22}&=\dfrac{\splitdfrac{R_0(\essh, \essp+\epsp, E(\essh,\essp+\epsp)) - R_0(\essh, \essp-\epsp, E(\essh,\essp+\epsp))}{\qquad - R_0(\essh, \essp+\epsp, E(\essh,\essp-\epsp)) + R_0(\essh, \essp-\epsp, E(\essh,\essp-\epsp))}}{4\epsp^2}
\end{align*}

To compute the entries of the Jacobian matrix the corresponding entries of the Hessian matrix and the matrix of cross derivatives can be added.

%\begin{align*}
%  j_{11}&=\;\dfrac{R_0(\essh+2\epsh, \essp, E(\essh, \essp)) - 2R_0(\essh, \essp, E(\essh, \essp))+ R_0(\essh-2\epsh, \essp, E(\essh, \essp))}{4\epsh^2}\\[2ex]
%  &\qquad+\;\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp, E(\essh+\epsh,\essp)) - R_0(\essh-\epsh, \essp, E(\essh+\epsh,\essp))}{\qquad - R_0(\essh+\epsh, \essp, E(\essh-\epsh,\essp)) + R_0(\essh-\epsh, \essp, E(\essh-\epsh,\essp))}}{4\epsh^2}
%\end{align*}
%
%The Jacobian matrix is not symmetric. The element $j_{12}$ can be computed as:
%\begin{equation}
%  j_{12}\;=\;\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp+\epsp, E(\essh, \essp+\epsp)) - R_0(\essh-\epsh, \essp+\epsp, E(\essh, \essp+\epsp))}{\qquad - R_0(\essh+\epsh, \essp-\epsp, E(\essh, \essp-\epsp)) + R_0(\essh-\epsh, \essp-\epsp, E(\essh, \essp-\epsp))}}{4\epsh\epsp}  
%\end{equation}
%while $j_{21}$ is computed from:
%\begin{equation}
%  j_{21}\;=\;\dfrac{\splitdfrac{R_0(\essh+\epsh, \essp+\epsp, E(\essh+\epsh, \essp)) - R_0(\essh+\epsh, \essp-\epsp, E(\essh+\epsh, \essp))}{\qquad - R_0(\essh-\epsh, \essp+\epsp, E(\essh-\epsh, \essp)) + R_0(\essh-\epsh, \essp-\epsp, E(\essh-\epsh, \essp))}}{4\epsh\epsp}  
%\end{equation}


%\bibliographystyle{agsm}
%\bibliography{bibliography}

\end{document}
