/*
 Onto_scaling_TPB1998.h
 
 Header file specifying the life history an invertebrate species feeding on 2, unstructured resources (stage-structured biomass model)
 
 
 
 Last modification: Htb - Sept 21 2015
 added: metamorphosis
 */

/*
 *===========================================================================
 * 		DEFINITION OF PROBLEM DIMENSIONS AND NUMERICAL SETTINGS
 *===========================================================================
 */
// Dimension settings: Required
#define POPULATION_NR		1
#define STAGES              3
#define	I_STATE_DIM         2
#define	ENVIRON_DIM         2
#define INTERACT_DIM		8
#define	PARAMETER_NR		17

// Numerical settings: Optional (default values adopted otherwise)
#define MIN_SURVIVAL		1.0E-12		// Survival at which individual is considered dead
#define MAX_AGE             100000		// Give some absolute maximum for individual age

#define DYTOL               1.0E-7		// Variable tolerance
#define RHSTOL              1.0E-6		// Function tolerance
#define ESSTOL              1.0E-6

/*
 *===========================================================================
 * 		DEFINITION OF ALIASES
 *===========================================================================
 */
// Define aliases for the istate variables
#define AGE         istate[0][0]
#define WEIGHT      istate[0][1]            // Mass
#define Survive     exp(istate[0][2])

// Define aliases for the environmental variables
#define R1           E[0]
#define R2           E[1]

// These are other variables or parameters occurring in the equations
#define Rho         parameter[ 0]	// Default: 0.1
#define R1MAX		parameter[ 1]	// Default:
#define R2MAX		parameter[ 2]	// Default:

#define PHI         parameter[ 3]	// Default: 1
#define PSI         parameter[ 4]	// Default:
#define AMAX        parameter[ 5]	// Default:

#define WAR         parameter[ 6]	// Default:
#define SIGMA       parameter[ 7]	// Default:

#define MURX        parameter[ 8]   // Default:
#define MURL        parameter[ 9]	// Default:
#define MURJ        parameter[10]	// Default:
#define MURA        parameter[11]	// Default:
#define HAB         parameter[12]	// Default:
#define Q           parameter[13]	// Default:

#define H           parameter[14]	// Default:
#define ZRL         parameter[15]	// Default:
#define ZRJ         parameter[16]	// Default:




/*
 *===========================================================================
 * 		DEFINITION OF NAMES AND DEFAULT VALUES OF THE PARAMETERS
 *===========================================================================
 */
// At least two parameters should be specified in this array
char  *parameternames[PARAMETER_NR] =
{"Rho","R1max","R2max","PHI","PSI","AMAX","WAR","SIGMA","MURX",
"MURL","MURJ","MURA","HAB","Q","H","ZRl","ZRJ"};

// These are the default parameters values
double	parameter[PARAMETER_NR] =
{0.1, 10, 9, 0.8932305,  0.93709161, 0.6, 0.0001, 0.5, 0, 0, 0, 0, 0, 1, 1, 0.1, 0.1};

/*
 *===========================================================================
 * 		DEFINITION OF THE LIFE HISTORY MODELS FOLLOWS BELOW
 *===========================================================================
 * Specify the number of states at birth for the individuals in all structured
 * populations in the problem in the vector BirthStates[].
 *===========================================================================
 */

void SetBirthStates(int BirthStates[POPULATION_NR], double E[])
{
//     static int     first = 1;
//     FILE            *fp;
//     
//     if (first)
//     {
//         fp = fopen("InputPars.txt", "r");
//         fscanf(fp, "%lG %lG", parameter+1, parameter+2);
//         fclose(fp);
//         first = 0;
//         fprintf(outfile, "#\n# X1max set to:%G\n# X2max set to:%G\n\n", parameter[1], parameter[2]);
//         first = 0;
//     }
    BirthStates[0] = 1;
    
    return;
}


/*
 *===========================================================================
 * Specify all the possible states at birth for all individuals in all
 * structured populations in the problem. BirthStateNr represents the index of
 * the state of birth to be specified. Each state at birth should be a single,
 * constant value for each i-state variable.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */
void StateAtBirth(double *istate[POPULATION_NR], int BirthStateNr, double E[])
{
    
    AGE = 0.0;
    WEIGHT = WAR*ZRL*ZRJ;
    
    
    return;
}


/*
 *===========================================================================
 * Specify the threshold determining the end point of each discrete life
 * stage in individual life history as function of the i-state variables and
 * the individual's state at birth for all populations in every life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void IntervalLimit(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                   double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
                   double limit[POPULATION_NR])
{
    double XJ;
    XJ = WAR*ZRJ;
    switch (lifestage[0])
    {
        case 0:
            limit[0] = WEIGHT - XJ;
            break;
        case 1:
            limit[0] = WEIGHT - WAR;
            break;
        case 2:
            limit[0] = AGE - MAX_AGE;
            break;
    }
    
    return;
}




/*
 *===========================================================================
 * Specify the individual development of individuals as function of i-state
 * and environment for all individuals of all populations in every life stage
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void Development(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                 double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
                 double development[POPULATION_NR][I_STATE_DIM])
{
    

    double          a2, a1,Wcr, Tr, Mur, W1,W2,a_L,attack1,attack2,nurl,nurj,nura;
    double          XJ,XB;
    
    
    XB =    WAR*ZRL*ZRJ;
    XJ =    WAR*ZRJ;
    
    
    Wcr =   pow(WAR,-0.25);
    Tr  =   0.01*Wcr;
    Mur =   0.002*Wcr;
    a1  =   PSI*AMAX;
    a2  =   AMAX-a1;
    
    
    W1      =   HAB+(1-HAB)*PHI;
    W2      =   HAB+(1-HAB)*(1-PHI);
    a_L     =   Q*a1*R1/(1+Q*a1*H*R1);               //Attack rate Larvae
    attack1 =   W1*a1*R1/(1+W1*a1*H*R1+W2*a2*H*R2);  //Attack rate on X1 for Juveniles and adults
    attack2 =   W2*a2*R2/(1+W1*a1*H*R1+W2*a2*H*R2);  //Attack rate on X2 for Juveniles and adults
    nurl    =   SIGMA*a_L-Tr;
    nurj    =   SIGMA*(attack1+attack2)-Tr;
    nura    =   SIGMA*(attack1+attack2)-Tr;
    
    development[0][0] = 1;
    
    if (lifestage[0] == 0)
    {
        
        development[0][1]= nurl*WEIGHT;
        
    }
    else if (lifestage[0] == 1)
    {
        
        development[0][1] = nurj*WEIGHT;
        
    }
    else
    {
        development[0][1] = 0;
        
    }
    
    
    return;
}


/*
 *===========================================================================
 * Specify the possible discrete changes (jumps) in the individual state
 * variables when ENTERING the stage specified by 'lifestage[]'.
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void DiscreteChanges(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                     double *birthstate[POPULATION_NR], int BirthStateNr, double E[])
{
    return;
}


/*
 *===========================================================================
 * Specify the fecundity of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * The number of offspring produced has to be specified for every possible
 * state at birth in the variable 'fecundity[][]'. The first index of this
 * variable refers to the number of the structured population, the second
 * index refers to the number of the birth state.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Fecundity(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double *fecundity[POPULATION_NR])
{

    double          a2, a1,Wcr, Tr, Mur, murtotl,murtotj,murtota,W1,W2,a_L,attack1,attack2,nurl,nurj,nura;
    double          XJ,XB;

    
    XB =    WAR*ZRL*ZRJ;
    XJ =    WAR*ZRJ;
    
    
    Wcr =   pow(WAR,-0.25);
    Tr  =   0.01*Wcr;
    Mur =   0.002*Wcr;
    
    murtotl =   Mur+MURL+MURX;
    murtotj =   Mur+MURJ+MURX;
    murtota =   Mur+MURA+MURX;
    a1  =   PSI*AMAX;
    a2  =   AMAX-a1;
    W1      =   HAB+(1-HAB)*PHI;
    W2      =   HAB+(1-HAB)*(1-PHI);
    a_L     =   Q*a1*R1/(1+Q*a1*H*R1);
    attack1 =   W1*a1*R1/(1+W1*a1*H*R1+W2*a2*H*R2);
    attack2 =   W2*a2*R2/(1+W1*a1*H*R1+W2*a2*H*R2);
    nurl    =   SIGMA*a_L-Tr;
    nurj    =   SIGMA*(attack1+attack2)-Tr;
    nura    =   SIGMA*(attack1+attack2)-Tr;
    
    if (lifestage[0] == 0)
    {
        fecundity[0][0] = 0.0;
        
    }
    else if (lifestage[0] == 1)
    {
        fecundity[0][0] = 0.0;
       
    }
    else
    {
        fecundity[0][0] = (nura*WEIGHT)/XB;
    }
    
    
    
    return;
}



/*
 *===========================================================================
 * Specify the mortality of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Mortality(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double mortality[POPULATION_NR])
{
    double Wcr,Mur,murtotl,murtotj,murtota;
    
    Wcr =   pow(WAR,-0.25);
    Mur =   0.002*Wcr;
    
    murtotl =   Mur+MURL+MURX;
    murtotj =   Mur+MURJ+MURX;
    murtota =   Mur+MURA+MURX;
    
    switch(lifestage[0])
    {
        case 0:
            mortality[0] = murtotl;
            break;
        case 1:
            mortality[0] = murtotj;
            break;
        case 2:
            mortality[0] = murtota;
            break;
    }
    
    
    
    return;
}


/*
 *===========================================================================
 * For all the integrals (measures) that occur in interactions of the
 * structured populations with their environments and for all the integrals
 * that should be computed for output purposes (e.g. total juvenile or adult
 * biomass), specify appropriate weighing function dependent on the i-state
 * variables, the environment variables and the current life stage of the
 * individuals. These weighing functions should be specified for all
 * structured populations in the problem. The number of weighing functions
 * is the same for all of them.
 *
 * Notice that the first index of the variables 'istate[][]' and 'impact[][]'
 * refers to the number of the structured population, the second index of the
 * variable 'istate[][]' refers to the number of the individual state variable,
 * while the second index of the variable 'impact[][]' refers to the number of
 * the interaction variable. The interpretation of these second indices is up
 * to the user.
 *===========================================================================
 */

void Impact(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
            double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
            double impact[POPULATION_NR][INTERACT_DIM])
{

    double          a2,a1, Wcr, Tr, Mur, murtotl,murtotj,murtota,W1,W2,a_L,attack1,attack2,nurl,nurj,nura;
    double          XJ,XB, In1, In2;

    
    
    
    XB =    WAR*ZRL*ZRJ;
    XJ =    WAR*ZRJ;
    
    
    Wcr =   pow(WAR,-0.25);
    Tr  =   0.01*Wcr;
    Mur =   0.002*Wcr;
    
    murtotl =   Mur+MURL+MURX;
    murtotj =   Mur+MURJ+MURX;
    murtota =   Mur+MURA+MURX;
    a1      =   PSI*AMAX;
    a2      =   AMAX-a1;
    W1      =   HAB+(1-HAB)*PHI;
    W2      =   HAB+(1-HAB)*(1-PHI);
    a_L     =   Q*a1*R1/(1+Q*a1*H*R1);
    attack1 =   W1*a1*R1/(1+W1*a1*H*R1+W2*a2*H*R2);
    attack2 =   W2*a2*R2/(1+W1*a1*H*R1+W2*a2*H*R2);
    nurl    =   SIGMA*a_L-Tr;
    nurj    =   SIGMA*(attack1+attack2)-Tr;
    nura    =   SIGMA*(attack1+attack2)-Tr;
    
    if (lifestage[0] == 0)
    {
        
        In1 = a_L*WEIGHT;
        In2 = 0;
    }
    else if (lifestage[0] == 1)
    {
       
        In1 = attack1*WEIGHT;
        In2 = attack2*WEIGHT;
    }
    else
    {
       
        In1 = attack1*WEIGHT;
        In2 = attack2*WEIGHT;
    }
    
    
    
    switch (lifestage[0])
    {
        case 0:
            impact[0][0] = In1;                                             // Ingestion R1
            impact[0][1] = 0.0;                                             // Ingestion R2
            impact[0][2] = WEIGHT;                                          // Larvae  mass
            impact[0][3] = WEIGHT;                                             // Juvenile mass
            impact[0][4] = 0.0;                                             //Adult mass
            impact[0][5] = 1.0;                                             // Larvae
            impact[0][6] = 0.0;                                             // Juveniles
            impact[0][7] = 0.0;                                             // Adults
            break;
            
        case 1:
            impact[0][0] = In1;                                             // Ingestion R1
            impact[0][1] = In2;                                             // Ingestion R2
            impact[0][2] = WEIGHT;                                             // Larvae  mass
            impact[0][3] = WEIGHT;                                          // Juvenile mass
            impact[0][4] = 0.0;                                             //Adult mass
            impact[0][5] = 0.0;                                             // Larvae
            impact[0][6] = 1.0;                                             // Juveniles
            impact[0][7] = 0.0;                                             // Adults
            break;
            
        case 2:
            impact[0][0] = In1;                                             // Ingestion R1
            impact[0][1] = In2;                                             // Ingestion R2
            impact[0][2] = 0.0;                                             // Larvae  mass
            impact[0][3] = 0.0;                                             // Juvenile mass
            impact[0][4] = WEIGHT;                                          // Adult mass
            impact[0][5] = 0.0;                                             // Larvae
            impact[0][6] = 0.0;                                             // Juveniles
            impact[0][7] = 1.0;                                             // Adults
            break;
    }
    return;
}


/*
 *===========================================================================
 * Specify the type of each of the environment variables by setting
 * the entries in EnvironmentType[ENVIRON_DIM] to PERCAPITARATE, GENERALODE
 * or POPULATIONINTEGRAL based on the classification below:
 *
 * Set an entry to PERCAPITARATE if the dynamics of E[j] follow an ODE and 0
 * is a possible equilibrium state of E[j]. The ODE is then of the form
 * dE[j]/dt = P(E,I)*E[j], with P(E,I) the per capita growth rate of E[j].
 * Specify the equilibrium condition as condition[j] = P(E,I), do not include
 * the multiplication with E[j] to allow for detecting and continuing the
 * transcritical bifurcation between the trivial and non-trivial equilibrium.
 *
 * Set an entry to GENERALODE if the dynamics of E[j] follow an ODE and 0 is
 * NOT an equilibrium state of E. The ODE then has a form dE[j]/dt = G(E,I).
 * Specify the equilibrium condition as condition[j] = G(E,I).
 *
 * Set an entry to POPULATIONINTEGRAL if E[j] is a (weighted) integral of the
 * population distribution, representing for example the total population
 * biomass. E[j] then can be expressed as E[j] = I[p][i]. Specify the
 * equilibrium condition in this case as condition[j] = I[p][i].
 *
 * Notice that the first index of the variable 'I[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the interaction variable. The interpretation of the latter
 * is up to the user. Also notice that the variable 'condition[j]' should
 * specify the equilibrium condition of environment variable 'E[j]'.
 *===========================================================================
 */

const int EnvironmentType[ENVIRON_DIM] = {GENERALODE};

void EnvEqui(double E[], double I[POPULATION_NR][INTERACT_DIM],
             double condition[ENVIRON_DIM])
{
    condition[0] = Rho*(R1MAX - R1) - I[0][0]*1.0E6;
    condition[1] = Rho*(R2MAX - R2) - I[0][1]*1.0E6;
    return;
}

/*==============================================================================*/

