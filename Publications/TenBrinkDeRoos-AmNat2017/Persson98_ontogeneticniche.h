/*
 Onto_scaling_TPB1998.h
 
 Header file specifying the life history of roach  (Ontogenetic scaling of foraging rates and the dynamics of a size-structured consumer-resource model. Persson et al 1998)
 
 
 
 Last modification: Htb - Sept 6 2016

 */

/*
 *===========================================================================
 * 		DEFINITION OF PROBLEM DIMENSIONS AND NUMERICAL SETTINGS
 *===========================================================================
 */
// Dimension settings: Required
#define POPULATION_NR		1
#define STAGES              2
#define	I_STATE_DIM         3
#define	ENVIRON_DIM         2
#define INTERACT_DIM		8
#define	PARAMETER_NR		23

// Numerical settings: Optional (default values adopted otherwise)
#define MIN_SURVIVAL		1.0E-12		// Survival at which individual is considered dead
#define MAX_AGE             100000		// Give some absolute maximum for individual age

#define DYTOL               1.0E-7		// Variable tolerance
#define RHSTOL              1.0E-6		// Function tolerance
#define ESSTOL              1.0E-6

/*
 *===========================================================================
 * 		DEFINITION OF ALIASES
 *===========================================================================
 */
// Define aliases for the istate variables
#define AGE         istate[0][0]
#define IR          istate[0][1]    // Irreversible mass
#define REV         istate[0][2]    // Reversible mass
#define Survive     exp(istate[0][3])

// Define aliases for the environmental variables
#define R1           E[0]
#define R2           E[1]

// These are their problem-specific meaning
#define Rho         parameter[ 0]	// Default: 0.5
#define R1MAX		parameter[ 1]	// Default: 5E+10
#define R2MAX		parameter[ 2]	// Default: 5E+10

#define AMAX        parameter[ 3]   // Default: 100000
#define ALPHA       parameter[ 4]   // Default: 0.93
#define W0          parameter[ 5]	// Default: 17.42

#define CHI1        parameter[ 6]   // Default: 4E-6
#define CHI2        parameter[ 7]	// Default: 8.19E-5
#define	CHI3        parameter[ 8]	// Default: 0.68
#define	CHI4        parameter[ 9]	// Default: 1.15E-3

#define M1          parameter[10]	// Default: 0.033
#define M2          parameter[11]	// Default: 0.77

#define K1          parameter[12]	// Default: 6.71E-6
#define K2          parameter[13]	// Default: 0.5

#define X0          parameter[14]	// Default:
#define XJ          parameter[15]
#define XF          parameter[16]	// Default:

#define QJ          parameter[17]	// Default:
#define QA          parameter[18]	// Default:
#define MU0         parameter[19]   // Default:

#define PHI         parameter[20]
#define PSI         parameter[21]
#define HAB         parameter[22]




/*
 *===========================================================================
 * 		DEFINITION OF NAMES AND DEFAULT VALUES OF THE PARAMETERS
 *===========================================================================
 */
// At least two parameters should be specified in this array
char  *parameternames[PARAMETER_NR] =
{"Rho","R1max","R2max","Amax","Alpha","W0","ch1","chi2","chi3","chi4","m1","m2","k1","k2","x0","Xj","Xf","Qj","Qa","Mu0","phi","psi", "hab"};

// These are the default parameters values
double	parameter[PARAMETER_NR] =
{0.5,5,15,100000,0.93,17.42,4E-6,8.19e-5,0.68,1.15e-3,0.033,0.77,6.71e-6,0.5,0.000804,1,5,0.742,1,0.01,1,1,1};

/*
 *===========================================================================
 * 		DEFINITION OF THE LIFE HISTORY MODELS FOLLOWS BELOW
 *===========================================================================
 * Specify the number of states at birth for the individuals in all structured
 * populations in the problem in the vector BirthStates[].
 *===========================================================================
 */

void SetBirthStates(int BirthStates[POPULATION_NR], double E[])
{
    BirthStates[0] = 1;
    
    return;
}


/*
 *===========================================================================
 * Specify all the possible states at birth for all individuals in all
 * structured populations in the problem. BirthStateNr represents the index of
 * the state of birth to be specified. Each state at birth should be a single,
 * constant value for each i-state variable.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void StateAtBirth(double *istate[POPULATION_NR], int BirthStateNr, double E[])
{
    
    AGE = 0.0;
    IR = X0;
    REV = QJ*X0;
    
    return;
}


/*
 *===========================================================================
 * Specify the threshold determining the end point of each discrete life
 * stage in individual life history as function of the i-state variables and
 * the individual's state at birth for all populations in every life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void IntervalLimit(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                   double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
                   double limit[POPULATION_NR])
{
    switch (lifestage[0])
    {
        
        case 0:
            limit[0] = IR - XF;
            break;
        case 1:
            limit[0] = AGE - MAX_AGE;
            break;
    }
    
    return;
}




/*
 *===========================================================================
 * Specify the individual development of individuals as function of i-state
 * and environment for all individuals of all populations in every life stage
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void Development(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                 double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
                 double development[POPULATION_NR][I_STATE_DIM])
{
    
   
    double          w, h,a1,a2, gamma, Eg, Em,Ei,kappa_qj,kappa_qa;
    double          In1,In2,A2,wmin, W1,W2,A1;
    
    
    A1 = AMAX*PSI;                                   // Maximum attack rate R1
    A2 = AMAX*(1-PSI);                               // Maximum attack rate R2
    w = (1+QJ)*IR;                                   // Effective body mass
    wmin = ((1+QJ)*XJ);                              // Min Weight for R2
    h = (CHI1 + CHI2*pow(w, -CHI3)*exp(CHI4*w));     // Handling time
    a1 = A1*pow((w/W0) * exp(1 - (w/W0)), ALPHA);    // Size-dependent attack rate on R1
    W1=HAB+(1-HAB)*PHI;                              // In case habitats do no overlap
    W2=HAB+(1-HAB)*(1-PHI);                          // In case habitats do no overlap
        
        if (w - wmin <= 0)
        {
            a2 = 0;
            
        }
        else
        {
            a2 = A2*pow((w-wmin)/W0 * exp(1 - (w-wmin)/W0), ALPHA);                            //Attack rate on R2
            
        }
        gamma = (a1*W1*R1+a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));
        In1 =(a1*W1*R1) / (1 + h*(a1*W1*R1+a2*W2*R2));
        In2 = (a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));
    

    Em = M1*pow(IR+REV, M2);                                                                    // Maintenance cost
    Ei = (K1)*gamma;                                                                            // Conversion efficiency
    Eg = Ei - Em;                                                                               // Net production
    kappa_qj = 1 / ((1+QJ)*QJ) * (REV / IR);                                                    // Fraction allocated to irreversible mass juveniles
    kappa_qa = 1 / ((1+QA)*QA) * (REV / IR);                                                    // Fraction allocated to irreversible mass adults
    
    
    development[0][0] = 1.0;
    
    if (lifestage[0] == 0)
    {
        if (Eg > 0)
        {
            development[0][1] = kappa_qj*Eg;   //ir
            development[0][2] = (1-kappa_qj)*Eg; //rev
        }
        else
        {
            development[0][1] = 0.0;
            development[0][2] = Eg;
        }
    }
    
    
    else
    {
        if (Eg > 0)
        {
            development[0][1] = kappa_qa*Eg;  //ir
            development[0][2] = (QJ*kappa_qa)*Eg;  //rev
        }
        else
        {
            development[0][1] = 0.0;
            development[0][2] = Eg;
        }
    }
    
    return;
}


/*
 *===========================================================================
 * Specify the possible discrete changes (jumps) in the individual state
 * variables when ENTERING the stage specified by 'lifestage[]'.
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void DiscreteChanges(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                     double *birthstate[POPULATION_NR], int BirthStateNr, double E[])
{
    return;
}


/*
 *===========================================================================
 * Specify the fecundity of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * The number of offspring produced has to be specified for every possible
 * state at birth in the variable 'fecundity[][]'. The first index of this
 * variable refers to the number of the structured population, the second
 * index refers to the number of the birth state.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Fecundity(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double *fecundity[POPULATION_NR])
{

    double          w, h,a1,a2, gamma, Eg, Em,Ei,kappa_qj,kappa_qa;
    double          In1,In2, A2,wmin, W1,W2,A1;
    
    
    
    A1 = AMAX*PSI;                                   // Maximum attack rate R1
    A2 = AMAX*(1-PSI);                               // Maximum attack rate R2
    w = (1+QJ)*IR;                                   // Effective body mass
    wmin = ((1+QJ)*XJ);                              // Min Weight for R2
    h = (CHI1 + CHI2*pow(w, -CHI3)*exp(CHI4*w));     // Handling time
    a1 = A1*pow((w/W0) * exp(1 - (w/W0)), ALPHA);    // Size-dependent attack rate on R1
    W1=HAB+(1-HAB)*PHI;                              // In case habitats do no overlap
    W2=HAB+(1-HAB)*(1-PHI);                          // In case habitats do no overlap
    

    if (w - wmin <= 0)
    {
        a2 = 0;
        
    }
    else
    {
        a2 = A2*pow((w-wmin)/W0 * exp(1 - (w-wmin)/W0), ALPHA);         // Size-dependent attack rate R2
        
    }
    gamma = (a1*W1*R1+a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));
    In1 =(a1*W1*R1) / (1 + h*(a1*W1*R1+a2*W2*R2));
    In2 = (a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));
    
    
    Em = M1*pow(IR+REV, M2);                                                                      // Maintenance cost
    Ei = (K1)*gamma;                                                                              // Conversion efficiency
    Eg = Ei - Em;                                                                                 // Net production
    kappa_qj = 1 / ((1+QJ)*QJ) * (REV / IR);                                                      // Fraction allocated to irreversible mass juveniles
    kappa_qa = 1 / ((1+QA)*QA) * (REV / IR);                                                     // Fraction allocated to irreversible mass adults
    
    
    
    
    if (lifestage[0] == 1)
    {
        if (Eg > 0.0)
        {
            fecundity[0][0] = K2*(1-(1+QJ)*kappa_qa)*Eg / ((1+QJ)*X0);
        }
        else
        {
            fecundity[0][0] = 0.0;
        }
    }
    else {
        fecundity[0][0] = 0.0;
    }
    
    
    
    return;
}



/*
 *===========================================================================
 * Specify the mortality of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Mortality(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double mortality[POPULATION_NR])
{
    
    
    mortality[0] = MU0;
    
    return;
}


/*
 *===========================================================================
 * For all the integrals (measures) that occur in interactions of the
 * structured populations with their environments and for all the integrals
 * that should be computed for output purposes (e.g. total juvenile or adult
 * biomass), specify appropriate weighing function dependent on the i-state
 * variables, the environment variables and the current life stage of the
 * individuals. These weighing functions should be specified for all
 * structured populations in the problem. The number of weighing functions
 * is the same for all of them.
 *
 * Notice that the first index of the variables 'istate[][]' and 'impact[][]'
 * refers to the number of the structured population, the second index of the
 * variable 'istate[][]' refers to the number of the individual state variable,
 * while the second index of the variable 'impact[][]' refers to the number of
 * the interaction variable. The interpretation of these second indices is up
 * to the user.
 *===========================================================================
 */

void Impact(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
            double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
            double impact[POPULATION_NR][INTERACT_DIM])
{

    double          w, h,a1,a2, gamma, Eg, Em,Ei,kappa_qj,kappa_qa;
    double          In1,In2, A2,wmin, W1,W2, A1;
    
    
    
    A1 = AMAX*PSI;                                   // Maximum attack rate R1
    A2 = AMAX*(1-PSI);                               // Maximum attack rate R2
    w = (1+QJ)*IR;                                   // Effective body mass
    wmin = ((1+QJ)*XJ);                              // Min Weight for R2
    h = (CHI1 + CHI2*pow(w, -CHI3)*exp(CHI4*w));     // Handling time
    a1 = A1*pow((w/W0) * exp(1 - (w/W0)), ALPHA);    // Size-dependent attack rate on R1
    W1=HAB+(1-HAB)*PHI;                              // In case habitats do no overlap
    W2=HAB+(1-HAB)*(1-PHI);                          // In case habitats do no overlap
    
    

        if (w - wmin <= 0)
        {
            a2 = 0;
            
        }
        else
        {
            a2 = A2*pow((w-wmin)/W0 * exp(1 - (w-wmin)/W0), ALPHA);
            
        }
        gamma = (a1*W1*R1+a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));
        In1 =(a1*W1*R1) / (1 + h*(a1*W1*R1+a2*W2*R2));
        In2 = (a2*W2*R2) / (1 + h*(a1*W1*R1+a2*W2*R2));

    Em = M1*pow(IR+REV, M2);                                                                      // Maintenance cost
    Ei = (K1)*gamma;                                                                              // Conversion efficiency
    Eg = Ei - Em;                                                                                 // Net production
    kappa_qj = 1 / ((1+QJ)*QJ) * (REV / IR);                                                      // Fraction allocated to irreversible mass juveniles
    kappa_qa = 1 / ((1+QA)*QA) * (REV / IR);                                                      // Fraction allocated to irreversible mass adults
    
    
    
    
    switch (lifestage[0])
    {

        case 0:
            impact[0][0] = In1;                                             // Ingestion R1
            impact[0][1] = In2;                                             // Ingestion R2
            impact[0][2] = IR;                                              // Juvenile irreversible mass
            impact[0][3] = REV;                                             // Juvenilee reversible mass
            impact[0][4] = 0.0;                                             // Adults irreversible mass
            impact[0][5] = 0.0;                                             // Adults reversible mass
            impact[0][6] = 1.0;                                             // Number of juveniles
            impact[0][7] = 0.0;                                             // Number of adults
            break;
            
        case 1:
            impact[0][0] = In1;                                             // Ingestion R1
            impact[0][1] = In2;                                             // Ingestion R2
            impact[0][2] = 0.0;                                             // Juvenile irreversible mass
            impact[0][3] = 0.0;                                             // Juvenilee reversible mass
            impact[0][4] = IR;                                              // Adults irreversible mass
            impact[0][5] = REV;                                             // Juveniles reversible mass
            impact[0][6] = 0.0;                                             // Number of juveniles
            impact[0][7] = 1.0;                                             // Number of adults
            break;
    }
    return;
}


/*
 *===========================================================================
 * Specify the type of each of the environment variables by setting
 * the entries in EnvironmentType[ENVIRON_DIM] to PERCAPITARATE, GENERALODE
 * or POPULATIONINTEGRAL based on the classification below:
 *
 * Set an entry to PERCAPITARATE if the dynamics of E[j] follow an ODE and 0
 * is a possible equilibrium state of E[j]. The ODE is then of the form
 * dE[j]/dt = P(E,I)*E[j], with P(E,I) the per capita growth rate of E[j].
 * Specify the equilibrium condition as condition[j] = P(E,I), do not include
 * the multiplication with E[j] to allow for detecting and continuing the
 * transcritical bifurcation between the trivial and non-trivial equilibrium.
 *
 * Set an entry to GENERALODE if the dynamics of E[j] follow an ODE and 0 is
 * NOT an equilibrium state of E. The ODE then has a form dE[j]/dt = G(E,I).
 * Specify the equilibrium condition as condition[j] = G(E,I).
 *
 * Set an entry to POPULATIONINTEGRAL if E[j] is a (weighted) integral of the
 * population distribution, representing for example the total population
 * biomass. E[j] then can be expressed as E[j] = I[p][i]. Specify the
 * equilibrium condition in this case as condition[j] = I[p][i].
 *
 * Notice that the first index of the variable 'I[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the interaction variable. The interpretation of the latter
 * is up to the user. Also notice that the variable 'condition[j]' should
 * specify the equilibrium condition of environment variable 'E[j]'.
 *===========================================================================
 */

const int EnvironmentType[ENVIRON_DIM] = {GENERALODE};

void EnvEqui(double E[], double I[POPULATION_NR][INTERACT_DIM],
             double condition[ENVIRON_DIM])
{
    condition[0] = Rho*(R1MAX - R1) - I[0][0];
    condition[1] = Rho*(R2MAX - R2) - I[0][1];
    return;
}

/*==============================================================================*/

