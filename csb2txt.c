/*
  NAME
    csb2txt

    Program converts a binary CSB file to text. Either the entire file is
    converted, or just the single state indicated by the time value that is
    passed to the program as command-line

  Last modification: AMdR - Oct 23, 2017
***/
#ifndef CSB2TXT
#define CSB2TXT
#endif

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "float.h"
#include <sys/stat.h>
#include <sys/param.h>
#include <unistd.h>

#define MAXCOLUMNS                100
#define MAXPARS                   1024
#define MAXSTRINGLEN              1024
#define MAXPOP                    20
#define USAGE                     "csb2txt [-f \"printf() format string\"] [-eq2ebt] [-t0] <CSB filename> [parameter or time value]"

// List of magic keys of all recognized CSB file types

#define CSB_MAGIC_KEY1            20030509

// Macros to round to memory pages

#define PAGESIZE                  (getpagesize())
#define ROUND_PAGE(s)             ((int)((s & (PAGESIZE - 1)) ? (s & ~(PAGESIZE - 1)) + PAGESIZE : s))

#if defined(DBL_MAX)                                                                // Stub value for missing data point
#define   MISSING_VALUE           (DBL_MAX)
#elif defined(MAXDOUBLE)
#define   MISSING_VALUE           (MAXDOUBLE)
#else
#define   MISSING_VALUE           (1.23456789e+307)
#endif

// Type definition for environment and population

#ifdef _MSC_VER
typedef __int32 uint32_t;
#else
#include <stdint.h>
#endif

typedef struct envdim
{
  double   timeval;
  int      columns;
  int      data_offset;
  uint32_t memory_used;
} Envdim;

typedef struct popdim
{
  double timeval;
  int    population;
  int    cohorts;
  int    columns;
  int    data_offset;
  int    lastpopdim;
} Popdim;

void                              *mem_base     = NULL;
long                              MemAllocated = 0L;
char                              buf[MAXPATHLEN];
char                              printfmt[128];
double                            **pop;
int                               *Cohort_No, parDim;
double                            output[MAXCOLUMNS];
double                            parVals[MAXPARS];
int                               changedParsDim, changedParsIndex[MAXPARS];
int                               convert2ebtisf = 0;
int                               settime0 = 0;

/*==================================================================================================================================*/

int isfile(char *f)                                                                 // Returns 1 if file is regular, 0 otherwise
{
  struct stat st;

  if (stat(f, &st) != 0) return 0;

  return ((st.st_mode & S_IFMT) == S_IFREG);
}


/*==================================================================================================================================*/

static void PrettyPrintDouble(double output)

{
  int    mag  = 0;
  double base = 1;

  while ((output > base) && (mag < 4))
    {
      mag++;
      base *= 10;
    }

  if (strlen(printfmt))
    (void)printf(printfmt, output);
  else if (((fabs(output) <= 1.0E4) && (fabs(output) >= 1.0E-4)) || (output == 0))
    {
      switch (mag)
        {
          case 4:
            (void)printf("%.7f", output);
            break;
          case 3:
            (void)printf("%.8f", output);
            break;
          case 2:
            (void)printf("%.9f", output);
            break;
          case 1:
            (void)printf("%.10f", output);
            break;
          default:
            (void)printf("%.10f", output);
            break;
        }
    }
  else
    (void)printf("%.6E", output);

  return;
}

/*==================================================================================================================================*/

static void write_txt_file(void)

{
  register int  i, j, k;
  double        *cdbl, *base_pnt;
  double        pars[MAXPARS];
  Envdim        *cenv;
  Popdim        *cpop;
  int           evodyn = 0, evopars = 0, bifpar1 = 0, bifpar2 = 0;
  int           popnr = 0, MaxBirthStateNr = 0, curBirthState = 0, IStateDim;
  char          popstring[MAXSTRINGLEN];
  double        *birthstates = NULL;
  int           bmag, num;
  

  cenv     = (Envdim *)mem_base;
  cdbl     = ((double *)mem_base + cenv->data_offset + cenv->columns);
  cpop     = (Popdim *)cdbl;
  base_pnt = ((double *)mem_base + cenv->data_offset);

  evodyn  = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "evolutionary time") != NULL);
  evopars = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "evolutionary parameters for parameter value") != NULL);
  bifpar1 = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "for parameter value ") != NULL);
  bifpar2 = (strstr((char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)), "for parameter values") != NULL);

  if (!convert2ebtisf)
    {
      (void)printf("##############################\n");
      if (evodyn)
        (void)printf("# %s\n", "Evolutionary time value");
      else if (evopars || bifpar1)
        (void)printf("# %s\n", "Bifurcation parameter value");
      else if (bifpar2)
        (void)printf("# %s\n", "Bifurcation parameter values");
      PrettyPrintDouble(cenv->timeval);

      if (evodyn || evopars)
        {
          (void)printf("\n\n");
          (void)printf("# %s\n", "Evolutionary parameter values");
          // In case of EVODYN start at i=0, otherwise skip the first (bifurcation) parameter
          for (i = evopars; i < changedParsDim; i++)
            {
              if (i - evopars)(void)printf("\t");
              PrettyPrintDouble(base_pnt[cenv->columns - changedParsDim + i]);
            }
        }
      else if (bifpar2)
        {
          (void)printf("\t");
          PrettyPrintDouble(base_pnt[cenv->columns - changedParsDim + 1]);
        }
      (void)printf("\n\n");

      memcpy(pars, parVals, parDim*sizeof(double));
      for (i = 0; i < changedParsDim; i++)
        {
          if ((changedParsDim == 2) && (i == 1) && (changedParsIndex[0] == changedParsIndex[1]))
            continue;                                                                   // PIP results
          pars[changedParsIndex[i]] = base_pnt[cenv->columns - changedParsDim + i];
        }

      (void)printf("# %s", "All parameter values");
      // The following for loop runs to parDim - changedParsDim - 1 only as the parameter values are followed by the number
      // of changed parameters and the indices of the changing parameters.
      for (i = 0; i < parDim - changedParsDim - 1; i++)
        {
          if (i % 10 == 0)
            (void)printf("\n");
          else
            (void)printf("\t");
          PrettyPrintDouble(pars[i]);
        }
      (void)printf("\n\n");
    }

  // Write environment state
  if (cenv->data_offset > (int)(sizeof(Envdim)/sizeof(double) + 2))
    (void)printf("# %s\n", (char *)(((double *)cenv) + (sizeof(Envdim)/sizeof(double) + 1)));
  if (convert2ebtisf) PrettyPrintDouble(0.0);
  for (i = 0; i < (cenv->columns - changedParsDim); i++)
    {
      if (i || convert2ebtisf) (void)printf("\t");
      if ((!i) && settime0) PrettyPrintDouble(0.0);
      else PrettyPrintDouble(base_pnt[i]);
    }
  (void)printf("\n\n");

  // Write population state
  while (1)
    {
      if (convert2ebtisf)
        {
          if (!MaxBirthStateNr)
            {
              sprintf(popstring, "Birth states of population #%d", popnr);
              if (strstr((char *)(((double *)cpop) + (sizeof(Popdim)/sizeof(double) + 1)), popstring) != NULL)
                {
                  MaxBirthStateNr = cpop->cohorts;
                  IStateDim       = cpop->columns;
                  birthstates     = realloc(birthstates, MaxBirthStateNr*IStateDim*sizeof(double));
                  base_pnt = ((double *)cpop + cpop->data_offset);
                  for (j = 0; j < cpop->cohorts; j++)
                    for (k = 0; k < cpop->columns; k++) 
                      *(birthstates + j*IStateDim + k) =*(base_pnt + k*cpop->cohorts + j);
                  curBirthState = 0;
                  bmag = 1;
                  num  = MaxBirthStateNr;
                  while (num > 0)
                    {
                      bmag++;
                      num = num/10;
                    }
                }
            }
          else
            {
              if (MaxBirthStateNr == 1)
                sprintf(popstring, "State of population #%d", popnr);
              else
                sprintf(popstring, "State of population #%d with birth state #%0*d", popnr, bmag, curBirthState);
              if (strstr((char *)(((double *)cpop) + (sizeof(Popdim)/sizeof(double) + 1)), popstring) != NULL)
                {
                  if (!curBirthState) (void)printf("# State of population #%d\n", popnr);
                  base_pnt = ((double *)cpop + cpop->data_offset);
                  for (j = 0; j < cpop->cohorts; j++)
                    {
                      for (k = 0; k < cpop->columns; k++)
                        {
                          if (k)(void)printf("\t");
                          PrettyPrintDouble(*(base_pnt + k*cpop->cohorts + j));
                        }
                      printf("\t%E", MISSING_VALUE);
                      for (k = 0; k < IStateDim; k++)
                        {
                          printf("\t");
                          PrettyPrintDouble(*(birthstates + curBirthState*IStateDim + k));
                        }
                      printf("\t0\t%d\t0.0\n", curBirthState);
                    }
                  curBirthState++;
                  if (curBirthState == MaxBirthStateNr)
                    {
                      popnr++;
                      MaxBirthStateNr = 0;
                      (void)printf("\n");
                    }
                }
            }
        }
      else
        {
          if (cpop->data_offset > (int)(sizeof(Popdim)/sizeof(double) + 2))
            (void)printf("# %s\n", (char *)(((double *)cpop) + (sizeof(Popdim)/sizeof(double) + 1)));
          base_pnt = ((double *)cpop + cpop->data_offset);
          for (j = 0; j < cpop->cohorts; j++, (void)printf("\n"))
            for (k = 0; k < cpop->columns; k++)
              {
                if (k)(void)printf("\t");
                PrettyPrintDouble(*(base_pnt + k*cpop->cohorts + j));
              }
          (void)printf("\n");
        }
      if (cpop->lastpopdim) break;

      cdbl = (((double *)cpop) + cpop->data_offset + (cpop->cohorts*cpop->columns));
      cpop = (Popdim *)cdbl;
    }

  (void)printf("\n");

  return;
}


/*==================================================================================================================================*/

int main(int argc, char **argv)

{
  char        **argpnt1 = NULL, **argpnt2 = NULL, **my_argv = NULL;
  int         my_argc;
  int         i, single = 0;
  double      timeval = -1.0, closest_time = HUGE_VAL;
  char        *fn;
  FILE        *fp = NULL;
  struct stat stattmp;
  uint32_t    filepos = 0, closest_fpos = -1, magic;
  Envdim      cur_env;
  int         file_ok = 1;

  my_argv = (char **)malloc((size_t)argc*sizeof(char *));
  argpnt1 = argv;
  argpnt2 = my_argv;
  my_argc = 0;
  strcpy(printfmt, "");
  while (*argpnt1)
    {
      if (!strcmp(*argpnt1, "-f"))
        {
          argpnt1++;
          if (!*argpnt1)
            {
              fprintf(stderr, "\nNo print format specified!\n");
              fprintf(stderr, "\nUsage: %s\n\n", USAGE);
              free(my_argv);
              return 1;
            }
          strcpy(printfmt, *argpnt1);
        }
      else if (!strcmp(*argpnt1, "-eq2ebt"))
        {
          convert2ebtisf = 1;
        }
      else if (!strcmp(*argpnt1, "-t0"))
        {
          settime0 = 1;
        }
      else if (!strncmp(*argpnt1, "-", 1))
        {
          fprintf(stderr, "\nUnknown command-line option %s!\n", *argpnt1);
          fprintf(stderr, "\nUsage: %s\n\n", USAGE);
          free(my_argv);
          return 1;
        }
      else
        {
          *argpnt2 =*argpnt1;
          my_argc++;
          argpnt2++;
        }
      argpnt1++;
    }

  switch (my_argc)
    {
      case 3:
        timeval = atof(my_argv[2]);
        single  = 1;
      case 2:
        fn = my_argv[1];
        if (!strcmp(fn + strlen(fn) - 1, "."))
          strcat(fn, "csb");
        else if (strcmp(fn + strlen(fn) - 4, ".csb"))
          strcat(fn, ".csb");

        if (!stat(fn, &stattmp))                                                    // couldn't stat
          {
            if ((!strcmp(fn + strlen(fn) - 3, "csb")) && (stattmp.st_mode & S_IFREG))
              fp = fopen(fn, "r");                                                  // open if regular file
            else
              {
                fprintf(stderr, "File %s is not a regular CSB file.\n", fn);
                free(my_argv);
                return 1;
              }
          }
        if (fp == NULL)
          {
            fprintf(stderr, "Can't open file %s\n", fn);
            free(my_argv);
            return 1;
          }
        break;
      default:
        fprintf(stderr, "\nUsage: %s\n\n", USAGE);
        free(my_argv);
        return 1;
    }

  if (fread(&magic, sizeof(uint32_t), 1, fp) != 1)
    {
      fprintf(stderr, "\nCSB file does not contain any data!\n\n");
      free(my_argv);
      return 1;
    }

  switch (magic)
    {
      case CSB_MAGIC_KEY1:
        // CSB file version dd. May 09, 2003 with array of parameter values at the start of the file
        file_ok              = (fread(&parDim, sizeof(int), 1, fp) == 1);
        if (file_ok) file_ok = (fread(parVals, sizeof(double), parDim, fp) == parDim);
        if (!file_ok)
          {
            fprintf(stderr, "\nCSB file seems to be invalid or corrupt!\n\n");
            free(my_argv);
            return 1;
          }
        changedParsDim = (int)floor(parVals[parDim - 1] + 0.5);
        for (i = 0; i < changedParsDim; i++) changedParsIndex[i] = (int)floor(parVals[parDim - 1 - changedParsDim + i] + 0.5);
        filepos                                                  = ftell(fp);
        break;
      default:
        fseek(fp, filepos, SEEK_SET);
        break;
    }

  while (fread(&cur_env, sizeof(Envdim), 1, fp) == 1)
    {
      fseek(fp, filepos, SEEK_SET);
      if (cur_env.memory_used > MemAllocated)
        {
          MemAllocated = ROUND_PAGE(cur_env.memory_used);
          mem_base     = (void *)realloc(mem_base, (size_t)MemAllocated);
          if (!mem_base)
            {
              fprintf(stderr, "\nCould not allocate memory to read in data!\n");
              free(my_argv);
              return 1;
            }
        }

      if (fread(mem_base, 1, (size_t)cur_env.memory_used, fp) != (size_t)cur_env.memory_used)
        {
          (void)fprintf(stderr, "\nFailed to read values from file: %s at line %d!\n\n", fn, __LINE__);
          if (mem_base) free(mem_base);
          free(my_argv);
          return 1;
        }

      if (single)
        {
          if (fabs(cur_env.timeval - timeval) < fabs(closest_time - timeval))
            {
              closest_time = cur_env.timeval;
              closest_fpos = filepos;
            }
        }
      else
        {
          write_txt_file();
        }

      fseek(fp, filepos, SEEK_SET);
      fseek(fp, cur_env.memory_used, SEEK_CUR);
      filepos = ftell(fp);
    }

  if (single)
    {
      fseek(fp, closest_fpos, SEEK_SET);
      if (fread(&cur_env, sizeof(Envdim), 1, fp) != 1)
        {
          (void)fprintf(stderr, "\nFailed to read values from file: %s at line %d!\n\n", fn, __LINE__);
          if (mem_base) free(mem_base);
          free(my_argv);
          return 1;
        }

      fseek(fp, closest_fpos, SEEK_SET);
      if (fread(mem_base, 1, (size_t)cur_env.memory_used, fp) != (size_t)cur_env.memory_used)
        {
          (void)fprintf(stderr, "\nFailed to read values from file: %s at line %d!\n\n", fn, __LINE__);
          if (mem_base) free(mem_base);
          free(my_argv);
          return 1;
        }
      write_txt_file();
      if (mem_base) free(mem_base);
      free(my_argv);
      return 1;
    }

  if (mem_base) free(mem_base);
  free(my_argv);
  return 0;
}


/*==================================================================================================================================*/
