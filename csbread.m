function popstate = csbread(filename, varargin)

%
%
% csbread: Performs demographic analysis of a structured population model
%
% Syntax:
%
%   popstate = csbread(filename, statenr)
%
% Arguments:
%
%   filename:  (string, required)
%              Basename of the CSB file with population states, either with or
%              without '.csb' extension
%
%   statenr:   (integer or string)
%              Integer index or string name of the population state to read in
%
% Output:
%
%   popstate:  Structure containing the population state
%
% Copyright (C) 2015, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%

workdir  = pwd; % Save the current directory
c = onCleanup(@()cd(workdir));
nVarargs = length(varargin);

% Process the file name argument
if ((length(filename) == 0) || (~ischar(filename)))
    msg = ['You have to specify a file name'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end
if (findstr(filename, '.csb') == length(filename)-3)
    csbfile = filename;
else
    csbfile = [filename, '.csb'];
end
if ~exist(csbfile, 'file')
    msg = ['Data file ', csbfile, ' does not exist'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end

% Process the optional state selector argument
selectedstate = int32(-1);
timeval = 0.0;
for k = 1:nVarargs
    if (isnumeric(varargin{k}) && (length(varargin{k}) == 1))
        selectedstate = int32(varargin{k});
        if (selectedstate < 1)
            warning('Negative index! Result shown for index set to its default value 1');
            selectedstate = int32(1);
        end
    elseif ischar(varargin{k})
        timeval = str2num(varargin{k}(7:end));
    else
        error('You have to specify a valid state name of the form "State-XXXXXXXX" or its corresponding index from a call to csbread()')
    end
end

is_octave = exist ('OCTAVE_VERSION', 'builtin');
fullfilename = mfilename('fullpath');
coredir = fileparts(fullfilename);
ext = mexext;
exefullname = [coredir, '/', 'csb2mat.', ext];
exename     = ['csb2mat.', ext];
exebasename = 'csb2mat';

if (mislocked(exebasename))
    munlock(exebasename);
end
clear(exebasename)

d1 = dir([coredir, '/', 'csb2mat.c']);
d2 = dir(exefullname);
if (~exist(exefullname, 'file') || (d1.datenum > d2.datenum))
    cd(coredir);
    if exist(exename, 'file')
        delete(exename);
    end
    msg = ['Building executable ', exename, ' using sources from ', pwd, ' ...'];
    disp(' ');
    disp(msg);
    disp(' ');
    if (is_octave)
        % To build the file using Octave, only tested on Mac OS:
        optstring   = '';
        outstring   = [' -o ' exename ];
        cflagstring = ' -DOCTAVE_MEX_FILE';
        utlib       = ' ';
    else
        % To build the file using Matlab
        outstring   = [' -output ' exename ];
        if strcmp(ext, 'mexw64')
            % To build the file on Windows:
            optstring   = ' -O  -largeArrayDims';
            cflagstring = ' COMPFLAGS="$COMPFLAGS"';
            utlib       = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libut.lib') '"'];
        else
            % To build the file on UNIX:
            % optstring   = ' -g -DDEBUG=1 -O  -largeArrayDims';
            optstring   = ' -O  -largeArrayDims';
            cflagstring = ' CFLAGS=''\$CFLAGS''';
            utlib       = ' -lut';
        end
    end
    
    cmd = ['mex' optstring outstring cflagstring ' -I"' coredir '"'];
    cmd = [cmd ' "' coredir '/csb2mat.c"' utlib];

    eval(cmd);
    if ~exist(exename, 'file')
        msg = ['Compilation of file ', exename, ' failed'];
        disp(' ');
        disp(msg);
        disp(' ');
        cd(workdir);
        return;
    end
    if is_octave
        delete('csb2mat.o');
    end
    cd(workdir);
end

if is_octave
    fflush(stdout);
end

if (nVarargs > 0)
    popstate = feval(exebasename, csbfile, int32(selectedstate), timeval);
else
    ignorestate = feval(exebasename, csbfile);
    popstate = [];
end

if (mislocked(exebasename))
    munlock(exebasename);
end
clear(exebasename)

end
