function output = PSPMind(modelname, environment, varargin)

%
% PSPMind: Computes the individual life history in a given environment 
%
% Syntax:
%
%   output = PSPMind(modelname, environment, parameters, options, 'clean', 'force', 'debug')
%
% Arguments:
%
%   modelname:   (string, required)
%                Basename of the file with model specification. The file
%                should have an extension '.m' or '.h'. For example, the model 'PNAS2002'
%                is specified in the file 'PNAS2002.m' or 'PNAS2002.h'
%
%   environment: (row vector, required)
%                Vector of length equal to the length of the variable
%                'EnvironmentState' in case the model is specified in Matlab
%                (in a '.m' file) or equal to the constant ENVIRON_DIM in case
%                the model is specified in C (in a '.h' file). The vector
%                should specify the value of the environmental variables 
%                at which to calculate the individual life history.
%                Optionally, this vector can be extended with values of the birth
%                rates for all structured populations in the model, which would
%                scale the output of the model with these birth rates.
%
%   parameters:  (row vector, required, but can be the empty vector [])
%                Vector of a length equal to the length of the variable
%                'DefaultParameters' in case the model is specified in Matlab
%                (in a '.m' file) or equal to the constant PARAMETER_NR in case
%                the model is specified in C (in a '.h' file). The vector
%                specifies the values for the model parameters to
%                use in the computation. Vectors of other lengths, including
%                an empty vector will be ignored.
%
%   options:     (cell array, required, but can be the empty cell array {})
%                Cell array with a pair of an option name and a value (for
%                example {'isort', '1'}). The only possible option name 
%                and its values is:
%
%                'isort',  '<index>': Index of i-state variable to use as
%                                     ruling variable for sorting the
%                                     structured populations
%
%   'clean':     (string. optional argument)
%                Remove all the result files of the model before the
%                computation
%
%   'force':     (string, optional argument)
%                Force a rebuilding of the model before the computation
%
%   'debug':     (string, optional argument)
%                Compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   The output is a structure with the population state as normally stored in the
%   .mat output file of PSPMdemo, PSPMequi, and PSPMevodyn.
%
% Copyright (C) 2017, Andre M. de Roos, University of Amsterdam
% This program comes with ABSOLUTELY NO WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
% License (<http://www.gnu.org/philosophy/why-not-lgpl.html>) for more details.
%


forcebuild = 0;
debugging = 0;
nVarargs = length(varargin);
myArgc = 0;
parameters = [];
options = {};

% Deduce the basename of the executable
if ((modelname(length(modelname)-1) == '.') && ((modelname(length(modelname)) == 'h') || (modelname(length(modelname)) == 'm')))
    basename = modelname(1:length(modelname)-2);
else
    basename = modelname;
end

% Process the variable arguments
for k = 1:nVarargs
    if ischar(varargin{k})
        if strcmp(varargin{k}, 'clean')
            resfnames = strcat(basename, '-', '*', '-', '*');
            delete(strcat(resfnames, '.err'));
            delete(strcat(resfnames, '.out'));
            delete(strcat(resfnames, '.mat'));
        elseif strcmp(varargin{k}, 'force') || strcmp(varargin{k}, 'forcebuild')
            forcebuild = 1;
        elseif strcmp(varargin{k}, 'debug')
            debugging = 1;
        else
            msg = ['Unknown function argument: ', varargin{k}];
            disp(' ');
            disp(msg);
            disp(' ');
        end
    else
        switch myArgc
            case 0
                parameters = varargin{k};
            case 1
                options = varargin{k};
        end
        myArgc = myArgc + 1;
    end
end

% Consistency checks of other arguments
if (~length(environment) || (~isa(environment,'double')))
    error('Environmental values should be a vector with double values');
end
if (length(parameters) && (~isa(parameters,'double'))) 
    error('If specified parameter values should be a vector with double values');
end
if (~isa(options,'cell'))
    error('If specified options should be a cell array');
end

% Build the executable
[Mmodel exebasename] = buildSO('PSPMind', modelname, forcebuild, debugging);
if (~exist(exebasename, 'file'))
    msg = ['Executable ', exebasename, ' not found'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end

drawnow;

fname = feval(exebasename, environment, parameters, options);

if (Mmodel == 1)
    clear global PopulationNr IStateDimension LifeHistoryStages ImpactDimension
    clear global NumericalOptions EnvironmentState DefaultParameters
    clear global StateAtBirthFunc LifeHistoryRatesFunc LifeStageEndingsFunc DiscreteChangesFunc EnvEquiFunc
end

if (~isempty(fname))
    if exist(strcat(fname, '.mat'), 'file')
        load(strcat(fname, '.mat'), 'LifeHistory');
        output = LifeHistory;
        disp(output);
    end
end
